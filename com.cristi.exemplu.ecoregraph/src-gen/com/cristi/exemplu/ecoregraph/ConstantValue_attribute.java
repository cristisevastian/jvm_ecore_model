/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constant Value attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ConstantValue_attribute#getInterfaceconstant_value <em>Interfaceconstant value</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getConstantValue_attribute()
 * @model
 * @generated
 */
public interface ConstantValue_attribute extends AbstractAttribute_info, InterfaceField_attribute_info {
	/**
	 * Returns the value of the '<em><b>Interfaceconstant value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interfaceconstant value</em>' reference.
	 * @see #setInterfaceconstant_value(InterfaceCONSTANT_Value)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getConstantValue_attribute_Interfaceconstant_value()
	 * @model required="true"
	 * @generated
	 */
	InterfaceCONSTANT_Value getInterfaceconstant_value();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.ConstantValue_attribute#getInterfaceconstant_value <em>Interfaceconstant value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interfaceconstant value</em>' reference.
	 * @see #getInterfaceconstant_value()
	 * @generated
	 */
	void setInterfaceconstant_value(InterfaceCONSTANT_Value value);

} // ConstantValue_attribute
