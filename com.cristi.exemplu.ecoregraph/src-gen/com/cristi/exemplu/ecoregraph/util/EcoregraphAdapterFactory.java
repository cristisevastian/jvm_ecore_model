/**
 */
package com.cristi.exemplu.ecoregraph.util;

import com.cristi.exemplu.ecoregraph.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage
 * @generated
 */
public class EcoregraphAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static EcoregraphPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EcoregraphAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = EcoregraphPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EcoregraphSwitch<Adapter> modelSwitch = new EcoregraphSwitch<Adapter>() {
		@Override
		public Adapter caseCONSTANT_MethodHandle_Info(CONSTANT_MethodHandle_Info object) {
			return createCONSTANT_MethodHandle_InfoAdapter();
		}

		@Override
		public Adapter caseCONSTANT_Fieldref_info(CONSTANT_Fieldref_info object) {
			return createCONSTANT_Fieldref_infoAdapter();
		}

		@Override
		public Adapter caseCONSTANT_Methodref_info(CONSTANT_Methodref_info object) {
			return createCONSTANT_Methodref_infoAdapter();
		}

		@Override
		public Adapter caseCONSTANT_InterfaceMethodref_info(CONSTANT_InterfaceMethodref_info object) {
			return createCONSTANT_InterfaceMethodref_infoAdapter();
		}

		@Override
		public Adapter caseCONSTANT_Class_info(CONSTANT_Class_info object) {
			return createCONSTANT_Class_infoAdapter();
		}

		@Override
		public Adapter caseCONSTANT_NameAndType_info(CONSTANT_NameAndType_info object) {
			return createCONSTANT_NameAndType_infoAdapter();
		}

		@Override
		public Adapter caseCONSTANT_Utf8_info(CONSTANT_Utf8_info object) {
			return createCONSTANT_Utf8_infoAdapter();
		}

		@Override
		public Adapter caseCONSTANT_MethodType_info(CONSTANT_MethodType_info object) {
			return createCONSTANT_MethodType_infoAdapter();
		}

		@Override
		public Adapter caseCONSTANT_String_info(CONSTANT_String_info object) {
			return createCONSTANT_String_infoAdapter();
		}

		@Override
		public Adapter caseCONSTANT_InvokeDynamic_info(CONSTANT_InvokeDynamic_info object) {
			return createCONSTANT_InvokeDynamic_infoAdapter();
		}

		@Override
		public Adapter caseField_info(Field_info object) {
			return createField_infoAdapter();
		}

		@Override
		public Adapter caseMethod_info(Method_info object) {
			return createMethod_infoAdapter();
		}

		@Override
		public Adapter caseAbstractCp_info(AbstractCp_info object) {
			return createAbstractCp_infoAdapter();
		}

		@Override
		public Adapter caseInterfaceCONSTANT_Ref_info(InterfaceCONSTANT_Ref_info object) {
			return createInterfaceCONSTANT_Ref_infoAdapter();
		}

		@Override
		public Adapter caseCONSTANT_Integer_info(CONSTANT_Integer_info object) {
			return createCONSTANT_Integer_infoAdapter();
		}

		@Override
		public Adapter caseCONSTANT_Float_info(CONSTANT_Float_info object) {
			return createCONSTANT_Float_infoAdapter();
		}

		@Override
		public Adapter caseCONSTANT_Long_info(CONSTANT_Long_info object) {
			return createCONSTANT_Long_infoAdapter();
		}

		@Override
		public Adapter caseCONSTANT_Double_info(CONSTANT_Double_info object) {
			return createCONSTANT_Double_infoAdapter();
		}

		@Override
		public Adapter caseClassFile(ClassFile object) {
			return createClassFileAdapter();
		}

		@Override
		public Adapter caseAbstractAttribute_info(AbstractAttribute_info object) {
			return createAbstractAttribute_infoAdapter();
		}

		@Override
		public Adapter caseInterfaceMethod_attribute_info(InterfaceMethod_attribute_info object) {
			return createInterfaceMethod_attribute_infoAdapter();
		}

		@Override
		public Adapter caseInterfaceField_attribute_info(InterfaceField_attribute_info object) {
			return createInterfaceField_attribute_infoAdapter();
		}

		@Override
		public Adapter caseInterfaceClass_attribute_info(InterfaceClass_attribute_info object) {
			return createInterfaceClass_attribute_infoAdapter();
		}

		@Override
		public Adapter caseConstantValue_attribute(ConstantValue_attribute object) {
			return createConstantValue_attributeAdapter();
		}

		@Override
		public Adapter caseInterfaceCONSTANT_Value(InterfaceCONSTANT_Value object) {
			return createInterfaceCONSTANT_ValueAdapter();
		}

		@Override
		public Adapter caseCode_attribute(Code_attribute object) {
			return createCode_attributeAdapter();
		}

		@Override
		public Adapter caseexception_table_entry(exception_table_entry object) {
			return createexception_table_entryAdapter();
		}

		@Override
		public Adapter caseStackMapTable_attribute(StackMapTable_attribute object) {
			return createStackMapTable_attributeAdapter();
		}

		@Override
		public Adapter caseAbstractVerification_type_info(AbstractVerification_type_info object) {
			return createAbstractVerification_type_infoAdapter();
		}

		@Override
		public Adapter caseTop_variable_info(Top_variable_info object) {
			return createTop_variable_infoAdapter();
		}

		@Override
		public Adapter caseInteger_variable_info(Integer_variable_info object) {
			return createInteger_variable_infoAdapter();
		}

		@Override
		public Adapter caseFloat_variable_info(Float_variable_info object) {
			return createFloat_variable_infoAdapter();
		}

		@Override
		public Adapter caseNull_variable_info(Null_variable_info object) {
			return createNull_variable_infoAdapter();
		}

		@Override
		public Adapter caseUninitializedThis_variable_info(UninitializedThis_variable_info object) {
			return createUninitializedThis_variable_infoAdapter();
		}

		@Override
		public Adapter caseObject_variable_info(Object_variable_info object) {
			return createObject_variable_infoAdapter();
		}

		@Override
		public Adapter caseNewEClass36(NewEClass36 object) {
			return createNewEClass36Adapter();
		}

		@Override
		public Adapter caseUninitialized_variable_info(Uninitialized_variable_info object) {
			return createUninitialized_variable_infoAdapter();
		}

		@Override
		public Adapter caseLong_variable_info(Long_variable_info object) {
			return createLong_variable_infoAdapter();
		}

		@Override
		public Adapter caseDouble_variable_info(Double_variable_info object) {
			return createDouble_variable_infoAdapter();
		}

		@Override
		public Adapter caseAbstractStack_map_frame(AbstractStack_map_frame object) {
			return createAbstractStack_map_frameAdapter();
		}

		@Override
		public Adapter casesame_frame(same_frame object) {
			return createsame_frameAdapter();
		}

		@Override
		public Adapter casesame_locals_1_stack_item_frame(same_locals_1_stack_item_frame object) {
			return createsame_locals_1_stack_item_frameAdapter();
		}

		@Override
		public Adapter casesame_locals_1_stack_item_frame_extended(same_locals_1_stack_item_frame_extended object) {
			return createsame_locals_1_stack_item_frame_extendedAdapter();
		}

		@Override
		public Adapter casesame_frame_extended(same_frame_extended object) {
			return createsame_frame_extendedAdapter();
		}

		@Override
		public Adapter casechop_frame(chop_frame object) {
			return createchop_frameAdapter();
		}

		@Override
		public Adapter caseappend_frame(append_frame object) {
			return createappend_frameAdapter();
		}

		@Override
		public Adapter casefull_frame(full_frame object) {
			return createfull_frameAdapter();
		}

		@Override
		public Adapter caseExceptions_attribute(Exceptions_attribute object) {
			return createExceptions_attributeAdapter();
		}

		@Override
		public Adapter caseInterfaceCode_attribute(InterfaceCode_attribute object) {
			return createInterfaceCode_attributeAdapter();
		}

		@Override
		public Adapter caseInnerClasses_attribute(InnerClasses_attribute object) {
			return createInnerClasses_attributeAdapter();
		}

		@Override
		public Adapter caseClass_member(Class_member object) {
			return createClass_memberAdapter();
		}

		@Override
		public Adapter caseSynthetic_attribute(Synthetic_attribute object) {
			return createSynthetic_attributeAdapter();
		}

		@Override
		public Adapter caseSignature_attribute(Signature_attribute object) {
			return createSignature_attributeAdapter();
		}

		@Override
		public Adapter caseNewEClass54(NewEClass54 object) {
			return createNewEClass54Adapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_MethodHandle_Info <em>CONSTANT Method Handle Info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_MethodHandle_Info
	 * @generated
	 */
	public Adapter createCONSTANT_MethodHandle_InfoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Fieldref_info <em>CONSTANT Fieldref info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Fieldref_info
	 * @generated
	 */
	public Adapter createCONSTANT_Fieldref_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Methodref_info <em>CONSTANT Methodref info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Methodref_info
	 * @generated
	 */
	public Adapter createCONSTANT_Methodref_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_InterfaceMethodref_info <em>CONSTANT Interface Methodref info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_InterfaceMethodref_info
	 * @generated
	 */
	public Adapter createCONSTANT_InterfaceMethodref_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Class_info <em>CONSTANT Class info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Class_info
	 * @generated
	 */
	public Adapter createCONSTANT_Class_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info <em>CONSTANT Name And Type info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info
	 * @generated
	 */
	public Adapter createCONSTANT_NameAndType_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info <em>CONSTANT Utf8 info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info
	 * @generated
	 */
	public Adapter createCONSTANT_Utf8_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_MethodType_info <em>CONSTANT Method Type info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_MethodType_info
	 * @generated
	 */
	public Adapter createCONSTANT_MethodType_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_String_info <em>CONSTANT String info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_String_info
	 * @generated
	 */
	public Adapter createCONSTANT_String_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_InvokeDynamic_info <em>CONSTANT Invoke Dynamic info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_InvokeDynamic_info
	 * @generated
	 */
	public Adapter createCONSTANT_InvokeDynamic_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.Field_info <em>Field info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.Field_info
	 * @generated
	 */
	public Adapter createField_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.Method_info <em>Method info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.Method_info
	 * @generated
	 */
	public Adapter createMethod_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.AbstractCp_info <em>Abstract Cp info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.AbstractCp_info
	 * @generated
	 */
	public Adapter createAbstractCp_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Ref_info <em>Interface CONSTANT Ref info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Ref_info
	 * @generated
	 */
	public Adapter createInterfaceCONSTANT_Ref_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Integer_info <em>CONSTANT Integer info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Integer_info
	 * @generated
	 */
	public Adapter createCONSTANT_Integer_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Float_info <em>CONSTANT Float info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Float_info
	 * @generated
	 */
	public Adapter createCONSTANT_Float_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Long_info <em>CONSTANT Long info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Long_info
	 * @generated
	 */
	public Adapter createCONSTANT_Long_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Double_info <em>CONSTANT Double info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Double_info
	 * @generated
	 */
	public Adapter createCONSTANT_Double_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.ClassFile <em>Class File</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile
	 * @generated
	 */
	public Adapter createClassFileAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.AbstractAttribute_info <em>Abstract Attribute info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.AbstractAttribute_info
	 * @generated
	 */
	public Adapter createAbstractAttribute_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.InterfaceMethod_attribute_info <em>Interface Method attribute info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.InterfaceMethod_attribute_info
	 * @generated
	 */
	public Adapter createInterfaceMethod_attribute_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.InterfaceField_attribute_info <em>Interface Field attribute info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.InterfaceField_attribute_info
	 * @generated
	 */
	public Adapter createInterfaceField_attribute_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.InterfaceClass_attribute_info <em>Interface Class attribute info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.InterfaceClass_attribute_info
	 * @generated
	 */
	public Adapter createInterfaceClass_attribute_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.ConstantValue_attribute <em>Constant Value attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.ConstantValue_attribute
	 * @generated
	 */
	public Adapter createConstantValue_attributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Value <em>Interface CONSTANT Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Value
	 * @generated
	 */
	public Adapter createInterfaceCONSTANT_ValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.Code_attribute <em>Code attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.Code_attribute
	 * @generated
	 */
	public Adapter createCode_attributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.exception_table_entry <em>exception table entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.exception_table_entry
	 * @generated
	 */
	public Adapter createexception_table_entryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.StackMapTable_attribute <em>Stack Map Table attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.StackMapTable_attribute
	 * @generated
	 */
	public Adapter createStackMapTable_attributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.AbstractVerification_type_info <em>Abstract Verification type info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.AbstractVerification_type_info
	 * @generated
	 */
	public Adapter createAbstractVerification_type_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.Top_variable_info <em>Top variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.Top_variable_info
	 * @generated
	 */
	public Adapter createTop_variable_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.Integer_variable_info <em>Integer variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.Integer_variable_info
	 * @generated
	 */
	public Adapter createInteger_variable_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.Float_variable_info <em>Float variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.Float_variable_info
	 * @generated
	 */
	public Adapter createFloat_variable_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.Null_variable_info <em>Null variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.Null_variable_info
	 * @generated
	 */
	public Adapter createNull_variable_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.UninitializedThis_variable_info <em>Uninitialized This variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.UninitializedThis_variable_info
	 * @generated
	 */
	public Adapter createUninitializedThis_variable_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.Object_variable_info <em>Object variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.Object_variable_info
	 * @generated
	 */
	public Adapter createObject_variable_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.NewEClass36 <em>New EClass36</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.NewEClass36
	 * @generated
	 */
	public Adapter createNewEClass36Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.Uninitialized_variable_info <em>Uninitialized variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.Uninitialized_variable_info
	 * @generated
	 */
	public Adapter createUninitialized_variable_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.Long_variable_info <em>Long variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.Long_variable_info
	 * @generated
	 */
	public Adapter createLong_variable_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.Double_variable_info <em>Double variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.Double_variable_info
	 * @generated
	 */
	public Adapter createDouble_variable_infoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.AbstractStack_map_frame <em>Abstract Stack map frame</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.AbstractStack_map_frame
	 * @generated
	 */
	public Adapter createAbstractStack_map_frameAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.same_frame <em>same frame</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.same_frame
	 * @generated
	 */
	public Adapter createsame_frameAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame <em>same locals 1stack item frame</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame
	 * @generated
	 */
	public Adapter createsame_locals_1_stack_item_frameAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame_extended <em>same locals 1stack item frame extended</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame_extended
	 * @generated
	 */
	public Adapter createsame_locals_1_stack_item_frame_extendedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.same_frame_extended <em>same frame extended</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.same_frame_extended
	 * @generated
	 */
	public Adapter createsame_frame_extendedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.chop_frame <em>chop frame</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.chop_frame
	 * @generated
	 */
	public Adapter createchop_frameAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.append_frame <em>append frame</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.append_frame
	 * @generated
	 */
	public Adapter createappend_frameAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.full_frame <em>full frame</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.full_frame
	 * @generated
	 */
	public Adapter createfull_frameAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.Exceptions_attribute <em>Exceptions attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.Exceptions_attribute
	 * @generated
	 */
	public Adapter createExceptions_attributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.InterfaceCode_attribute <em>Interface Code attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.InterfaceCode_attribute
	 * @generated
	 */
	public Adapter createInterfaceCode_attributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.InnerClasses_attribute <em>Inner Classes attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.InnerClasses_attribute
	 * @generated
	 */
	public Adapter createInnerClasses_attributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.Class_member <em>Class member</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.Class_member
	 * @generated
	 */
	public Adapter createClass_memberAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.Synthetic_attribute <em>Synthetic attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.Synthetic_attribute
	 * @generated
	 */
	public Adapter createSynthetic_attributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.Signature_attribute <em>Signature attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.Signature_attribute
	 * @generated
	 */
	public Adapter createSignature_attributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.cristi.exemplu.ecoregraph.NewEClass54 <em>New EClass54</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.cristi.exemplu.ecoregraph.NewEClass54
	 * @generated
	 */
	public Adapter createNewEClass54Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //EcoregraphAdapterFactory
