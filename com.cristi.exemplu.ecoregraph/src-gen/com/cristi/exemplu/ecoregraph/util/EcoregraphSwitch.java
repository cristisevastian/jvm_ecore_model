/**
 */
package com.cristi.exemplu.ecoregraph.util;

import com.cristi.exemplu.ecoregraph.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage
 * @generated
 */
public class EcoregraphSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static EcoregraphPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EcoregraphSwitch() {
		if (modelPackage == null) {
			modelPackage = EcoregraphPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case EcoregraphPackage.CONSTANT_METHOD_HANDLE_INFO: {
			CONSTANT_MethodHandle_Info constanT_MethodHandle_Info = (CONSTANT_MethodHandle_Info) theEObject;
			T result = caseCONSTANT_MethodHandle_Info(constanT_MethodHandle_Info);
			if (result == null)
				result = caseAbstractCp_info(constanT_MethodHandle_Info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CONSTANT_FIELDREF_INFO: {
			CONSTANT_Fieldref_info constanT_Fieldref_info = (CONSTANT_Fieldref_info) theEObject;
			T result = caseCONSTANT_Fieldref_info(constanT_Fieldref_info);
			if (result == null)
				result = caseInterfaceCONSTANT_Ref_info(constanT_Fieldref_info);
			if (result == null)
				result = caseAbstractCp_info(constanT_Fieldref_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CONSTANT_METHODREF_INFO: {
			CONSTANT_Methodref_info constanT_Methodref_info = (CONSTANT_Methodref_info) theEObject;
			T result = caseCONSTANT_Methodref_info(constanT_Methodref_info);
			if (result == null)
				result = caseInterfaceCONSTANT_Ref_info(constanT_Methodref_info);
			if (result == null)
				result = caseAbstractCp_info(constanT_Methodref_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CONSTANT_INTERFACE_METHODREF_INFO: {
			CONSTANT_InterfaceMethodref_info constanT_InterfaceMethodref_info = (CONSTANT_InterfaceMethodref_info) theEObject;
			T result = caseCONSTANT_InterfaceMethodref_info(constanT_InterfaceMethodref_info);
			if (result == null)
				result = caseInterfaceCONSTANT_Ref_info(constanT_InterfaceMethodref_info);
			if (result == null)
				result = caseAbstractCp_info(constanT_InterfaceMethodref_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CONSTANT_CLASS_INFO: {
			CONSTANT_Class_info constanT_Class_info = (CONSTANT_Class_info) theEObject;
			T result = caseCONSTANT_Class_info(constanT_Class_info);
			if (result == null)
				result = caseAbstractCp_info(constanT_Class_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CONSTANT_NAME_AND_TYPE_INFO: {
			CONSTANT_NameAndType_info constanT_NameAndType_info = (CONSTANT_NameAndType_info) theEObject;
			T result = caseCONSTANT_NameAndType_info(constanT_NameAndType_info);
			if (result == null)
				result = caseAbstractCp_info(constanT_NameAndType_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CONSTANT_UTF8_INFO: {
			CONSTANT_Utf8_info constanT_Utf8_info = (CONSTANT_Utf8_info) theEObject;
			T result = caseCONSTANT_Utf8_info(constanT_Utf8_info);
			if (result == null)
				result = caseAbstractCp_info(constanT_Utf8_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CONSTANT_METHOD_TYPE_INFO: {
			CONSTANT_MethodType_info constanT_MethodType_info = (CONSTANT_MethodType_info) theEObject;
			T result = caseCONSTANT_MethodType_info(constanT_MethodType_info);
			if (result == null)
				result = caseAbstractCp_info(constanT_MethodType_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CONSTANT_STRING_INFO: {
			CONSTANT_String_info constanT_String_info = (CONSTANT_String_info) theEObject;
			T result = caseCONSTANT_String_info(constanT_String_info);
			if (result == null)
				result = caseAbstractCp_info(constanT_String_info);
			if (result == null)
				result = caseInterfaceCONSTANT_Value(constanT_String_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CONSTANT_INVOKE_DYNAMIC_INFO: {
			CONSTANT_InvokeDynamic_info constanT_InvokeDynamic_info = (CONSTANT_InvokeDynamic_info) theEObject;
			T result = caseCONSTANT_InvokeDynamic_info(constanT_InvokeDynamic_info);
			if (result == null)
				result = caseAbstractCp_info(constanT_InvokeDynamic_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.FIELD_INFO: {
			Field_info field_info = (Field_info) theEObject;
			T result = caseField_info(field_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.METHOD_INFO: {
			Method_info method_info = (Method_info) theEObject;
			T result = caseMethod_info(method_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.ABSTRACT_CP_INFO: {
			AbstractCp_info abstractCp_info = (AbstractCp_info) theEObject;
			T result = caseAbstractCp_info(abstractCp_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.INTERFACE_CONSTANT_REF_INFO: {
			InterfaceCONSTANT_Ref_info interfaceCONSTANT_Ref_info = (InterfaceCONSTANT_Ref_info) theEObject;
			T result = caseInterfaceCONSTANT_Ref_info(interfaceCONSTANT_Ref_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CONSTANT_INTEGER_INFO: {
			CONSTANT_Integer_info constanT_Integer_info = (CONSTANT_Integer_info) theEObject;
			T result = caseCONSTANT_Integer_info(constanT_Integer_info);
			if (result == null)
				result = caseAbstractCp_info(constanT_Integer_info);
			if (result == null)
				result = caseInterfaceCONSTANT_Value(constanT_Integer_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CONSTANT_FLOAT_INFO: {
			CONSTANT_Float_info constanT_Float_info = (CONSTANT_Float_info) theEObject;
			T result = caseCONSTANT_Float_info(constanT_Float_info);
			if (result == null)
				result = caseAbstractCp_info(constanT_Float_info);
			if (result == null)
				result = caseInterfaceCONSTANT_Value(constanT_Float_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CONSTANT_LONG_INFO: {
			CONSTANT_Long_info constanT_Long_info = (CONSTANT_Long_info) theEObject;
			T result = caseCONSTANT_Long_info(constanT_Long_info);
			if (result == null)
				result = caseAbstractCp_info(constanT_Long_info);
			if (result == null)
				result = caseInterfaceCONSTANT_Value(constanT_Long_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CONSTANT_DOUBLE_INFO: {
			CONSTANT_Double_info constanT_Double_info = (CONSTANT_Double_info) theEObject;
			T result = caseCONSTANT_Double_info(constanT_Double_info);
			if (result == null)
				result = caseAbstractCp_info(constanT_Double_info);
			if (result == null)
				result = caseInterfaceCONSTANT_Value(constanT_Double_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CLASS_FILE: {
			ClassFile classFile = (ClassFile) theEObject;
			T result = caseClassFile(classFile);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO: {
			AbstractAttribute_info abstractAttribute_info = (AbstractAttribute_info) theEObject;
			T result = caseAbstractAttribute_info(abstractAttribute_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.INTERFACE_METHOD_ATTRIBUTE_INFO: {
			InterfaceMethod_attribute_info interfaceMethod_attribute_info = (InterfaceMethod_attribute_info) theEObject;
			T result = caseInterfaceMethod_attribute_info(interfaceMethod_attribute_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.INTERFACE_FIELD_ATTRIBUTE_INFO: {
			InterfaceField_attribute_info interfaceField_attribute_info = (InterfaceField_attribute_info) theEObject;
			T result = caseInterfaceField_attribute_info(interfaceField_attribute_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.INTERFACE_CLASS_ATTRIBUTE_INFO: {
			InterfaceClass_attribute_info interfaceClass_attribute_info = (InterfaceClass_attribute_info) theEObject;
			T result = caseInterfaceClass_attribute_info(interfaceClass_attribute_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CONSTANT_VALUE_ATTRIBUTE: {
			ConstantValue_attribute constantValue_attribute = (ConstantValue_attribute) theEObject;
			T result = caseConstantValue_attribute(constantValue_attribute);
			if (result == null)
				result = caseAbstractAttribute_info(constantValue_attribute);
			if (result == null)
				result = caseInterfaceField_attribute_info(constantValue_attribute);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.INTERFACE_CONSTANT_VALUE: {
			InterfaceCONSTANT_Value interfaceCONSTANT_Value = (InterfaceCONSTANT_Value) theEObject;
			T result = caseInterfaceCONSTANT_Value(interfaceCONSTANT_Value);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CODE_ATTRIBUTE: {
			Code_attribute code_attribute = (Code_attribute) theEObject;
			T result = caseCode_attribute(code_attribute);
			if (result == null)
				result = caseAbstractAttribute_info(code_attribute);
			if (result == null)
				result = caseInterfaceMethod_attribute_info(code_attribute);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY: {
			exception_table_entry exception_table_entry = (exception_table_entry) theEObject;
			T result = caseexception_table_entry(exception_table_entry);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.STACK_MAP_TABLE_ATTRIBUTE: {
			StackMapTable_attribute stackMapTable_attribute = (StackMapTable_attribute) theEObject;
			T result = caseStackMapTable_attribute(stackMapTable_attribute);
			if (result == null)
				result = caseAbstractAttribute_info(stackMapTable_attribute);
			if (result == null)
				result = caseInterfaceCode_attribute(stackMapTable_attribute);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.ABSTRACT_VERIFICATION_TYPE_INFO: {
			AbstractVerification_type_info abstractVerification_type_info = (AbstractVerification_type_info) theEObject;
			T result = caseAbstractVerification_type_info(abstractVerification_type_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.TOP_VARIABLE_INFO: {
			Top_variable_info top_variable_info = (Top_variable_info) theEObject;
			T result = caseTop_variable_info(top_variable_info);
			if (result == null)
				result = caseAbstractVerification_type_info(top_variable_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.INTEGER_VARIABLE_INFO: {
			Integer_variable_info integer_variable_info = (Integer_variable_info) theEObject;
			T result = caseInteger_variable_info(integer_variable_info);
			if (result == null)
				result = caseAbstractVerification_type_info(integer_variable_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.FLOAT_VARIABLE_INFO: {
			Float_variable_info float_variable_info = (Float_variable_info) theEObject;
			T result = caseFloat_variable_info(float_variable_info);
			if (result == null)
				result = caseAbstractVerification_type_info(float_variable_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.NULL_VARIABLE_INFO: {
			Null_variable_info null_variable_info = (Null_variable_info) theEObject;
			T result = caseNull_variable_info(null_variable_info);
			if (result == null)
				result = caseAbstractVerification_type_info(null_variable_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.UNINITIALIZED_THIS_VARIABLE_INFO: {
			UninitializedThis_variable_info uninitializedThis_variable_info = (UninitializedThis_variable_info) theEObject;
			T result = caseUninitializedThis_variable_info(uninitializedThis_variable_info);
			if (result == null)
				result = caseAbstractVerification_type_info(uninitializedThis_variable_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.OBJECT_VARIABLE_INFO: {
			Object_variable_info object_variable_info = (Object_variable_info) theEObject;
			T result = caseObject_variable_info(object_variable_info);
			if (result == null)
				result = caseAbstractVerification_type_info(object_variable_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.NEW_ECLASS36: {
			NewEClass36 newEClass36 = (NewEClass36) theEObject;
			T result = caseNewEClass36(newEClass36);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.UNINITIALIZED_VARIABLE_INFO: {
			Uninitialized_variable_info uninitialized_variable_info = (Uninitialized_variable_info) theEObject;
			T result = caseUninitialized_variable_info(uninitialized_variable_info);
			if (result == null)
				result = caseAbstractVerification_type_info(uninitialized_variable_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.LONG_VARIABLE_INFO: {
			Long_variable_info long_variable_info = (Long_variable_info) theEObject;
			T result = caseLong_variable_info(long_variable_info);
			if (result == null)
				result = caseAbstractVerification_type_info(long_variable_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.DOUBLE_VARIABLE_INFO: {
			Double_variable_info double_variable_info = (Double_variable_info) theEObject;
			T result = caseDouble_variable_info(double_variable_info);
			if (result == null)
				result = caseAbstractVerification_type_info(double_variable_info);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.ABSTRACT_STACK_MAP_FRAME: {
			AbstractStack_map_frame abstractStack_map_frame = (AbstractStack_map_frame) theEObject;
			T result = caseAbstractStack_map_frame(abstractStack_map_frame);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.SAME_FRAME: {
			same_frame same_frame = (same_frame) theEObject;
			T result = casesame_frame(same_frame);
			if (result == null)
				result = caseAbstractStack_map_frame(same_frame);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME: {
			same_locals_1_stack_item_frame same_locals_1_stack_item_frame = (same_locals_1_stack_item_frame) theEObject;
			T result = casesame_locals_1_stack_item_frame(same_locals_1_stack_item_frame);
			if (result == null)
				result = caseAbstractStack_map_frame(same_locals_1_stack_item_frame);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED: {
			same_locals_1_stack_item_frame_extended same_locals_1_stack_item_frame_extended = (same_locals_1_stack_item_frame_extended) theEObject;
			T result = casesame_locals_1_stack_item_frame_extended(same_locals_1_stack_item_frame_extended);
			if (result == null)
				result = caseAbstractStack_map_frame(same_locals_1_stack_item_frame_extended);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.SAME_FRAME_EXTENDED: {
			same_frame_extended same_frame_extended = (same_frame_extended) theEObject;
			T result = casesame_frame_extended(same_frame_extended);
			if (result == null)
				result = caseAbstractStack_map_frame(same_frame_extended);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CHOP_FRAME: {
			chop_frame chop_frame = (chop_frame) theEObject;
			T result = casechop_frame(chop_frame);
			if (result == null)
				result = caseAbstractStack_map_frame(chop_frame);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.APPEND_FRAME: {
			append_frame append_frame = (append_frame) theEObject;
			T result = caseappend_frame(append_frame);
			if (result == null)
				result = caseAbstractStack_map_frame(append_frame);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.FULL_FRAME: {
			full_frame full_frame = (full_frame) theEObject;
			T result = casefull_frame(full_frame);
			if (result == null)
				result = caseAbstractStack_map_frame(full_frame);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE: {
			Exceptions_attribute exceptions_attribute = (Exceptions_attribute) theEObject;
			T result = caseExceptions_attribute(exceptions_attribute);
			if (result == null)
				result = caseInterfaceMethod_attribute_info(exceptions_attribute);
			if (result == null)
				result = caseAbstractAttribute_info(exceptions_attribute);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.INTERFACE_CODE_ATTRIBUTE: {
			InterfaceCode_attribute interfaceCode_attribute = (InterfaceCode_attribute) theEObject;
			T result = caseInterfaceCode_attribute(interfaceCode_attribute);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.INNER_CLASSES_ATTRIBUTE: {
			InnerClasses_attribute innerClasses_attribute = (InnerClasses_attribute) theEObject;
			T result = caseInnerClasses_attribute(innerClasses_attribute);
			if (result == null)
				result = caseInterfaceClass_attribute_info(innerClasses_attribute);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.CLASS_MEMBER: {
			Class_member class_member = (Class_member) theEObject;
			T result = caseClass_member(class_member);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.SYNTHETIC_ATTRIBUTE: {
			Synthetic_attribute synthetic_attribute = (Synthetic_attribute) theEObject;
			T result = caseSynthetic_attribute(synthetic_attribute);
			if (result == null)
				result = caseAbstractAttribute_info(synthetic_attribute);
			if (result == null)
				result = caseInterfaceClass_attribute_info(synthetic_attribute);
			if (result == null)
				result = caseInterfaceField_attribute_info(synthetic_attribute);
			if (result == null)
				result = caseInterfaceMethod_attribute_info(synthetic_attribute);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.SIGNATURE_ATTRIBUTE: {
			Signature_attribute signature_attribute = (Signature_attribute) theEObject;
			T result = caseSignature_attribute(signature_attribute);
			if (result == null)
				result = caseInterfaceClass_attribute_info(signature_attribute);
			if (result == null)
				result = caseInterfaceField_attribute_info(signature_attribute);
			if (result == null)
				result = caseInterfaceMethod_attribute_info(signature_attribute);
			if (result == null)
				result = caseAbstractAttribute_info(signature_attribute);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case EcoregraphPackage.NEW_ECLASS54: {
			NewEClass54 newEClass54 = (NewEClass54) theEObject;
			T result = caseNewEClass54(newEClass54);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CONSTANT Method Handle Info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CONSTANT Method Handle Info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCONSTANT_MethodHandle_Info(CONSTANT_MethodHandle_Info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CONSTANT Fieldref info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CONSTANT Fieldref info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCONSTANT_Fieldref_info(CONSTANT_Fieldref_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CONSTANT Methodref info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CONSTANT Methodref info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCONSTANT_Methodref_info(CONSTANT_Methodref_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CONSTANT Interface Methodref info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CONSTANT Interface Methodref info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCONSTANT_InterfaceMethodref_info(CONSTANT_InterfaceMethodref_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CONSTANT Class info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CONSTANT Class info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCONSTANT_Class_info(CONSTANT_Class_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CONSTANT Name And Type info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CONSTANT Name And Type info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCONSTANT_NameAndType_info(CONSTANT_NameAndType_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CONSTANT Utf8 info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CONSTANT Utf8 info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCONSTANT_Utf8_info(CONSTANT_Utf8_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CONSTANT Method Type info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CONSTANT Method Type info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCONSTANT_MethodType_info(CONSTANT_MethodType_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CONSTANT String info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CONSTANT String info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCONSTANT_String_info(CONSTANT_String_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CONSTANT Invoke Dynamic info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CONSTANT Invoke Dynamic info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCONSTANT_InvokeDynamic_info(CONSTANT_InvokeDynamic_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Field info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Field info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseField_info(Field_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Method info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Method info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMethod_info(Method_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Cp info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Cp info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractCp_info(AbstractCp_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interface CONSTANT Ref info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interface CONSTANT Ref info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterfaceCONSTANT_Ref_info(InterfaceCONSTANT_Ref_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CONSTANT Integer info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CONSTANT Integer info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCONSTANT_Integer_info(CONSTANT_Integer_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CONSTANT Float info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CONSTANT Float info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCONSTANT_Float_info(CONSTANT_Float_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CONSTANT Long info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CONSTANT Long info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCONSTANT_Long_info(CONSTANT_Long_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CONSTANT Double info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CONSTANT Double info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCONSTANT_Double_info(CONSTANT_Double_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class File</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class File</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassFile(ClassFile object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Attribute info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Attribute info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractAttribute_info(AbstractAttribute_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interface Method attribute info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interface Method attribute info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterfaceMethod_attribute_info(InterfaceMethod_attribute_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interface Field attribute info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interface Field attribute info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterfaceField_attribute_info(InterfaceField_attribute_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interface Class attribute info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interface Class attribute info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterfaceClass_attribute_info(InterfaceClass_attribute_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constant Value attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constant Value attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstantValue_attribute(ConstantValue_attribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interface CONSTANT Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interface CONSTANT Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterfaceCONSTANT_Value(InterfaceCONSTANT_Value object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Code attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Code attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCode_attribute(Code_attribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>exception table entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>exception table entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseexception_table_entry(exception_table_entry object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Stack Map Table attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Stack Map Table attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStackMapTable_attribute(StackMapTable_attribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Verification type info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Verification type info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractVerification_type_info(AbstractVerification_type_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Top variable info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Top variable info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTop_variable_info(Top_variable_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Integer variable info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer variable info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInteger_variable_info(Integer_variable_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Float variable info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Float variable info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFloat_variable_info(Float_variable_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Null variable info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Null variable info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNull_variable_info(Null_variable_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uninitialized This variable info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uninitialized This variable info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUninitializedThis_variable_info(UninitializedThis_variable_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object variable info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object variable info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObject_variable_info(Object_variable_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>New EClass36</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>New EClass36</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNewEClass36(NewEClass36 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uninitialized variable info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uninitialized variable info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUninitialized_variable_info(Uninitialized_variable_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Long variable info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Long variable info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLong_variable_info(Long_variable_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Double variable info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Double variable info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDouble_variable_info(Double_variable_info object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Stack map frame</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Stack map frame</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractStack_map_frame(AbstractStack_map_frame object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>same frame</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>same frame</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casesame_frame(same_frame object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>same locals 1stack item frame</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>same locals 1stack item frame</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casesame_locals_1_stack_item_frame(same_locals_1_stack_item_frame object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>same locals 1stack item frame extended</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>same locals 1stack item frame extended</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casesame_locals_1_stack_item_frame_extended(same_locals_1_stack_item_frame_extended object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>same frame extended</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>same frame extended</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casesame_frame_extended(same_frame_extended object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>chop frame</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>chop frame</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casechop_frame(chop_frame object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>append frame</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>append frame</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseappend_frame(append_frame object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>full frame</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>full frame</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casefull_frame(full_frame object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exceptions attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exceptions attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExceptions_attribute(Exceptions_attribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interface Code attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interface Code attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterfaceCode_attribute(InterfaceCode_attribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inner Classes attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inner Classes attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInnerClasses_attribute(InnerClasses_attribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class member</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class member</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClass_member(Class_member object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Synthetic attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Synthetic attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSynthetic_attribute(Synthetic_attribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signature attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signature attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignature_attribute(Signature_attribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>New EClass54</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>New EClass54</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNewEClass54(NewEClass54 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //EcoregraphSwitch
