/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CONSTANT Method Type info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.CONSTANT_MethodType_info#getMethod_descriptor <em>Method descriptor</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_MethodType_info()
 * @model
 * @generated
 */
public interface CONSTANT_MethodType_info extends AbstractCp_info {
	/**
	 * Returns the value of the '<em><b>Method descriptor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method descriptor</em>' reference.
	 * @see #setMethod_descriptor(CONSTANT_Utf8_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_MethodType_info_Method_descriptor()
	 * @model required="true"
	 * @generated
	 */
	CONSTANT_Utf8_info getMethod_descriptor();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.CONSTANT_MethodType_info#getMethod_descriptor <em>Method descriptor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method descriptor</em>' reference.
	 * @see #getMethod_descriptor()
	 * @generated
	 */
	void setMethod_descriptor(CONSTANT_Utf8_info value);

} // CONSTANT_MethodType_info
