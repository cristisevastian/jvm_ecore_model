/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CONSTANT Methodref info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.CONSTANT_Methodref_info#getConstant_class_info <em>Constant class info</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.CONSTANT_Methodref_info#getConstant_nameandtype_info <em>Constant nameandtype info</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_Methodref_info()
 * @model
 * @generated
 */
public interface CONSTANT_Methodref_info extends InterfaceCONSTANT_Ref_info, AbstractCp_info {
	/**
	 * Returns the value of the '<em><b>Constant class info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant class info</em>' reference.
	 * @see #setConstant_class_info(CONSTANT_Class_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_Methodref_info_Constant_class_info()
	 * @model required="true"
	 * @generated
	 */
	CONSTANT_Class_info getConstant_class_info();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Methodref_info#getConstant_class_info <em>Constant class info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant class info</em>' reference.
	 * @see #getConstant_class_info()
	 * @generated
	 */
	void setConstant_class_info(CONSTANT_Class_info value);

	/**
	 * Returns the value of the '<em><b>Constant nameandtype info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant nameandtype info</em>' reference.
	 * @see #setConstant_nameandtype_info(CONSTANT_NameAndType_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_Methodref_info_Constant_nameandtype_info()
	 * @model required="true"
	 * @generated
	 */
	CONSTANT_NameAndType_info getConstant_nameandtype_info();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Methodref_info#getConstant_nameandtype_info <em>Constant nameandtype info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant nameandtype info</em>' reference.
	 * @see #getConstant_nameandtype_info()
	 * @generated
	 */
	void setConstant_nameandtype_info(CONSTANT_NameAndType_info value);

} // CONSTANT_Methodref_info
