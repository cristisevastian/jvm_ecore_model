/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CONSTANT String info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.CONSTANT_String_info#getConstant_utf8_info <em>Constant utf8 info</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_String_info()
 * @model
 * @generated
 */
public interface CONSTANT_String_info extends AbstractCp_info, InterfaceCONSTANT_Value {
	/**
	 * Returns the value of the '<em><b>Constant utf8 info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant utf8 info</em>' reference.
	 * @see #setConstant_utf8_info(CONSTANT_Utf8_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_String_info_Constant_utf8_info()
	 * @model required="true"
	 * @generated
	 */
	CONSTANT_Utf8_info getConstant_utf8_info();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.CONSTANT_String_info#getConstant_utf8_info <em>Constant utf8 info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant utf8 info</em>' reference.
	 * @see #getConstant_utf8_info()
	 * @generated
	 */
	void setConstant_utf8_info(CONSTANT_Utf8_info value);

} // CONSTANT_String_info
