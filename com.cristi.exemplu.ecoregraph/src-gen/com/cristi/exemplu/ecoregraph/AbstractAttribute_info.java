/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Attribute info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.AbstractAttribute_info#getAttribute_length <em>Attribute length</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.AbstractAttribute_info#getAttribute_name <em>Attribute name</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getAbstractAttribute_info()
 * @model abstract="true"
 * @generated
 */
public interface AbstractAttribute_info extends EObject {
	/**
	 * Returns the value of the '<em><b>Attribute length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute length</em>' attribute.
	 * @see #setAttribute_length(long)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getAbstractAttribute_info_Attribute_length()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 * @generated
	 */
	long getAttribute_length();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.AbstractAttribute_info#getAttribute_length <em>Attribute length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute length</em>' attribute.
	 * @see #getAttribute_length()
	 * @generated
	 */
	void setAttribute_length(long value);

	/**
	 * Returns the value of the '<em><b>Attribute name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute name</em>' reference.
	 * @see #setAttribute_name(CONSTANT_Utf8_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getAbstractAttribute_info_Attribute_name()
	 * @model required="true"
	 * @generated
	 */
	CONSTANT_Utf8_info getAttribute_name();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.AbstractAttribute_info#getAttribute_name <em>Attribute name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute name</em>' reference.
	 * @see #getAttribute_name()
	 * @generated
	 */
	void setAttribute_name(CONSTANT_Utf8_info value);

} // AbstractAttribute_info
