/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CONSTANT Name And Type info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info#getName <em>Name</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info#getField_method_descriptor <em>Field method descriptor</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_NameAndType_info()
 * @model
 * @generated
 */
public interface CONSTANT_NameAndType_info extends AbstractCp_info {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' reference.
	 * @see #setName(CONSTANT_Utf8_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_NameAndType_info_Name()
	 * @model required="true"
	 * @generated
	 */
	CONSTANT_Utf8_info getName();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info#getName <em>Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' reference.
	 * @see #getName()
	 * @generated
	 */
	void setName(CONSTANT_Utf8_info value);

	/**
	 * Returns the value of the '<em><b>Field method descriptor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Field method descriptor</em>' reference.
	 * @see #setField_method_descriptor(CONSTANT_Utf8_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_NameAndType_info_Field_method_descriptor()
	 * @model required="true"
	 * @generated
	 */
	CONSTANT_Utf8_info getField_method_descriptor();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info#getField_method_descriptor <em>Field method descriptor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Field method descriptor</em>' reference.
	 * @see #getField_method_descriptor()
	 * @generated
	 */
	void setField_method_descriptor(CONSTANT_Utf8_info value);

} // CONSTANT_NameAndType_info
