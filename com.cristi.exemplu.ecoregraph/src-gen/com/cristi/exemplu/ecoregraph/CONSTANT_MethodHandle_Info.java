/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CONSTANT Method Handle Info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.CONSTANT_MethodHandle_Info#getReference_kind <em>Reference kind</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.CONSTANT_MethodHandle_Info#getConstant_ref_info <em>Constant ref info</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_MethodHandle_Info()
 * @model
 * @generated
 */
public interface CONSTANT_MethodHandle_Info extends AbstractCp_info {
	/**
	 * Returns the value of the '<em><b>Reference kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference kind</em>' attribute.
	 * @see #setReference_kind(short)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_MethodHandle_Info_Reference_kind()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedByte"
	 * @generated
	 */
	short getReference_kind();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.CONSTANT_MethodHandle_Info#getReference_kind <em>Reference kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference kind</em>' attribute.
	 * @see #getReference_kind()
	 * @generated
	 */
	void setReference_kind(short value);

	/**
	 * Returns the value of the '<em><b>Constant ref info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant ref info</em>' reference.
	 * @see #setConstant_ref_info(InterfaceCONSTANT_Ref_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_MethodHandle_Info_Constant_ref_info()
	 * @model required="true"
	 * @generated
	 */
	InterfaceCONSTANT_Ref_info getConstant_ref_info();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.CONSTANT_MethodHandle_Info#getConstant_ref_info <em>Constant ref info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant ref info</em>' reference.
	 * @see #getConstant_ref_info()
	 * @generated
	 */
	void setConstant_ref_info(InterfaceCONSTANT_Ref_info value);

} // CONSTANT_MethodHandle_Info
