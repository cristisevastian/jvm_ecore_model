/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface Class attribute info</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getInterfaceClass_attribute_info()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface InterfaceClass_attribute_info extends EObject {
} // InterfaceClass_attribute_info
