/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signature attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Signature_attribute#getSignature <em>Signature</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getSignature_attribute()
 * @model
 * @generated
 */
public interface Signature_attribute extends InterfaceClass_attribute_info, InterfaceField_attribute_info,
		InterfaceMethod_attribute_info, AbstractAttribute_info {
	/**
	 * Returns the value of the '<em><b>Signature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signature</em>' reference.
	 * @see #setSignature(CONSTANT_Utf8_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getSignature_attribute_Signature()
	 * @model required="true"
	 * @generated
	 */
	CONSTANT_Utf8_info getSignature();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Signature_attribute#getSignature <em>Signature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signature</em>' reference.
	 * @see #getSignature()
	 * @generated
	 */
	void setSignature(CONSTANT_Utf8_info value);

} // Signature_attribute
