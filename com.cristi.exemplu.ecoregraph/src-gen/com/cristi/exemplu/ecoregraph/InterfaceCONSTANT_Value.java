/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface CONSTANT Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getInterfaceCONSTANT_Value()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface InterfaceCONSTANT_Value extends EObject {
} // InterfaceCONSTANT_Value
