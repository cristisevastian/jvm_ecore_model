/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CONSTANT Class info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.CONSTANT_Class_info#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_Class_info()
 * @model
 * @generated
 */
public interface CONSTANT_Class_info extends AbstractCp_info {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' reference.
	 * @see #setName(CONSTANT_Utf8_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_Class_info_Name()
	 * @model required="true"
	 * @generated
	 */
	CONSTANT_Utf8_info getName();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Class_info#getName <em>Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' reference.
	 * @see #getName()
	 * @generated
	 */
	void setName(CONSTANT_Utf8_info value);

} // CONSTANT_Class_info
