/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class member</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Class_member#getInner_class_access_flags <em>Inner class access flags</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Class_member#getInner_class_info <em>Inner class info</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Class_member#getOuter_class_info <em>Outer class info</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Class_member#getInner_name <em>Inner name</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClass_member()
 * @model
 * @generated
 */
public interface Class_member extends EObject {
	/**
	 * Returns the value of the '<em><b>Inner class access flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inner class access flags</em>' attribute.
	 * @see #setInner_class_access_flags(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClass_member_Inner_class_access_flags()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getInner_class_access_flags();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Class_member#getInner_class_access_flags <em>Inner class access flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inner class access flags</em>' attribute.
	 * @see #getInner_class_access_flags()
	 * @generated
	 */
	void setInner_class_access_flags(int value);

	/**
	 * Returns the value of the '<em><b>Inner class info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inner class info</em>' reference.
	 * @see #setInner_class_info(CONSTANT_Class_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClass_member_Inner_class_info()
	 * @model required="true"
	 * @generated
	 */
	CONSTANT_Class_info getInner_class_info();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Class_member#getInner_class_info <em>Inner class info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inner class info</em>' reference.
	 * @see #getInner_class_info()
	 * @generated
	 */
	void setInner_class_info(CONSTANT_Class_info value);

	/**
	 * Returns the value of the '<em><b>Outer class info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outer class info</em>' reference.
	 * @see #setOuter_class_info(CONSTANT_Class_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClass_member_Outer_class_info()
	 * @model required="true"
	 * @generated
	 */
	CONSTANT_Class_info getOuter_class_info();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Class_member#getOuter_class_info <em>Outer class info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Outer class info</em>' reference.
	 * @see #getOuter_class_info()
	 * @generated
	 */
	void setOuter_class_info(CONSTANT_Class_info value);

	/**
	 * Returns the value of the '<em><b>Inner name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inner name</em>' reference.
	 * @see #setInner_name(CONSTANT_Utf8_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClass_member_Inner_name()
	 * @model required="true"
	 * @generated
	 */
	CONSTANT_Utf8_info getInner_name();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Class_member#getInner_name <em>Inner name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inner name</em>' reference.
	 * @see #getInner_name()
	 * @generated
	 */
	void setInner_name(CONSTANT_Utf8_info value);

} // Class_member
