/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface CONSTANT Ref info</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getInterfaceCONSTANT_Ref_info()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface InterfaceCONSTANT_Ref_info extends EObject {
} // InterfaceCONSTANT_Ref_info
