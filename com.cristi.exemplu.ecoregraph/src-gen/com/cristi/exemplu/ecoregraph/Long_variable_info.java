/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Long variable info</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getLong_variable_info()
 * @model
 * @generated
 */
public interface Long_variable_info extends AbstractVerification_type_info {
} // Long_variable_info
