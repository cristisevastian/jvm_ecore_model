/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface Code attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getInterfaceCode_attribute()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface InterfaceCode_attribute extends EObject {
} // InterfaceCode_attribute
