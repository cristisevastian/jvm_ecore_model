/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class File</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getMagic <em>Magic</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getMinor_version <em>Minor version</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getMajor_version <em>Major version</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getConstant_pool_count <em>Constant pool count</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getAccess_flags <em>Access flags</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getInterfaces_count <em>Interfaces count</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getFields_count <em>Fields count</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getMethods_count <em>Methods count</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getAttributes_count <em>Attributes count</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getCp_info <em>Cp info</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getField_info <em>Field info</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getMethod_info <em>Method info</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getSuper_class <em>Super class</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getThis_class <em>This class</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.ClassFile#getInterfaceclass_attribute_info <em>Interfaceclass attribute info</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile()
 * @model
 * @generated
 */
public interface ClassFile extends EObject {
	/**
	 * Returns the value of the '<em><b>Magic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Magic</em>' attribute.
	 * @see #setMagic(long)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_Magic()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 * @generated
	 */
	long getMagic();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.ClassFile#getMagic <em>Magic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Magic</em>' attribute.
	 * @see #getMagic()
	 * @generated
	 */
	void setMagic(long value);

	/**
	 * Returns the value of the '<em><b>Minor version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Minor version</em>' attribute.
	 * @see #setMinor_version(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_Minor_version()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getMinor_version();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.ClassFile#getMinor_version <em>Minor version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Minor version</em>' attribute.
	 * @see #getMinor_version()
	 * @generated
	 */
	void setMinor_version(int value);

	/**
	 * Returns the value of the '<em><b>Major version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Major version</em>' attribute.
	 * @see #setMajor_version(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_Major_version()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getMajor_version();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.ClassFile#getMajor_version <em>Major version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Major version</em>' attribute.
	 * @see #getMajor_version()
	 * @generated
	 */
	void setMajor_version(int value);

	/**
	 * Returns the value of the '<em><b>Constant pool count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant pool count</em>' attribute.
	 * @see #setConstant_pool_count(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_Constant_pool_count()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getConstant_pool_count();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.ClassFile#getConstant_pool_count <em>Constant pool count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant pool count</em>' attribute.
	 * @see #getConstant_pool_count()
	 * @generated
	 */
	void setConstant_pool_count(int value);

	/**
	 * Returns the value of the '<em><b>Access flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access flags</em>' attribute.
	 * @see #setAccess_flags(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_Access_flags()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getAccess_flags();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.ClassFile#getAccess_flags <em>Access flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access flags</em>' attribute.
	 * @see #getAccess_flags()
	 * @generated
	 */
	void setAccess_flags(int value);

	/**
	 * Returns the value of the '<em><b>Interfaces count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interfaces count</em>' attribute.
	 * @see #setInterfaces_count(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_Interfaces_count()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getInterfaces_count();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.ClassFile#getInterfaces_count <em>Interfaces count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interfaces count</em>' attribute.
	 * @see #getInterfaces_count()
	 * @generated
	 */
	void setInterfaces_count(int value);

	/**
	 * Returns the value of the '<em><b>Fields count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fields count</em>' attribute.
	 * @see #setFields_count(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_Fields_count()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getFields_count();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.ClassFile#getFields_count <em>Fields count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fields count</em>' attribute.
	 * @see #getFields_count()
	 * @generated
	 */
	void setFields_count(int value);

	/**
	 * Returns the value of the '<em><b>Methods count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Methods count</em>' attribute.
	 * @see #setMethods_count(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_Methods_count()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getMethods_count();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.ClassFile#getMethods_count <em>Methods count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Methods count</em>' attribute.
	 * @see #getMethods_count()
	 * @generated
	 */
	void setMethods_count(int value);

	/**
	 * Returns the value of the '<em><b>Attributes count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes count</em>' attribute.
	 * @see #setAttributes_count(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_Attributes_count()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getAttributes_count();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.ClassFile#getAttributes_count <em>Attributes count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attributes count</em>' attribute.
	 * @see #getAttributes_count()
	 * @generated
	 */
	void setAttributes_count(int value);

	/**
	 * Returns the value of the '<em><b>Cp info</b></em>' containment reference list.
	 * The list contents are of type {@link com.cristi.exemplu.ecoregraph.AbstractCp_info}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cp info</em>' containment reference list.
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_Cp_info()
	 * @model containment="true"
	 * @generated
	 */
	EList<AbstractCp_info> getCp_info();

	/**
	 * Returns the value of the '<em><b>Field info</b></em>' containment reference list.
	 * The list contents are of type {@link com.cristi.exemplu.ecoregraph.Field_info}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Field info</em>' containment reference list.
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_Field_info()
	 * @model containment="true"
	 * @generated
	 */
	EList<Field_info> getField_info();

	/**
	 * Returns the value of the '<em><b>Method info</b></em>' containment reference list.
	 * The list contents are of type {@link com.cristi.exemplu.ecoregraph.Method_info}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method info</em>' containment reference list.
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_Method_info()
	 * @model containment="true"
	 * @generated
	 */
	EList<Method_info> getMethod_info();

	/**
	 * Returns the value of the '<em><b>Super class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super class</em>' reference.
	 * @see #setSuper_class(CONSTANT_Class_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_Super_class()
	 * @model
	 * @generated
	 */
	CONSTANT_Class_info getSuper_class();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.ClassFile#getSuper_class <em>Super class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Super class</em>' reference.
	 * @see #getSuper_class()
	 * @generated
	 */
	void setSuper_class(CONSTANT_Class_info value);

	/**
	 * Returns the value of the '<em><b>Interfaces</b></em>' reference list.
	 * The list contents are of type {@link com.cristi.exemplu.ecoregraph.CONSTANT_Class_info}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interfaces</em>' reference list.
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_Interfaces()
	 * @model
	 * @generated
	 */
	EList<CONSTANT_Class_info> getInterfaces();

	/**
	 * Returns the value of the '<em><b>This class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>This class</em>' reference.
	 * @see #setThis_class(CONSTANT_Class_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_This_class()
	 * @model required="true"
	 * @generated
	 */
	CONSTANT_Class_info getThis_class();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.ClassFile#getThis_class <em>This class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>This class</em>' reference.
	 * @see #getThis_class()
	 * @generated
	 */
	void setThis_class(CONSTANT_Class_info value);

	/**
	 * Returns the value of the '<em><b>Interfaceclass attribute info</b></em>' reference list.
	 * The list contents are of type {@link com.cristi.exemplu.ecoregraph.InterfaceClass_attribute_info}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interfaceclass attribute info</em>' reference list.
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getClassFile_Interfaceclass_attribute_info()
	 * @model
	 * @generated
	 */
	EList<InterfaceClass_attribute_info> getInterfaceclass_attribute_info();

} // ClassFile
