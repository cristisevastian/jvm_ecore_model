/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.Class_member;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.InnerClasses_attribute;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Inner Classes attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.InnerClasses_attributeImpl#getNumber_of_classes <em>Number of classes</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.InnerClasses_attributeImpl#getClasses <em>Classes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InnerClasses_attributeImpl extends MinimalEObjectImpl.Container implements InnerClasses_attribute {
	/**
	 * The default value of the '{@link #getNumber_of_classes() <em>Number of classes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber_of_classes()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_CLASSES_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumber_of_classes() <em>Number of classes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber_of_classes()
	 * @generated
	 * @ordered
	 */
	protected int number_of_classes = NUMBER_OF_CLASSES_EDEFAULT;

	/**
	 * The cached value of the '{@link #getClasses() <em>Classes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<Class_member> classes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InnerClasses_attributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.INNER_CLASSES_ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumber_of_classes() {
		return number_of_classes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumber_of_classes(int newNumber_of_classes) {
		int oldNumber_of_classes = number_of_classes;
		number_of_classes = newNumber_of_classes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.INNER_CLASSES_ATTRIBUTE__NUMBER_OF_CLASSES, oldNumber_of_classes,
					number_of_classes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Class_member> getClasses() {
		if (classes == null) {
			classes = new EObjectContainmentEList<Class_member>(Class_member.class, this,
					EcoregraphPackage.INNER_CLASSES_ATTRIBUTE__CLASSES);
		}
		return classes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case EcoregraphPackage.INNER_CLASSES_ATTRIBUTE__CLASSES:
			return ((InternalEList<?>) getClasses()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.INNER_CLASSES_ATTRIBUTE__NUMBER_OF_CLASSES:
			return getNumber_of_classes();
		case EcoregraphPackage.INNER_CLASSES_ATTRIBUTE__CLASSES:
			return getClasses();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.INNER_CLASSES_ATTRIBUTE__NUMBER_OF_CLASSES:
			setNumber_of_classes((Integer) newValue);
			return;
		case EcoregraphPackage.INNER_CLASSES_ATTRIBUTE__CLASSES:
			getClasses().clear();
			getClasses().addAll((Collection<? extends Class_member>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.INNER_CLASSES_ATTRIBUTE__NUMBER_OF_CLASSES:
			setNumber_of_classes(NUMBER_OF_CLASSES_EDEFAULT);
			return;
		case EcoregraphPackage.INNER_CLASSES_ATTRIBUTE__CLASSES:
			getClasses().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.INNER_CLASSES_ATTRIBUTE__NUMBER_OF_CLASSES:
			return number_of_classes != NUMBER_OF_CLASSES_EDEFAULT;
		case EcoregraphPackage.INNER_CLASSES_ATTRIBUTE__CLASSES:
			return classes != null && !classes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (number_of_classes: ");
		result.append(number_of_classes);
		result.append(')');
		return result.toString();
	}

} //InnerClasses_attributeImpl
