/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.CONSTANT_InvokeDynamic_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CONSTANT Invoke Dynamic info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_InvokeDynamic_infoImpl#getConstant_nameandtype_info <em>Constant nameandtype info</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CONSTANT_InvokeDynamic_infoImpl extends AbstractCp_infoImpl implements CONSTANT_InvokeDynamic_info {
	/**
	 * The cached value of the '{@link #getConstant_nameandtype_info() <em>Constant nameandtype info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant_nameandtype_info()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_NameAndType_info constant_nameandtype_info;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CONSTANT_InvokeDynamic_infoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.CONSTANT_INVOKE_DYNAMIC_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_NameAndType_info getConstant_nameandtype_info() {
		if (constant_nameandtype_info != null && constant_nameandtype_info.eIsProxy()) {
			InternalEObject oldConstant_nameandtype_info = (InternalEObject) constant_nameandtype_info;
			constant_nameandtype_info = (CONSTANT_NameAndType_info) eResolveProxy(oldConstant_nameandtype_info);
			if (constant_nameandtype_info != oldConstant_nameandtype_info) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.CONSTANT_INVOKE_DYNAMIC_INFO__CONSTANT_NAMEANDTYPE_INFO,
							oldConstant_nameandtype_info, constant_nameandtype_info));
			}
		}
		return constant_nameandtype_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_NameAndType_info basicGetConstant_nameandtype_info() {
		return constant_nameandtype_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstant_nameandtype_info(CONSTANT_NameAndType_info newConstant_nameandtype_info) {
		CONSTANT_NameAndType_info oldConstant_nameandtype_info = constant_nameandtype_info;
		constant_nameandtype_info = newConstant_nameandtype_info;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.CONSTANT_INVOKE_DYNAMIC_INFO__CONSTANT_NAMEANDTYPE_INFO,
					oldConstant_nameandtype_info, constant_nameandtype_info));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_INVOKE_DYNAMIC_INFO__CONSTANT_NAMEANDTYPE_INFO:
			if (resolve)
				return getConstant_nameandtype_info();
			return basicGetConstant_nameandtype_info();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_INVOKE_DYNAMIC_INFO__CONSTANT_NAMEANDTYPE_INFO:
			setConstant_nameandtype_info((CONSTANT_NameAndType_info) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_INVOKE_DYNAMIC_INFO__CONSTANT_NAMEANDTYPE_INFO:
			setConstant_nameandtype_info((CONSTANT_NameAndType_info) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_INVOKE_DYNAMIC_INFO__CONSTANT_NAMEANDTYPE_INFO:
			return constant_nameandtype_info != null;
		}
		return super.eIsSet(featureID);
	}

} //CONSTANT_InvokeDynamic_infoImpl
