/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.AbstractVerification_type_info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame_extended;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>same locals 1stack item frame extended</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.same_locals_1_stack_item_frame_extendedImpl#getOffset_delta <em>Offset delta</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.same_locals_1_stack_item_frame_extendedImpl#getStack <em>Stack</em>}</li>
 * </ul>
 *
 * @generated
 */
public class same_locals_1_stack_item_frame_extendedImpl extends AbstractStack_map_frameImpl
		implements same_locals_1_stack_item_frame_extended {
	/**
	 * The default value of the '{@link #getOffset_delta() <em>Offset delta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset_delta()
	 * @generated
	 * @ordered
	 */
	protected static final int OFFSET_DELTA_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getOffset_delta() <em>Offset delta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset_delta()
	 * @generated
	 * @ordered
	 */
	protected int offset_delta = OFFSET_DELTA_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStack() <em>Stack</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStack()
	 * @generated
	 * @ordered
	 */
	protected AbstractVerification_type_info stack;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected same_locals_1_stack_item_frame_extendedImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOffset_delta() {
		return offset_delta;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOffset_delta(int newOffset_delta) {
		int oldOffset_delta = offset_delta;
		offset_delta = newOffset_delta;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__OFFSET_DELTA, oldOffset_delta,
					offset_delta));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractVerification_type_info getStack() {
		return stack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStack(AbstractVerification_type_info newStack, NotificationChain msgs) {
		AbstractVerification_type_info oldStack = stack;
		stack = newStack;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__STACK, oldStack, newStack);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStack(AbstractVerification_type_info newStack) {
		if (newStack != stack) {
			NotificationChain msgs = null;
			if (stack != null)
				msgs = ((InternalEObject) stack).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__STACK, null,
						msgs);
			if (newStack != null)
				msgs = ((InternalEObject) newStack).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__STACK, null,
						msgs);
			msgs = basicSetStack(newStack, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__STACK, newStack, newStack));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__STACK:
			return basicSetStack(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__OFFSET_DELTA:
			return getOffset_delta();
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__STACK:
			return getStack();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__OFFSET_DELTA:
			setOffset_delta((Integer) newValue);
			return;
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__STACK:
			setStack((AbstractVerification_type_info) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__OFFSET_DELTA:
			setOffset_delta(OFFSET_DELTA_EDEFAULT);
			return;
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__STACK:
			setStack((AbstractVerification_type_info) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__OFFSET_DELTA:
			return offset_delta != OFFSET_DELTA_EDEFAULT;
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__STACK:
			return stack != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (offset_delta: ");
		result.append(offset_delta);
		result.append(')');
		return result.toString();
	}

} //same_locals_1_stack_item_frame_extendedImpl
