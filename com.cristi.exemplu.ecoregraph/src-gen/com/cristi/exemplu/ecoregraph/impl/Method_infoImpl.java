/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.InterfaceMethod_attribute_info;
import com.cristi.exemplu.ecoregraph.Method_info;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Method info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Method_infoImpl#getAccess_flags <em>Access flags</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Method_infoImpl#getAttributes_count <em>Attributes count</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Method_infoImpl#getMethod_descriptor <em>Method descriptor</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Method_infoImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Method_infoImpl#getInterfacemethod_attribute_info <em>Interfacemethod attribute info</em>}</li>
 * </ul>
 *
 * @generated
 */
public class Method_infoImpl extends MinimalEObjectImpl.Container implements Method_info {
	/**
	 * The default value of the '{@link #getAccess_flags() <em>Access flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccess_flags()
	 * @generated
	 * @ordered
	 */
	protected static final int ACCESS_FLAGS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getAccess_flags() <em>Access flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccess_flags()
	 * @generated
	 * @ordered
	 */
	protected int access_flags = ACCESS_FLAGS_EDEFAULT;

	/**
	 * The default value of the '{@link #getAttributes_count() <em>Attributes count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributes_count()
	 * @generated
	 * @ordered
	 */
	protected static final int ATTRIBUTES_COUNT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getAttributes_count() <em>Attributes count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributes_count()
	 * @generated
	 * @ordered
	 */
	protected int attributes_count = ATTRIBUTES_COUNT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMethod_descriptor() <em>Method descriptor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethod_descriptor()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Utf8_info method_descriptor;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Utf8_info name;

	/**
	 * The cached value of the '{@link #getInterfacemethod_attribute_info() <em>Interfacemethod attribute info</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfacemethod_attribute_info()
	 * @generated
	 * @ordered
	 */
	protected EList<InterfaceMethod_attribute_info> interfacemethod_attribute_info;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Method_infoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.METHOD_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAccess_flags() {
		return access_flags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccess_flags(int newAccess_flags) {
		int oldAccess_flags = access_flags;
		access_flags = newAccess_flags;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.METHOD_INFO__ACCESS_FLAGS,
					oldAccess_flags, access_flags));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAttributes_count() {
		return attributes_count;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributes_count(int newAttributes_count) {
		int oldAttributes_count = attributes_count;
		attributes_count = newAttributes_count;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.METHOD_INFO__ATTRIBUTES_COUNT,
					oldAttributes_count, attributes_count));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info getMethod_descriptor() {
		if (method_descriptor != null && method_descriptor.eIsProxy()) {
			InternalEObject oldMethod_descriptor = (InternalEObject) method_descriptor;
			method_descriptor = (CONSTANT_Utf8_info) eResolveProxy(oldMethod_descriptor);
			if (method_descriptor != oldMethod_descriptor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.METHOD_INFO__METHOD_DESCRIPTOR, oldMethod_descriptor, method_descriptor));
			}
		}
		return method_descriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info basicGetMethod_descriptor() {
		return method_descriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethod_descriptor(CONSTANT_Utf8_info newMethod_descriptor) {
		CONSTANT_Utf8_info oldMethod_descriptor = method_descriptor;
		method_descriptor = newMethod_descriptor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.METHOD_INFO__METHOD_DESCRIPTOR,
					oldMethod_descriptor, method_descriptor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info getName() {
		if (name != null && name.eIsProxy()) {
			InternalEObject oldName = (InternalEObject) name;
			name = (CONSTANT_Utf8_info) eResolveProxy(oldName);
			if (name != oldName) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EcoregraphPackage.METHOD_INFO__NAME,
							oldName, name));
			}
		}
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info basicGetName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(CONSTANT_Utf8_info newName) {
		CONSTANT_Utf8_info oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.METHOD_INFO__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterfaceMethod_attribute_info> getInterfacemethod_attribute_info() {
		if (interfacemethod_attribute_info == null) {
			interfacemethod_attribute_info = new EObjectContainmentEList<InterfaceMethod_attribute_info>(
					InterfaceMethod_attribute_info.class, this,
					EcoregraphPackage.METHOD_INFO__INTERFACEMETHOD_ATTRIBUTE_INFO);
		}
		return interfacemethod_attribute_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case EcoregraphPackage.METHOD_INFO__INTERFACEMETHOD_ATTRIBUTE_INFO:
			return ((InternalEList<?>) getInterfacemethod_attribute_info()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.METHOD_INFO__ACCESS_FLAGS:
			return getAccess_flags();
		case EcoregraphPackage.METHOD_INFO__ATTRIBUTES_COUNT:
			return getAttributes_count();
		case EcoregraphPackage.METHOD_INFO__METHOD_DESCRIPTOR:
			if (resolve)
				return getMethod_descriptor();
			return basicGetMethod_descriptor();
		case EcoregraphPackage.METHOD_INFO__NAME:
			if (resolve)
				return getName();
			return basicGetName();
		case EcoregraphPackage.METHOD_INFO__INTERFACEMETHOD_ATTRIBUTE_INFO:
			return getInterfacemethod_attribute_info();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.METHOD_INFO__ACCESS_FLAGS:
			setAccess_flags((Integer) newValue);
			return;
		case EcoregraphPackage.METHOD_INFO__ATTRIBUTES_COUNT:
			setAttributes_count((Integer) newValue);
			return;
		case EcoregraphPackage.METHOD_INFO__METHOD_DESCRIPTOR:
			setMethod_descriptor((CONSTANT_Utf8_info) newValue);
			return;
		case EcoregraphPackage.METHOD_INFO__NAME:
			setName((CONSTANT_Utf8_info) newValue);
			return;
		case EcoregraphPackage.METHOD_INFO__INTERFACEMETHOD_ATTRIBUTE_INFO:
			getInterfacemethod_attribute_info().clear();
			getInterfacemethod_attribute_info().addAll((Collection<? extends InterfaceMethod_attribute_info>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.METHOD_INFO__ACCESS_FLAGS:
			setAccess_flags(ACCESS_FLAGS_EDEFAULT);
			return;
		case EcoregraphPackage.METHOD_INFO__ATTRIBUTES_COUNT:
			setAttributes_count(ATTRIBUTES_COUNT_EDEFAULT);
			return;
		case EcoregraphPackage.METHOD_INFO__METHOD_DESCRIPTOR:
			setMethod_descriptor((CONSTANT_Utf8_info) null);
			return;
		case EcoregraphPackage.METHOD_INFO__NAME:
			setName((CONSTANT_Utf8_info) null);
			return;
		case EcoregraphPackage.METHOD_INFO__INTERFACEMETHOD_ATTRIBUTE_INFO:
			getInterfacemethod_attribute_info().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.METHOD_INFO__ACCESS_FLAGS:
			return access_flags != ACCESS_FLAGS_EDEFAULT;
		case EcoregraphPackage.METHOD_INFO__ATTRIBUTES_COUNT:
			return attributes_count != ATTRIBUTES_COUNT_EDEFAULT;
		case EcoregraphPackage.METHOD_INFO__METHOD_DESCRIPTOR:
			return method_descriptor != null;
		case EcoregraphPackage.METHOD_INFO__NAME:
			return name != null;
		case EcoregraphPackage.METHOD_INFO__INTERFACEMETHOD_ATTRIBUTE_INFO:
			return interfacemethod_attribute_info != null && !interfacemethod_attribute_info.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (access_flags: ");
		result.append(access_flags);
		result.append(", attributes_count: ");
		result.append(attributes_count);
		result.append(')');
		return result.toString();
	}

} //Method_infoImpl
