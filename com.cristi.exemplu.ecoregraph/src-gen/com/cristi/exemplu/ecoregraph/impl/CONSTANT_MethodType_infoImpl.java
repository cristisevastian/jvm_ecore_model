/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.CONSTANT_MethodType_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CONSTANT Method Type info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_MethodType_infoImpl#getMethod_descriptor <em>Method descriptor</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CONSTANT_MethodType_infoImpl extends AbstractCp_infoImpl implements CONSTANT_MethodType_info {
	/**
	 * The cached value of the '{@link #getMethod_descriptor() <em>Method descriptor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethod_descriptor()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Utf8_info method_descriptor;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CONSTANT_MethodType_infoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.CONSTANT_METHOD_TYPE_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info getMethod_descriptor() {
		if (method_descriptor != null && method_descriptor.eIsProxy()) {
			InternalEObject oldMethod_descriptor = (InternalEObject) method_descriptor;
			method_descriptor = (CONSTANT_Utf8_info) eResolveProxy(oldMethod_descriptor);
			if (method_descriptor != oldMethod_descriptor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.CONSTANT_METHOD_TYPE_INFO__METHOD_DESCRIPTOR, oldMethod_descriptor,
							method_descriptor));
			}
		}
		return method_descriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info basicGetMethod_descriptor() {
		return method_descriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethod_descriptor(CONSTANT_Utf8_info newMethod_descriptor) {
		CONSTANT_Utf8_info oldMethod_descriptor = method_descriptor;
		method_descriptor = newMethod_descriptor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.CONSTANT_METHOD_TYPE_INFO__METHOD_DESCRIPTOR, oldMethod_descriptor,
					method_descriptor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_METHOD_TYPE_INFO__METHOD_DESCRIPTOR:
			if (resolve)
				return getMethod_descriptor();
			return basicGetMethod_descriptor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_METHOD_TYPE_INFO__METHOD_DESCRIPTOR:
			setMethod_descriptor((CONSTANT_Utf8_info) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_METHOD_TYPE_INFO__METHOD_DESCRIPTOR:
			setMethod_descriptor((CONSTANT_Utf8_info) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_METHOD_TYPE_INFO__METHOD_DESCRIPTOR:
			return method_descriptor != null;
		}
		return super.eIsSet(featureID);
	}

} //CONSTANT_MethodType_infoImpl
