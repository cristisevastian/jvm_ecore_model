/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.AbstractCp_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Class_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Fieldref_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CONSTANT Fieldref info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Fieldref_infoImpl#getTag <em>Tag</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Fieldref_infoImpl#getConstant_class_info <em>Constant class info</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Fieldref_infoImpl#getConstant_nameandtype_info <em>Constant nameandtype info</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CONSTANT_Fieldref_infoImpl extends MinimalEObjectImpl.Container implements CONSTANT_Fieldref_info {
	/**
	 * The default value of the '{@link #getTag() <em>Tag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTag()
	 * @generated
	 * @ordered
	 */
	protected static final short TAG_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTag() <em>Tag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTag()
	 * @generated
	 * @ordered
	 */
	protected short tag = TAG_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConstant_class_info() <em>Constant class info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant_class_info()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Class_info constant_class_info;

	/**
	 * The cached value of the '{@link #getConstant_nameandtype_info() <em>Constant nameandtype info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant_nameandtype_info()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_NameAndType_info constant_nameandtype_info;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CONSTANT_Fieldref_infoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.CONSTANT_FIELDREF_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public short getTag() {
		return tag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTag(short newTag) {
		short oldTag = tag;
		tag = newTag;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CONSTANT_FIELDREF_INFO__TAG, oldTag,
					tag));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Class_info getConstant_class_info() {
		if (constant_class_info != null && constant_class_info.eIsProxy()) {
			InternalEObject oldConstant_class_info = (InternalEObject) constant_class_info;
			constant_class_info = (CONSTANT_Class_info) eResolveProxy(oldConstant_class_info);
			if (constant_class_info != oldConstant_class_info) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.CONSTANT_FIELDREF_INFO__CONSTANT_CLASS_INFO, oldConstant_class_info,
							constant_class_info));
			}
		}
		return constant_class_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Class_info basicGetConstant_class_info() {
		return constant_class_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstant_class_info(CONSTANT_Class_info newConstant_class_info) {
		CONSTANT_Class_info oldConstant_class_info = constant_class_info;
		constant_class_info = newConstant_class_info;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.CONSTANT_FIELDREF_INFO__CONSTANT_CLASS_INFO, oldConstant_class_info,
					constant_class_info));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_NameAndType_info getConstant_nameandtype_info() {
		if (constant_nameandtype_info != null && constant_nameandtype_info.eIsProxy()) {
			InternalEObject oldConstant_nameandtype_info = (InternalEObject) constant_nameandtype_info;
			constant_nameandtype_info = (CONSTANT_NameAndType_info) eResolveProxy(oldConstant_nameandtype_info);
			if (constant_nameandtype_info != oldConstant_nameandtype_info) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.CONSTANT_FIELDREF_INFO__CONSTANT_NAMEANDTYPE_INFO,
							oldConstant_nameandtype_info, constant_nameandtype_info));
			}
		}
		return constant_nameandtype_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_NameAndType_info basicGetConstant_nameandtype_info() {
		return constant_nameandtype_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstant_nameandtype_info(CONSTANT_NameAndType_info newConstant_nameandtype_info) {
		CONSTANT_NameAndType_info oldConstant_nameandtype_info = constant_nameandtype_info;
		constant_nameandtype_info = newConstant_nameandtype_info;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.CONSTANT_FIELDREF_INFO__CONSTANT_NAMEANDTYPE_INFO, oldConstant_nameandtype_info,
					constant_nameandtype_info));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_FIELDREF_INFO__TAG:
			return getTag();
		case EcoregraphPackage.CONSTANT_FIELDREF_INFO__CONSTANT_CLASS_INFO:
			if (resolve)
				return getConstant_class_info();
			return basicGetConstant_class_info();
		case EcoregraphPackage.CONSTANT_FIELDREF_INFO__CONSTANT_NAMEANDTYPE_INFO:
			if (resolve)
				return getConstant_nameandtype_info();
			return basicGetConstant_nameandtype_info();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_FIELDREF_INFO__TAG:
			setTag((Short) newValue);
			return;
		case EcoregraphPackage.CONSTANT_FIELDREF_INFO__CONSTANT_CLASS_INFO:
			setConstant_class_info((CONSTANT_Class_info) newValue);
			return;
		case EcoregraphPackage.CONSTANT_FIELDREF_INFO__CONSTANT_NAMEANDTYPE_INFO:
			setConstant_nameandtype_info((CONSTANT_NameAndType_info) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_FIELDREF_INFO__TAG:
			setTag(TAG_EDEFAULT);
			return;
		case EcoregraphPackage.CONSTANT_FIELDREF_INFO__CONSTANT_CLASS_INFO:
			setConstant_class_info((CONSTANT_Class_info) null);
			return;
		case EcoregraphPackage.CONSTANT_FIELDREF_INFO__CONSTANT_NAMEANDTYPE_INFO:
			setConstant_nameandtype_info((CONSTANT_NameAndType_info) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_FIELDREF_INFO__TAG:
			return tag != TAG_EDEFAULT;
		case EcoregraphPackage.CONSTANT_FIELDREF_INFO__CONSTANT_CLASS_INFO:
			return constant_class_info != null;
		case EcoregraphPackage.CONSTANT_FIELDREF_INFO__CONSTANT_NAMEANDTYPE_INFO:
			return constant_nameandtype_info != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AbstractCp_info.class) {
			switch (derivedFeatureID) {
			case EcoregraphPackage.CONSTANT_FIELDREF_INFO__TAG:
				return EcoregraphPackage.ABSTRACT_CP_INFO__TAG;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AbstractCp_info.class) {
			switch (baseFeatureID) {
			case EcoregraphPackage.ABSTRACT_CP_INFO__TAG:
				return EcoregraphPackage.CONSTANT_FIELDREF_INFO__TAG;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (tag: ");
		result.append(tag);
		result.append(')');
		return result.toString();
	}

} //CONSTANT_Fieldref_infoImpl
