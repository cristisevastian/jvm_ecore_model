/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.CONSTANT_Class_info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.Object_variable_info;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Object variable info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Object_variable_infoImpl#getConstant_class_info <em>Constant class info</em>}</li>
 * </ul>
 *
 * @generated
 */
public class Object_variable_infoImpl extends AbstractVerification_type_infoImpl implements Object_variable_info {
	/**
	 * The cached value of the '{@link #getConstant_class_info() <em>Constant class info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant_class_info()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Class_info constant_class_info;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Object_variable_infoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.OBJECT_VARIABLE_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Class_info getConstant_class_info() {
		if (constant_class_info != null && constant_class_info.eIsProxy()) {
			InternalEObject oldConstant_class_info = (InternalEObject) constant_class_info;
			constant_class_info = (CONSTANT_Class_info) eResolveProxy(oldConstant_class_info);
			if (constant_class_info != oldConstant_class_info) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.OBJECT_VARIABLE_INFO__CONSTANT_CLASS_INFO, oldConstant_class_info,
							constant_class_info));
			}
		}
		return constant_class_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Class_info basicGetConstant_class_info() {
		return constant_class_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstant_class_info(CONSTANT_Class_info newConstant_class_info) {
		CONSTANT_Class_info oldConstant_class_info = constant_class_info;
		constant_class_info = newConstant_class_info;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.OBJECT_VARIABLE_INFO__CONSTANT_CLASS_INFO, oldConstant_class_info,
					constant_class_info));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.OBJECT_VARIABLE_INFO__CONSTANT_CLASS_INFO:
			if (resolve)
				return getConstant_class_info();
			return basicGetConstant_class_info();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.OBJECT_VARIABLE_INFO__CONSTANT_CLASS_INFO:
			setConstant_class_info((CONSTANT_Class_info) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.OBJECT_VARIABLE_INFO__CONSTANT_CLASS_INFO:
			setConstant_class_info((CONSTANT_Class_info) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.OBJECT_VARIABLE_INFO__CONSTANT_CLASS_INFO:
			return constant_class_info != null;
		}
		return super.eIsSet(featureID);
	}

} //Object_variable_infoImpl
