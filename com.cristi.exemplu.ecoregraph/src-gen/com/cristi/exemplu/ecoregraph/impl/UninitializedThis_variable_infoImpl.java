/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.UninitializedThis_variable_info;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Uninitialized This variable info</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UninitializedThis_variable_infoImpl extends AbstractVerification_type_infoImpl
		implements UninitializedThis_variable_info {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UninitializedThis_variable_infoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.UNINITIALIZED_THIS_VARIABLE_INFO;
	}

} //UninitializedThis_variable_infoImpl
