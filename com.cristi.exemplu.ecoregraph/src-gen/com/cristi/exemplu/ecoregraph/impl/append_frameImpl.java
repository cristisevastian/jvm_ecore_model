/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.AbstractVerification_type_info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.append_frame;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>append frame</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.append_frameImpl#getOffset_delta <em>Offset delta</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.append_frameImpl#getLocals <em>Locals</em>}</li>
 * </ul>
 *
 * @generated
 */
public class append_frameImpl extends AbstractStack_map_frameImpl implements append_frame {
	/**
	 * The default value of the '{@link #getOffset_delta() <em>Offset delta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset_delta()
	 * @generated
	 * @ordered
	 */
	protected static final int OFFSET_DELTA_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getOffset_delta() <em>Offset delta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset_delta()
	 * @generated
	 * @ordered
	 */
	protected int offset_delta = OFFSET_DELTA_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLocals() <em>Locals</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocals()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractVerification_type_info> locals;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected append_frameImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.APPEND_FRAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOffset_delta() {
		return offset_delta;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOffset_delta(int newOffset_delta) {
		int oldOffset_delta = offset_delta;
		offset_delta = newOffset_delta;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.APPEND_FRAME__OFFSET_DELTA,
					oldOffset_delta, offset_delta));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractVerification_type_info> getLocals() {
		if (locals == null) {
			locals = new EObjectContainmentEList<AbstractVerification_type_info>(AbstractVerification_type_info.class,
					this, EcoregraphPackage.APPEND_FRAME__LOCALS);
		}
		return locals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case EcoregraphPackage.APPEND_FRAME__LOCALS:
			return ((InternalEList<?>) getLocals()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.APPEND_FRAME__OFFSET_DELTA:
			return getOffset_delta();
		case EcoregraphPackage.APPEND_FRAME__LOCALS:
			return getLocals();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.APPEND_FRAME__OFFSET_DELTA:
			setOffset_delta((Integer) newValue);
			return;
		case EcoregraphPackage.APPEND_FRAME__LOCALS:
			getLocals().clear();
			getLocals().addAll((Collection<? extends AbstractVerification_type_info>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.APPEND_FRAME__OFFSET_DELTA:
			setOffset_delta(OFFSET_DELTA_EDEFAULT);
			return;
		case EcoregraphPackage.APPEND_FRAME__LOCALS:
			getLocals().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.APPEND_FRAME__OFFSET_DELTA:
			return offset_delta != OFFSET_DELTA_EDEFAULT;
		case EcoregraphPackage.APPEND_FRAME__LOCALS:
			return locals != null && !locals.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (offset_delta: ");
		result.append(offset_delta);
		result.append(')');
		return result.toString();
	}

} //append_frameImpl
