/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.ConstantValue_attribute;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Value;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Constant Value attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ConstantValue_attributeImpl#getInterfaceconstant_value <em>Interfaceconstant value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConstantValue_attributeImpl extends AbstractAttribute_infoImpl implements ConstantValue_attribute {
	/**
	 * The cached value of the '{@link #getInterfaceconstant_value() <em>Interfaceconstant value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaceconstant_value()
	 * @generated
	 * @ordered
	 */
	protected InterfaceCONSTANT_Value interfaceconstant_value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstantValue_attributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.CONSTANT_VALUE_ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceCONSTANT_Value getInterfaceconstant_value() {
		if (interfaceconstant_value != null && interfaceconstant_value.eIsProxy()) {
			InternalEObject oldInterfaceconstant_value = (InternalEObject) interfaceconstant_value;
			interfaceconstant_value = (InterfaceCONSTANT_Value) eResolveProxy(oldInterfaceconstant_value);
			if (interfaceconstant_value != oldInterfaceconstant_value) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.CONSTANT_VALUE_ATTRIBUTE__INTERFACECONSTANT_VALUE,
							oldInterfaceconstant_value, interfaceconstant_value));
			}
		}
		return interfaceconstant_value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceCONSTANT_Value basicGetInterfaceconstant_value() {
		return interfaceconstant_value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterfaceconstant_value(InterfaceCONSTANT_Value newInterfaceconstant_value) {
		InterfaceCONSTANT_Value oldInterfaceconstant_value = interfaceconstant_value;
		interfaceconstant_value = newInterfaceconstant_value;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.CONSTANT_VALUE_ATTRIBUTE__INTERFACECONSTANT_VALUE, oldInterfaceconstant_value,
					interfaceconstant_value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_VALUE_ATTRIBUTE__INTERFACECONSTANT_VALUE:
			if (resolve)
				return getInterfaceconstant_value();
			return basicGetInterfaceconstant_value();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_VALUE_ATTRIBUTE__INTERFACECONSTANT_VALUE:
			setInterfaceconstant_value((InterfaceCONSTANT_Value) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_VALUE_ATTRIBUTE__INTERFACECONSTANT_VALUE:
			setInterfaceconstant_value((InterfaceCONSTANT_Value) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_VALUE_ATTRIBUTE__INTERFACECONSTANT_VALUE:
			return interfaceconstant_value != null;
		}
		return super.eIsSet(featureID);
	}

} //ConstantValue_attributeImpl
