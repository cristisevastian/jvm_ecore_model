/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.AbstractAttribute_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Attribute info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.AbstractAttribute_infoImpl#getAttribute_length <em>Attribute length</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.AbstractAttribute_infoImpl#getAttribute_name <em>Attribute name</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractAttribute_infoImpl extends MinimalEObjectImpl.Container
		implements AbstractAttribute_info {
	/**
	 * The default value of the '{@link #getAttribute_length() <em>Attribute length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttribute_length()
	 * @generated
	 * @ordered
	 */
	protected static final long ATTRIBUTE_LENGTH_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getAttribute_length() <em>Attribute length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttribute_length()
	 * @generated
	 * @ordered
	 */
	protected long attribute_length = ATTRIBUTE_LENGTH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAttribute_name() <em>Attribute name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttribute_name()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Utf8_info attribute_name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractAttribute_infoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.ABSTRACT_ATTRIBUTE_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getAttribute_length() {
		return attribute_length;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttribute_length(long newAttribute_length) {
		long oldAttribute_length = attribute_length;
		attribute_length = newAttribute_length;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_LENGTH, oldAttribute_length,
					attribute_length));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info getAttribute_name() {
		if (attribute_name != null && attribute_name.eIsProxy()) {
			InternalEObject oldAttribute_name = (InternalEObject) attribute_name;
			attribute_name = (CONSTANT_Utf8_info) eResolveProxy(oldAttribute_name);
			if (attribute_name != oldAttribute_name) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_NAME, oldAttribute_name,
							attribute_name));
			}
		}
		return attribute_name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info basicGetAttribute_name() {
		return attribute_name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttribute_name(CONSTANT_Utf8_info newAttribute_name) {
		CONSTANT_Utf8_info oldAttribute_name = attribute_name;
		attribute_name = newAttribute_name;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_NAME, oldAttribute_name, attribute_name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_LENGTH:
			return getAttribute_length();
		case EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_NAME:
			if (resolve)
				return getAttribute_name();
			return basicGetAttribute_name();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_LENGTH:
			setAttribute_length((Long) newValue);
			return;
		case EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_NAME:
			setAttribute_name((CONSTANT_Utf8_info) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_LENGTH:
			setAttribute_length(ATTRIBUTE_LENGTH_EDEFAULT);
			return;
		case EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_NAME:
			setAttribute_name((CONSTANT_Utf8_info) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_LENGTH:
			return attribute_length != ATTRIBUTE_LENGTH_EDEFAULT;
		case EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_NAME:
			return attribute_name != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (attribute_length: ");
		result.append(attribute_length);
		result.append(')');
		return result.toString();
	}

} //AbstractAttribute_infoImpl
