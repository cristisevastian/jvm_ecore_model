/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.CONSTANT_Class_info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.exception_table_entry;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>exception table entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.exception_table_entryImpl#getStart_pc <em>Start pc</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.exception_table_entryImpl#getEnd_pc <em>End pc</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.exception_table_entryImpl#getHandler_pc <em>Handler pc</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.exception_table_entryImpl#getCatch_type <em>Catch type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class exception_table_entryImpl extends MinimalEObjectImpl.Container implements exception_table_entry {
	/**
	 * The default value of the '{@link #getStart_pc() <em>Start pc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStart_pc()
	 * @generated
	 * @ordered
	 */
	protected static final int START_PC_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getStart_pc() <em>Start pc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStart_pc()
	 * @generated
	 * @ordered
	 */
	protected int start_pc = START_PC_EDEFAULT;

	/**
	 * The default value of the '{@link #getEnd_pc() <em>End pc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnd_pc()
	 * @generated
	 * @ordered
	 */
	protected static final int END_PC_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEnd_pc() <em>End pc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnd_pc()
	 * @generated
	 * @ordered
	 */
	protected int end_pc = END_PC_EDEFAULT;

	/**
	 * The default value of the '{@link #getHandler_pc() <em>Handler pc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHandler_pc()
	 * @generated
	 * @ordered
	 */
	protected static final int HANDLER_PC_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getHandler_pc() <em>Handler pc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHandler_pc()
	 * @generated
	 * @ordered
	 */
	protected int handler_pc = HANDLER_PC_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCatch_type() <em>Catch type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCatch_type()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Class_info catch_type;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected exception_table_entryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.EXCEPTION_TABLE_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getStart_pc() {
		return start_pc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStart_pc(int newStart_pc) {
		int oldStart_pc = start_pc;
		start_pc = newStart_pc;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.EXCEPTION_TABLE_ENTRY__START_PC,
					oldStart_pc, start_pc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEnd_pc() {
		return end_pc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnd_pc(int newEnd_pc) {
		int oldEnd_pc = end_pc;
		end_pc = newEnd_pc;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.EXCEPTION_TABLE_ENTRY__END_PC,
					oldEnd_pc, end_pc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getHandler_pc() {
		return handler_pc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHandler_pc(int newHandler_pc) {
		int oldHandler_pc = handler_pc;
		handler_pc = newHandler_pc;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.EXCEPTION_TABLE_ENTRY__HANDLER_PC,
					oldHandler_pc, handler_pc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Class_info getCatch_type() {
		if (catch_type != null && catch_type.eIsProxy()) {
			InternalEObject oldCatch_type = (InternalEObject) catch_type;
			catch_type = (CONSTANT_Class_info) eResolveProxy(oldCatch_type);
			if (catch_type != oldCatch_type) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.EXCEPTION_TABLE_ENTRY__CATCH_TYPE, oldCatch_type, catch_type));
			}
		}
		return catch_type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Class_info basicGetCatch_type() {
		return catch_type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCatch_type(CONSTANT_Class_info newCatch_type) {
		CONSTANT_Class_info oldCatch_type = catch_type;
		catch_type = newCatch_type;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.EXCEPTION_TABLE_ENTRY__CATCH_TYPE,
					oldCatch_type, catch_type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__START_PC:
			return getStart_pc();
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__END_PC:
			return getEnd_pc();
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__HANDLER_PC:
			return getHandler_pc();
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__CATCH_TYPE:
			if (resolve)
				return getCatch_type();
			return basicGetCatch_type();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__START_PC:
			setStart_pc((Integer) newValue);
			return;
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__END_PC:
			setEnd_pc((Integer) newValue);
			return;
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__HANDLER_PC:
			setHandler_pc((Integer) newValue);
			return;
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__CATCH_TYPE:
			setCatch_type((CONSTANT_Class_info) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__START_PC:
			setStart_pc(START_PC_EDEFAULT);
			return;
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__END_PC:
			setEnd_pc(END_PC_EDEFAULT);
			return;
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__HANDLER_PC:
			setHandler_pc(HANDLER_PC_EDEFAULT);
			return;
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__CATCH_TYPE:
			setCatch_type((CONSTANT_Class_info) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__START_PC:
			return start_pc != START_PC_EDEFAULT;
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__END_PC:
			return end_pc != END_PC_EDEFAULT;
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__HANDLER_PC:
			return handler_pc != HANDLER_PC_EDEFAULT;
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY__CATCH_TYPE:
			return catch_type != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (start_pc: ");
		result.append(start_pc);
		result.append(", end_pc: ");
		result.append(end_pc);
		result.append(", handler_pc: ");
		result.append(handler_pc);
		result.append(')');
		return result.toString();
	}

} //exception_table_entryImpl
