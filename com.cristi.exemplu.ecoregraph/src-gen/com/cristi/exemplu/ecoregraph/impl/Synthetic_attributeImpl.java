/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.Synthetic_attribute;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Synthetic attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Synthetic_attributeImpl extends AbstractAttribute_infoImpl implements Synthetic_attribute {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Synthetic_attributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.SYNTHETIC_ATTRIBUTE;
	}

} //Synthetic_attributeImpl
