/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.CONSTANT_Long_info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CONSTANT Long info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Long_infoImpl#getHigh_bytes <em>High bytes</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Long_infoImpl#getLow_bytes <em>Low bytes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CONSTANT_Long_infoImpl extends AbstractCp_infoImpl implements CONSTANT_Long_info {
	/**
	 * The default value of the '{@link #getHigh_bytes() <em>High bytes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHigh_bytes()
	 * @generated
	 * @ordered
	 */
	protected static final long HIGH_BYTES_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getHigh_bytes() <em>High bytes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHigh_bytes()
	 * @generated
	 * @ordered
	 */
	protected long high_bytes = HIGH_BYTES_EDEFAULT;

	/**
	 * The default value of the '{@link #getLow_bytes() <em>Low bytes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLow_bytes()
	 * @generated
	 * @ordered
	 */
	protected static final long LOW_BYTES_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getLow_bytes() <em>Low bytes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLow_bytes()
	 * @generated
	 * @ordered
	 */
	protected long low_bytes = LOW_BYTES_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CONSTANT_Long_infoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.CONSTANT_LONG_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getHigh_bytes() {
		return high_bytes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHigh_bytes(long newHigh_bytes) {
		long oldHigh_bytes = high_bytes;
		high_bytes = newHigh_bytes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CONSTANT_LONG_INFO__HIGH_BYTES,
					oldHigh_bytes, high_bytes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getLow_bytes() {
		return low_bytes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLow_bytes(long newLow_bytes) {
		long oldLow_bytes = low_bytes;
		low_bytes = newLow_bytes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CONSTANT_LONG_INFO__LOW_BYTES,
					oldLow_bytes, low_bytes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_LONG_INFO__HIGH_BYTES:
			return getHigh_bytes();
		case EcoregraphPackage.CONSTANT_LONG_INFO__LOW_BYTES:
			return getLow_bytes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_LONG_INFO__HIGH_BYTES:
			setHigh_bytes((Long) newValue);
			return;
		case EcoregraphPackage.CONSTANT_LONG_INFO__LOW_BYTES:
			setLow_bytes((Long) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_LONG_INFO__HIGH_BYTES:
			setHigh_bytes(HIGH_BYTES_EDEFAULT);
			return;
		case EcoregraphPackage.CONSTANT_LONG_INFO__LOW_BYTES:
			setLow_bytes(LOW_BYTES_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_LONG_INFO__HIGH_BYTES:
			return high_bytes != HIGH_BYTES_EDEFAULT;
		case EcoregraphPackage.CONSTANT_LONG_INFO__LOW_BYTES:
			return low_bytes != LOW_BYTES_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (high_bytes: ");
		result.append(high_bytes);
		result.append(", low_bytes: ");
		result.append(low_bytes);
		result.append(')');
		return result.toString();
	}

} //CONSTANT_Long_infoImpl
