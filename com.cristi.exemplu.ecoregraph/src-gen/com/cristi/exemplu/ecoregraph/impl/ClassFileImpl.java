/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.AbstractCp_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Class_info;
import com.cristi.exemplu.ecoregraph.ClassFile;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.Field_info;
import com.cristi.exemplu.ecoregraph.InterfaceClass_attribute_info;
import com.cristi.exemplu.ecoregraph.Method_info;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class File</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getMagic <em>Magic</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getMinor_version <em>Minor version</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getMajor_version <em>Major version</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getConstant_pool_count <em>Constant pool count</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getAccess_flags <em>Access flags</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getInterfaces_count <em>Interfaces count</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getFields_count <em>Fields count</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getMethods_count <em>Methods count</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getAttributes_count <em>Attributes count</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getCp_info <em>Cp info</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getField_info <em>Field info</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getMethod_info <em>Method info</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getSuper_class <em>Super class</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getThis_class <em>This class</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl#getInterfaceclass_attribute_info <em>Interfaceclass attribute info</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClassFileImpl extends MinimalEObjectImpl.Container implements ClassFile {
	/**
	 * The default value of the '{@link #getMagic() <em>Magic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMagic()
	 * @generated
	 * @ordered
	 */
	protected static final long MAGIC_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getMagic() <em>Magic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMagic()
	 * @generated
	 * @ordered
	 */
	protected long magic = MAGIC_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinor_version() <em>Minor version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinor_version()
	 * @generated
	 * @ordered
	 */
	protected static final int MINOR_VERSION_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMinor_version() <em>Minor version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinor_version()
	 * @generated
	 * @ordered
	 */
	protected int minor_version = MINOR_VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getMajor_version() <em>Major version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMajor_version()
	 * @generated
	 * @ordered
	 */
	protected static final int MAJOR_VERSION_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMajor_version() <em>Major version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMajor_version()
	 * @generated
	 * @ordered
	 */
	protected int major_version = MAJOR_VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getConstant_pool_count() <em>Constant pool count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant_pool_count()
	 * @generated
	 * @ordered
	 */
	protected static final int CONSTANT_POOL_COUNT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getConstant_pool_count() <em>Constant pool count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant_pool_count()
	 * @generated
	 * @ordered
	 */
	protected int constant_pool_count = CONSTANT_POOL_COUNT_EDEFAULT;

	/**
	 * The default value of the '{@link #getAccess_flags() <em>Access flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccess_flags()
	 * @generated
	 * @ordered
	 */
	protected static final int ACCESS_FLAGS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getAccess_flags() <em>Access flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccess_flags()
	 * @generated
	 * @ordered
	 */
	protected int access_flags = ACCESS_FLAGS_EDEFAULT;

	/**
	 * The default value of the '{@link #getInterfaces_count() <em>Interfaces count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaces_count()
	 * @generated
	 * @ordered
	 */
	protected static final int INTERFACES_COUNT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getInterfaces_count() <em>Interfaces count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaces_count()
	 * @generated
	 * @ordered
	 */
	protected int interfaces_count = INTERFACES_COUNT_EDEFAULT;

	/**
	 * The default value of the '{@link #getFields_count() <em>Fields count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFields_count()
	 * @generated
	 * @ordered
	 */
	protected static final int FIELDS_COUNT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFields_count() <em>Fields count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFields_count()
	 * @generated
	 * @ordered
	 */
	protected int fields_count = FIELDS_COUNT_EDEFAULT;

	/**
	 * The default value of the '{@link #getMethods_count() <em>Methods count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethods_count()
	 * @generated
	 * @ordered
	 */
	protected static final int METHODS_COUNT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMethods_count() <em>Methods count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethods_count()
	 * @generated
	 * @ordered
	 */
	protected int methods_count = METHODS_COUNT_EDEFAULT;

	/**
	 * The default value of the '{@link #getAttributes_count() <em>Attributes count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributes_count()
	 * @generated
	 * @ordered
	 */
	protected static final int ATTRIBUTES_COUNT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getAttributes_count() <em>Attributes count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributes_count()
	 * @generated
	 * @ordered
	 */
	protected int attributes_count = ATTRIBUTES_COUNT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCp_info() <em>Cp info</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCp_info()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractCp_info> cp_info;

	/**
	 * The cached value of the '{@link #getField_info() <em>Field info</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getField_info()
	 * @generated
	 * @ordered
	 */
	protected EList<Field_info> field_info;

	/**
	 * The cached value of the '{@link #getMethod_info() <em>Method info</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethod_info()
	 * @generated
	 * @ordered
	 */
	protected EList<Method_info> method_info;

	/**
	 * The cached value of the '{@link #getSuper_class() <em>Super class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuper_class()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Class_info super_class;

	/**
	 * The cached value of the '{@link #getInterfaces() <em>Interfaces</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaces()
	 * @generated
	 * @ordered
	 */
	protected EList<CONSTANT_Class_info> interfaces;

	/**
	 * The cached value of the '{@link #getThis_class() <em>This class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThis_class()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Class_info this_class;

	/**
	 * The cached value of the '{@link #getInterfaceclass_attribute_info() <em>Interfaceclass attribute info</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaceclass_attribute_info()
	 * @generated
	 * @ordered
	 */
	protected EList<InterfaceClass_attribute_info> interfaceclass_attribute_info;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassFileImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.CLASS_FILE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMagic() {
		return magic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMagic(long newMagic) {
		long oldMagic = magic;
		magic = newMagic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CLASS_FILE__MAGIC, oldMagic,
					magic));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinor_version() {
		return minor_version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinor_version(int newMinor_version) {
		int oldMinor_version = minor_version;
		minor_version = newMinor_version;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CLASS_FILE__MINOR_VERSION,
					oldMinor_version, minor_version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMajor_version() {
		return major_version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMajor_version(int newMajor_version) {
		int oldMajor_version = major_version;
		major_version = newMajor_version;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CLASS_FILE__MAJOR_VERSION,
					oldMajor_version, major_version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getConstant_pool_count() {
		return constant_pool_count;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstant_pool_count(int newConstant_pool_count) {
		int oldConstant_pool_count = constant_pool_count;
		constant_pool_count = newConstant_pool_count;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CLASS_FILE__CONSTANT_POOL_COUNT,
					oldConstant_pool_count, constant_pool_count));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAccess_flags() {
		return access_flags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccess_flags(int newAccess_flags) {
		int oldAccess_flags = access_flags;
		access_flags = newAccess_flags;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CLASS_FILE__ACCESS_FLAGS,
					oldAccess_flags, access_flags));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getInterfaces_count() {
		return interfaces_count;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterfaces_count(int newInterfaces_count) {
		int oldInterfaces_count = interfaces_count;
		interfaces_count = newInterfaces_count;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CLASS_FILE__INTERFACES_COUNT,
					oldInterfaces_count, interfaces_count));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFields_count() {
		return fields_count;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFields_count(int newFields_count) {
		int oldFields_count = fields_count;
		fields_count = newFields_count;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CLASS_FILE__FIELDS_COUNT,
					oldFields_count, fields_count));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMethods_count() {
		return methods_count;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethods_count(int newMethods_count) {
		int oldMethods_count = methods_count;
		methods_count = newMethods_count;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CLASS_FILE__METHODS_COUNT,
					oldMethods_count, methods_count));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAttributes_count() {
		return attributes_count;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributes_count(int newAttributes_count) {
		int oldAttributes_count = attributes_count;
		attributes_count = newAttributes_count;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CLASS_FILE__ATTRIBUTES_COUNT,
					oldAttributes_count, attributes_count));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractCp_info> getCp_info() {
		if (cp_info == null) {
			cp_info = new EObjectContainmentEList<AbstractCp_info>(AbstractCp_info.class, this,
					EcoregraphPackage.CLASS_FILE__CP_INFO);
		}
		return cp_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Field_info> getField_info() {
		if (field_info == null) {
			field_info = new EObjectContainmentEList<Field_info>(Field_info.class, this,
					EcoregraphPackage.CLASS_FILE__FIELD_INFO);
		}
		return field_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Method_info> getMethod_info() {
		if (method_info == null) {
			method_info = new EObjectContainmentEList<Method_info>(Method_info.class, this,
					EcoregraphPackage.CLASS_FILE__METHOD_INFO);
		}
		return method_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Class_info getSuper_class() {
		if (super_class != null && super_class.eIsProxy()) {
			InternalEObject oldSuper_class = (InternalEObject) super_class;
			super_class = (CONSTANT_Class_info) eResolveProxy(oldSuper_class);
			if (super_class != oldSuper_class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EcoregraphPackage.CLASS_FILE__SUPER_CLASS,
							oldSuper_class, super_class));
			}
		}
		return super_class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Class_info basicGetSuper_class() {
		return super_class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuper_class(CONSTANT_Class_info newSuper_class) {
		CONSTANT_Class_info oldSuper_class = super_class;
		super_class = newSuper_class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CLASS_FILE__SUPER_CLASS,
					oldSuper_class, super_class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CONSTANT_Class_info> getInterfaces() {
		if (interfaces == null) {
			interfaces = new EObjectResolvingEList<CONSTANT_Class_info>(CONSTANT_Class_info.class, this,
					EcoregraphPackage.CLASS_FILE__INTERFACES);
		}
		return interfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Class_info getThis_class() {
		if (this_class != null && this_class.eIsProxy()) {
			InternalEObject oldThis_class = (InternalEObject) this_class;
			this_class = (CONSTANT_Class_info) eResolveProxy(oldThis_class);
			if (this_class != oldThis_class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EcoregraphPackage.CLASS_FILE__THIS_CLASS,
							oldThis_class, this_class));
			}
		}
		return this_class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Class_info basicGetThis_class() {
		return this_class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThis_class(CONSTANT_Class_info newThis_class) {
		CONSTANT_Class_info oldThis_class = this_class;
		this_class = newThis_class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CLASS_FILE__THIS_CLASS,
					oldThis_class, this_class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterfaceClass_attribute_info> getInterfaceclass_attribute_info() {
		if (interfaceclass_attribute_info == null) {
			interfaceclass_attribute_info = new EObjectResolvingEList<InterfaceClass_attribute_info>(
					InterfaceClass_attribute_info.class, this,
					EcoregraphPackage.CLASS_FILE__INTERFACECLASS_ATTRIBUTE_INFO);
		}
		return interfaceclass_attribute_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case EcoregraphPackage.CLASS_FILE__CP_INFO:
			return ((InternalEList<?>) getCp_info()).basicRemove(otherEnd, msgs);
		case EcoregraphPackage.CLASS_FILE__FIELD_INFO:
			return ((InternalEList<?>) getField_info()).basicRemove(otherEnd, msgs);
		case EcoregraphPackage.CLASS_FILE__METHOD_INFO:
			return ((InternalEList<?>) getMethod_info()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.CLASS_FILE__MAGIC:
			return getMagic();
		case EcoregraphPackage.CLASS_FILE__MINOR_VERSION:
			return getMinor_version();
		case EcoregraphPackage.CLASS_FILE__MAJOR_VERSION:
			return getMajor_version();
		case EcoregraphPackage.CLASS_FILE__CONSTANT_POOL_COUNT:
			return getConstant_pool_count();
		case EcoregraphPackage.CLASS_FILE__ACCESS_FLAGS:
			return getAccess_flags();
		case EcoregraphPackage.CLASS_FILE__INTERFACES_COUNT:
			return getInterfaces_count();
		case EcoregraphPackage.CLASS_FILE__FIELDS_COUNT:
			return getFields_count();
		case EcoregraphPackage.CLASS_FILE__METHODS_COUNT:
			return getMethods_count();
		case EcoregraphPackage.CLASS_FILE__ATTRIBUTES_COUNT:
			return getAttributes_count();
		case EcoregraphPackage.CLASS_FILE__CP_INFO:
			return getCp_info();
		case EcoregraphPackage.CLASS_FILE__FIELD_INFO:
			return getField_info();
		case EcoregraphPackage.CLASS_FILE__METHOD_INFO:
			return getMethod_info();
		case EcoregraphPackage.CLASS_FILE__SUPER_CLASS:
			if (resolve)
				return getSuper_class();
			return basicGetSuper_class();
		case EcoregraphPackage.CLASS_FILE__INTERFACES:
			return getInterfaces();
		case EcoregraphPackage.CLASS_FILE__THIS_CLASS:
			if (resolve)
				return getThis_class();
			return basicGetThis_class();
		case EcoregraphPackage.CLASS_FILE__INTERFACECLASS_ATTRIBUTE_INFO:
			return getInterfaceclass_attribute_info();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.CLASS_FILE__MAGIC:
			setMagic((Long) newValue);
			return;
		case EcoregraphPackage.CLASS_FILE__MINOR_VERSION:
			setMinor_version((Integer) newValue);
			return;
		case EcoregraphPackage.CLASS_FILE__MAJOR_VERSION:
			setMajor_version((Integer) newValue);
			return;
		case EcoregraphPackage.CLASS_FILE__CONSTANT_POOL_COUNT:
			setConstant_pool_count((Integer) newValue);
			return;
		case EcoregraphPackage.CLASS_FILE__ACCESS_FLAGS:
			setAccess_flags((Integer) newValue);
			return;
		case EcoregraphPackage.CLASS_FILE__INTERFACES_COUNT:
			setInterfaces_count((Integer) newValue);
			return;
		case EcoregraphPackage.CLASS_FILE__FIELDS_COUNT:
			setFields_count((Integer) newValue);
			return;
		case EcoregraphPackage.CLASS_FILE__METHODS_COUNT:
			setMethods_count((Integer) newValue);
			return;
		case EcoregraphPackage.CLASS_FILE__ATTRIBUTES_COUNT:
			setAttributes_count((Integer) newValue);
			return;
		case EcoregraphPackage.CLASS_FILE__CP_INFO:
			getCp_info().clear();
			getCp_info().addAll((Collection<? extends AbstractCp_info>) newValue);
			return;
		case EcoregraphPackage.CLASS_FILE__FIELD_INFO:
			getField_info().clear();
			getField_info().addAll((Collection<? extends Field_info>) newValue);
			return;
		case EcoregraphPackage.CLASS_FILE__METHOD_INFO:
			getMethod_info().clear();
			getMethod_info().addAll((Collection<? extends Method_info>) newValue);
			return;
		case EcoregraphPackage.CLASS_FILE__SUPER_CLASS:
			setSuper_class((CONSTANT_Class_info) newValue);
			return;
		case EcoregraphPackage.CLASS_FILE__INTERFACES:
			getInterfaces().clear();
			getInterfaces().addAll((Collection<? extends CONSTANT_Class_info>) newValue);
			return;
		case EcoregraphPackage.CLASS_FILE__THIS_CLASS:
			setThis_class((CONSTANT_Class_info) newValue);
			return;
		case EcoregraphPackage.CLASS_FILE__INTERFACECLASS_ATTRIBUTE_INFO:
			getInterfaceclass_attribute_info().clear();
			getInterfaceclass_attribute_info().addAll((Collection<? extends InterfaceClass_attribute_info>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CLASS_FILE__MAGIC:
			setMagic(MAGIC_EDEFAULT);
			return;
		case EcoregraphPackage.CLASS_FILE__MINOR_VERSION:
			setMinor_version(MINOR_VERSION_EDEFAULT);
			return;
		case EcoregraphPackage.CLASS_FILE__MAJOR_VERSION:
			setMajor_version(MAJOR_VERSION_EDEFAULT);
			return;
		case EcoregraphPackage.CLASS_FILE__CONSTANT_POOL_COUNT:
			setConstant_pool_count(CONSTANT_POOL_COUNT_EDEFAULT);
			return;
		case EcoregraphPackage.CLASS_FILE__ACCESS_FLAGS:
			setAccess_flags(ACCESS_FLAGS_EDEFAULT);
			return;
		case EcoregraphPackage.CLASS_FILE__INTERFACES_COUNT:
			setInterfaces_count(INTERFACES_COUNT_EDEFAULT);
			return;
		case EcoregraphPackage.CLASS_FILE__FIELDS_COUNT:
			setFields_count(FIELDS_COUNT_EDEFAULT);
			return;
		case EcoregraphPackage.CLASS_FILE__METHODS_COUNT:
			setMethods_count(METHODS_COUNT_EDEFAULT);
			return;
		case EcoregraphPackage.CLASS_FILE__ATTRIBUTES_COUNT:
			setAttributes_count(ATTRIBUTES_COUNT_EDEFAULT);
			return;
		case EcoregraphPackage.CLASS_FILE__CP_INFO:
			getCp_info().clear();
			return;
		case EcoregraphPackage.CLASS_FILE__FIELD_INFO:
			getField_info().clear();
			return;
		case EcoregraphPackage.CLASS_FILE__METHOD_INFO:
			getMethod_info().clear();
			return;
		case EcoregraphPackage.CLASS_FILE__SUPER_CLASS:
			setSuper_class((CONSTANT_Class_info) null);
			return;
		case EcoregraphPackage.CLASS_FILE__INTERFACES:
			getInterfaces().clear();
			return;
		case EcoregraphPackage.CLASS_FILE__THIS_CLASS:
			setThis_class((CONSTANT_Class_info) null);
			return;
		case EcoregraphPackage.CLASS_FILE__INTERFACECLASS_ATTRIBUTE_INFO:
			getInterfaceclass_attribute_info().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CLASS_FILE__MAGIC:
			return magic != MAGIC_EDEFAULT;
		case EcoregraphPackage.CLASS_FILE__MINOR_VERSION:
			return minor_version != MINOR_VERSION_EDEFAULT;
		case EcoregraphPackage.CLASS_FILE__MAJOR_VERSION:
			return major_version != MAJOR_VERSION_EDEFAULT;
		case EcoregraphPackage.CLASS_FILE__CONSTANT_POOL_COUNT:
			return constant_pool_count != CONSTANT_POOL_COUNT_EDEFAULT;
		case EcoregraphPackage.CLASS_FILE__ACCESS_FLAGS:
			return access_flags != ACCESS_FLAGS_EDEFAULT;
		case EcoregraphPackage.CLASS_FILE__INTERFACES_COUNT:
			return interfaces_count != INTERFACES_COUNT_EDEFAULT;
		case EcoregraphPackage.CLASS_FILE__FIELDS_COUNT:
			return fields_count != FIELDS_COUNT_EDEFAULT;
		case EcoregraphPackage.CLASS_FILE__METHODS_COUNT:
			return methods_count != METHODS_COUNT_EDEFAULT;
		case EcoregraphPackage.CLASS_FILE__ATTRIBUTES_COUNT:
			return attributes_count != ATTRIBUTES_COUNT_EDEFAULT;
		case EcoregraphPackage.CLASS_FILE__CP_INFO:
			return cp_info != null && !cp_info.isEmpty();
		case EcoregraphPackage.CLASS_FILE__FIELD_INFO:
			return field_info != null && !field_info.isEmpty();
		case EcoregraphPackage.CLASS_FILE__METHOD_INFO:
			return method_info != null && !method_info.isEmpty();
		case EcoregraphPackage.CLASS_FILE__SUPER_CLASS:
			return super_class != null;
		case EcoregraphPackage.CLASS_FILE__INTERFACES:
			return interfaces != null && !interfaces.isEmpty();
		case EcoregraphPackage.CLASS_FILE__THIS_CLASS:
			return this_class != null;
		case EcoregraphPackage.CLASS_FILE__INTERFACECLASS_ATTRIBUTE_INFO:
			return interfaceclass_attribute_info != null && !interfaceclass_attribute_info.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (magic: ");
		result.append(magic);
		result.append(", minor_version: ");
		result.append(minor_version);
		result.append(", major_version: ");
		result.append(major_version);
		result.append(", constant_pool_count: ");
		result.append(constant_pool_count);
		result.append(", access_flags: ");
		result.append(access_flags);
		result.append(", interfaces_count: ");
		result.append(interfaces_count);
		result.append(", fields_count: ");
		result.append(fields_count);
		result.append(", methods_count: ");
		result.append(methods_count);
		result.append(", attributes_count: ");
		result.append(attributes_count);
		result.append(')');
		return result.toString();
	}

} //ClassFileImpl
