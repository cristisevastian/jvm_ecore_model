/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.AbstractAttribute_info;
import com.cristi.exemplu.ecoregraph.AbstractCp_info;
import com.cristi.exemplu.ecoregraph.AbstractStack_map_frame;
import com.cristi.exemplu.ecoregraph.AbstractVerification_type_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Class_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Double_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Fieldref_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Float_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Integer_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_InterfaceMethodref_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_InvokeDynamic_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Long_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_MethodHandle_Info;
import com.cristi.exemplu.ecoregraph.CONSTANT_MethodType_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Methodref_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_String_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info;
import com.cristi.exemplu.ecoregraph.ClassFile;
import com.cristi.exemplu.ecoregraph.Class_member;
import com.cristi.exemplu.ecoregraph.Code_attribute;
import com.cristi.exemplu.ecoregraph.ConstantValue_attribute;
import com.cristi.exemplu.ecoregraph.Double_variable_info;
import com.cristi.exemplu.ecoregraph.EcoregraphFactory;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.Exceptions_attribute;
import com.cristi.exemplu.ecoregraph.Field_info;
import com.cristi.exemplu.ecoregraph.Float_variable_info;
import com.cristi.exemplu.ecoregraph.InnerClasses_attribute;
import com.cristi.exemplu.ecoregraph.Integer_variable_info;
import com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Ref_info;
import com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Value;
import com.cristi.exemplu.ecoregraph.InterfaceClass_attribute_info;
import com.cristi.exemplu.ecoregraph.InterfaceCode_attribute;
import com.cristi.exemplu.ecoregraph.InterfaceField_attribute_info;
import com.cristi.exemplu.ecoregraph.InterfaceMethod_attribute_info;
import com.cristi.exemplu.ecoregraph.Long_variable_info;
import com.cristi.exemplu.ecoregraph.Method_info;
import com.cristi.exemplu.ecoregraph.NewEClass36;
import com.cristi.exemplu.ecoregraph.NewEClass54;
import com.cristi.exemplu.ecoregraph.Null_variable_info;
import com.cristi.exemplu.ecoregraph.Object_variable_info;
import com.cristi.exemplu.ecoregraph.Signature_attribute;
import com.cristi.exemplu.ecoregraph.StackMapTable_attribute;
import com.cristi.exemplu.ecoregraph.Synthetic_attribute;
import com.cristi.exemplu.ecoregraph.Top_variable_info;
import com.cristi.exemplu.ecoregraph.UninitializedThis_variable_info;
import com.cristi.exemplu.ecoregraph.Uninitialized_variable_info;
import com.cristi.exemplu.ecoregraph.append_frame;
import com.cristi.exemplu.ecoregraph.chop_frame;
import com.cristi.exemplu.ecoregraph.exception_table_entry;
import com.cristi.exemplu.ecoregraph.full_frame;
import com.cristi.exemplu.ecoregraph.same_frame;
import com.cristi.exemplu.ecoregraph.same_frame_extended;
import com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame;
import com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame_extended;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EcoregraphPackageImpl extends EPackageImpl implements EcoregraphPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constanT_MethodHandle_InfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constanT_Fieldref_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constanT_Methodref_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constanT_InterfaceMethodref_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constanT_Class_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constanT_NameAndType_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constanT_Utf8_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constanT_MethodType_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constanT_String_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constanT_InvokeDynamic_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass field_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass method_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractCp_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interfaceCONSTANT_Ref_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constanT_Integer_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constanT_Float_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constanT_Long_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constanT_Double_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass classFileEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractAttribute_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interfaceMethod_attribute_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interfaceField_attribute_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interfaceClass_attribute_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constantValue_attributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interfaceCONSTANT_ValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass code_attributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass exception_table_entryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stackMapTable_attributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractVerification_type_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass top_variable_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass integer_variable_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass float_variable_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass null_variable_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uninitializedThis_variable_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass object_variable_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass newEClass36EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uninitialized_variable_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass long_variable_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass double_variable_infoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractStack_map_frameEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass same_frameEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass same_locals_1_stack_item_frameEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass same_locals_1_stack_item_frame_extendedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass same_frame_extendedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass chop_frameEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass append_frameEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass full_frameEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass exceptions_attributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interfaceCode_attributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass innerClasses_attributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass class_memberEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass synthetic_attributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signature_attributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass newEClass54EClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private EcoregraphPackageImpl() {
		super(eNS_URI, EcoregraphFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link EcoregraphPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static EcoregraphPackage init() {
		if (isInited)
			return (EcoregraphPackage) EPackage.Registry.INSTANCE.getEPackage(EcoregraphPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredEcoregraphPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		EcoregraphPackageImpl theEcoregraphPackage = registeredEcoregraphPackage instanceof EcoregraphPackageImpl
				? (EcoregraphPackageImpl) registeredEcoregraphPackage
				: new EcoregraphPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theEcoregraphPackage.createPackageContents();

		// Initialize created meta-data
		theEcoregraphPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theEcoregraphPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(EcoregraphPackage.eNS_URI, theEcoregraphPackage);
		return theEcoregraphPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCONSTANT_MethodHandle_Info() {
		return constanT_MethodHandle_InfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCONSTANT_MethodHandle_Info_Reference_kind() {
		return (EAttribute) constanT_MethodHandle_InfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCONSTANT_MethodHandle_Info_Constant_ref_info() {
		return (EReference) constanT_MethodHandle_InfoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCONSTANT_Fieldref_info() {
		return constanT_Fieldref_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCONSTANT_Fieldref_info_Constant_class_info() {
		return (EReference) constanT_Fieldref_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCONSTANT_Fieldref_info_Constant_nameandtype_info() {
		return (EReference) constanT_Fieldref_infoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCONSTANT_Methodref_info() {
		return constanT_Methodref_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCONSTANT_Methodref_info_Constant_class_info() {
		return (EReference) constanT_Methodref_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCONSTANT_Methodref_info_Constant_nameandtype_info() {
		return (EReference) constanT_Methodref_infoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCONSTANT_InterfaceMethodref_info() {
		return constanT_InterfaceMethodref_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCONSTANT_InterfaceMethodref_info_Constant_class_info() {
		return (EReference) constanT_InterfaceMethodref_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCONSTANT_InterfaceMethodref_info_Constant_nameandtype_info() {
		return (EReference) constanT_InterfaceMethodref_infoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCONSTANT_Class_info() {
		return constanT_Class_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCONSTANT_Class_info_Name() {
		return (EReference) constanT_Class_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCONSTANT_NameAndType_info() {
		return constanT_NameAndType_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCONSTANT_NameAndType_info_Name() {
		return (EReference) constanT_NameAndType_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCONSTANT_NameAndType_info_Field_method_descriptor() {
		return (EReference) constanT_NameAndType_infoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCONSTANT_Utf8_info() {
		return constanT_Utf8_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCONSTANT_Utf8_info_Length() {
		return (EAttribute) constanT_Utf8_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCONSTANT_Utf8_info_Bytes() {
		return (EAttribute) constanT_Utf8_infoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCONSTANT_MethodType_info() {
		return constanT_MethodType_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCONSTANT_MethodType_info_Method_descriptor() {
		return (EReference) constanT_MethodType_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCONSTANT_String_info() {
		return constanT_String_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCONSTANT_String_info_Constant_utf8_info() {
		return (EReference) constanT_String_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCONSTANT_InvokeDynamic_info() {
		return constanT_InvokeDynamic_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCONSTANT_InvokeDynamic_info_Constant_nameandtype_info() {
		return (EReference) constanT_InvokeDynamic_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getField_info() {
		return field_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getField_info_Name() {
		return (EReference) field_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getField_info_Field_descriptor() {
		return (EReference) field_infoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_info_Access_flags() {
		return (EAttribute) field_infoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_info_Attributes_count() {
		return (EAttribute) field_infoEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getField_info_Interfacefield_attribute_info() {
		return (EReference) field_infoEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMethod_info() {
		return method_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_info_Access_flags() {
		return (EAttribute) method_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_info_Attributes_count() {
		return (EAttribute) method_infoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_info_Method_descriptor() {
		return (EReference) method_infoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_info_Name() {
		return (EReference) method_infoEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_info_Interfacemethod_attribute_info() {
		return (EReference) method_infoEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractCp_info() {
		return abstractCp_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractCp_info_Tag() {
		return (EAttribute) abstractCp_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterfaceCONSTANT_Ref_info() {
		return interfaceCONSTANT_Ref_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCONSTANT_Integer_info() {
		return constanT_Integer_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCONSTANT_Integer_info_Bytes() {
		return (EAttribute) constanT_Integer_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCONSTANT_Float_info() {
		return constanT_Float_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCONSTANT_Float_info_Bytes() {
		return (EAttribute) constanT_Float_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCONSTANT_Long_info() {
		return constanT_Long_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCONSTANT_Long_info_High_bytes() {
		return (EAttribute) constanT_Long_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCONSTANT_Long_info_Low_bytes() {
		return (EAttribute) constanT_Long_infoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCONSTANT_Double_info() {
		return constanT_Double_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCONSTANT_Double_info_High_bytes() {
		return (EAttribute) constanT_Double_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCONSTANT_Double_info_Low_bytes() {
		return (EAttribute) constanT_Double_infoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClassFile() {
		return classFileEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClassFile_Magic() {
		return (EAttribute) classFileEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClassFile_Minor_version() {
		return (EAttribute) classFileEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClassFile_Major_version() {
		return (EAttribute) classFileEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClassFile_Constant_pool_count() {
		return (EAttribute) classFileEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClassFile_Access_flags() {
		return (EAttribute) classFileEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClassFile_Interfaces_count() {
		return (EAttribute) classFileEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClassFile_Fields_count() {
		return (EAttribute) classFileEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClassFile_Methods_count() {
		return (EAttribute) classFileEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClassFile_Attributes_count() {
		return (EAttribute) classFileEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassFile_Cp_info() {
		return (EReference) classFileEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassFile_Field_info() {
		return (EReference) classFileEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassFile_Method_info() {
		return (EReference) classFileEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassFile_Super_class() {
		return (EReference) classFileEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassFile_Interfaces() {
		return (EReference) classFileEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassFile_This_class() {
		return (EReference) classFileEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassFile_Interfaceclass_attribute_info() {
		return (EReference) classFileEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractAttribute_info() {
		return abstractAttribute_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractAttribute_info_Attribute_length() {
		return (EAttribute) abstractAttribute_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractAttribute_info_Attribute_name() {
		return (EReference) abstractAttribute_infoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterfaceMethod_attribute_info() {
		return interfaceMethod_attribute_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterfaceField_attribute_info() {
		return interfaceField_attribute_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterfaceClass_attribute_info() {
		return interfaceClass_attribute_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConstantValue_attribute() {
		return constantValue_attributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConstantValue_attribute_Interfaceconstant_value() {
		return (EReference) constantValue_attributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterfaceCONSTANT_Value() {
		return interfaceCONSTANT_ValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCode_attribute() {
		return code_attributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCode_attribute_Max_stack() {
		return (EAttribute) code_attributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCode_attribute_Max_locals() {
		return (EAttribute) code_attributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCode_attribute_Code_length() {
		return (EAttribute) code_attributeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCode_attribute_Code() {
		return (EAttribute) code_attributeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCode_attribute_Exception_table() {
		return (EReference) code_attributeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCode_attribute_Exception_table_length() {
		return (EAttribute) code_attributeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCode_attribute_Attributes_count() {
		return (EAttribute) code_attributeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCode_attribute_Interfacecode_attribute() {
		return (EReference) code_attributeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getexception_table_entry() {
		return exception_table_entryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getexception_table_entry_Start_pc() {
		return (EAttribute) exception_table_entryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getexception_table_entry_End_pc() {
		return (EAttribute) exception_table_entryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getexception_table_entry_Handler_pc() {
		return (EAttribute) exception_table_entryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getexception_table_entry_Catch_type() {
		return (EReference) exception_table_entryEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStackMapTable_attribute() {
		return stackMapTable_attributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStackMapTable_attribute_Number_of_entries() {
		return (EAttribute) stackMapTable_attributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStackMapTable_attribute_Abstractstack_map_frame() {
		return (EReference) stackMapTable_attributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractVerification_type_info() {
		return abstractVerification_type_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractVerification_type_info_Tag() {
		return (EAttribute) abstractVerification_type_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTop_variable_info() {
		return top_variable_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInteger_variable_info() {
		return integer_variable_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFloat_variable_info() {
		return float_variable_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNull_variable_info() {
		return null_variable_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUninitializedThis_variable_info() {
		return uninitializedThis_variable_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObject_variable_info() {
		return object_variable_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObject_variable_info_Constant_class_info() {
		return (EReference) object_variable_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNewEClass36() {
		return newEClass36EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUninitialized_variable_info() {
		return uninitialized_variable_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUninitialized_variable_info_Offset() {
		return (EAttribute) uninitialized_variable_infoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLong_variable_info() {
		return long_variable_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDouble_variable_info() {
		return double_variable_infoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractStack_map_frame() {
		return abstractStack_map_frameEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractStack_map_frame_Frame_type() {
		return (EAttribute) abstractStack_map_frameEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getsame_frame() {
		return same_frameEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getsame_locals_1_stack_item_frame() {
		return same_locals_1_stack_item_frameEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsame_locals_1_stack_item_frame_Stack() {
		return (EReference) same_locals_1_stack_item_frameEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getsame_locals_1_stack_item_frame_extended() {
		return same_locals_1_stack_item_frame_extendedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getsame_locals_1_stack_item_frame_extended_Offset_delta() {
		return (EAttribute) same_locals_1_stack_item_frame_extendedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsame_locals_1_stack_item_frame_extended_Stack() {
		return (EReference) same_locals_1_stack_item_frame_extendedEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getsame_frame_extended() {
		return same_frame_extendedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getsame_frame_extended_Offset_delta() {
		return (EAttribute) same_frame_extendedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getchop_frame() {
		return chop_frameEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getchop_frame_Offset_delta() {
		return (EAttribute) chop_frameEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getappend_frame() {
		return append_frameEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getappend_frame_Offset_delta() {
		return (EAttribute) append_frameEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getappend_frame_Locals() {
		return (EReference) append_frameEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getfull_frame() {
		return full_frameEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getfull_frame_Offset_delta() {
		return (EAttribute) full_frameEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getfull_frame_Number_of_locals() {
		return (EAttribute) full_frameEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getfull_frame_Number_of_stack_items() {
		return (EAttribute) full_frameEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfull_frame_Locals() {
		return (EReference) full_frameEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfull_frame_Stack() {
		return (EReference) full_frameEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExceptions_attribute() {
		return exceptions_attributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExceptions_attribute_Exception_table() {
		return (EReference) exceptions_attributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExceptions_attribute_Number_of_exceptions() {
		return (EAttribute) exceptions_attributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterfaceCode_attribute() {
		return interfaceCode_attributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInnerClasses_attribute() {
		return innerClasses_attributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInnerClasses_attribute_Number_of_classes() {
		return (EAttribute) innerClasses_attributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInnerClasses_attribute_Classes() {
		return (EReference) innerClasses_attributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClass_member() {
		return class_memberEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClass_member_Inner_class_access_flags() {
		return (EAttribute) class_memberEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClass_member_Inner_class_info() {
		return (EReference) class_memberEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClass_member_Outer_class_info() {
		return (EReference) class_memberEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClass_member_Inner_name() {
		return (EReference) class_memberEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSynthetic_attribute() {
		return synthetic_attributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignature_attribute() {
		return signature_attributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignature_attribute_Signature() {
		return (EReference) signature_attributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNewEClass54() {
		return newEClass54EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EcoregraphFactory getEcoregraphFactory() {
		return (EcoregraphFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		constanT_MethodHandle_InfoEClass = createEClass(CONSTANT_METHOD_HANDLE_INFO);
		createEAttribute(constanT_MethodHandle_InfoEClass, CONSTANT_METHOD_HANDLE_INFO__REFERENCE_KIND);
		createEReference(constanT_MethodHandle_InfoEClass, CONSTANT_METHOD_HANDLE_INFO__CONSTANT_REF_INFO);

		constanT_Fieldref_infoEClass = createEClass(CONSTANT_FIELDREF_INFO);
		createEReference(constanT_Fieldref_infoEClass, CONSTANT_FIELDREF_INFO__CONSTANT_CLASS_INFO);
		createEReference(constanT_Fieldref_infoEClass, CONSTANT_FIELDREF_INFO__CONSTANT_NAMEANDTYPE_INFO);

		constanT_Methodref_infoEClass = createEClass(CONSTANT_METHODREF_INFO);
		createEReference(constanT_Methodref_infoEClass, CONSTANT_METHODREF_INFO__CONSTANT_CLASS_INFO);
		createEReference(constanT_Methodref_infoEClass, CONSTANT_METHODREF_INFO__CONSTANT_NAMEANDTYPE_INFO);

		constanT_InterfaceMethodref_infoEClass = createEClass(CONSTANT_INTERFACE_METHODREF_INFO);
		createEReference(constanT_InterfaceMethodref_infoEClass,
				CONSTANT_INTERFACE_METHODREF_INFO__CONSTANT_CLASS_INFO);
		createEReference(constanT_InterfaceMethodref_infoEClass,
				CONSTANT_INTERFACE_METHODREF_INFO__CONSTANT_NAMEANDTYPE_INFO);

		constanT_Class_infoEClass = createEClass(CONSTANT_CLASS_INFO);
		createEReference(constanT_Class_infoEClass, CONSTANT_CLASS_INFO__NAME);

		constanT_NameAndType_infoEClass = createEClass(CONSTANT_NAME_AND_TYPE_INFO);
		createEReference(constanT_NameAndType_infoEClass, CONSTANT_NAME_AND_TYPE_INFO__NAME);
		createEReference(constanT_NameAndType_infoEClass, CONSTANT_NAME_AND_TYPE_INFO__FIELD_METHOD_DESCRIPTOR);

		constanT_Utf8_infoEClass = createEClass(CONSTANT_UTF8_INFO);
		createEAttribute(constanT_Utf8_infoEClass, CONSTANT_UTF8_INFO__LENGTH);
		createEAttribute(constanT_Utf8_infoEClass, CONSTANT_UTF8_INFO__BYTES);

		constanT_MethodType_infoEClass = createEClass(CONSTANT_METHOD_TYPE_INFO);
		createEReference(constanT_MethodType_infoEClass, CONSTANT_METHOD_TYPE_INFO__METHOD_DESCRIPTOR);

		constanT_String_infoEClass = createEClass(CONSTANT_STRING_INFO);
		createEReference(constanT_String_infoEClass, CONSTANT_STRING_INFO__CONSTANT_UTF8_INFO);

		constanT_InvokeDynamic_infoEClass = createEClass(CONSTANT_INVOKE_DYNAMIC_INFO);
		createEReference(constanT_InvokeDynamic_infoEClass, CONSTANT_INVOKE_DYNAMIC_INFO__CONSTANT_NAMEANDTYPE_INFO);

		field_infoEClass = createEClass(FIELD_INFO);
		createEReference(field_infoEClass, FIELD_INFO__NAME);
		createEReference(field_infoEClass, FIELD_INFO__FIELD_DESCRIPTOR);
		createEAttribute(field_infoEClass, FIELD_INFO__ACCESS_FLAGS);
		createEAttribute(field_infoEClass, FIELD_INFO__ATTRIBUTES_COUNT);
		createEReference(field_infoEClass, FIELD_INFO__INTERFACEFIELD_ATTRIBUTE_INFO);

		method_infoEClass = createEClass(METHOD_INFO);
		createEAttribute(method_infoEClass, METHOD_INFO__ACCESS_FLAGS);
		createEAttribute(method_infoEClass, METHOD_INFO__ATTRIBUTES_COUNT);
		createEReference(method_infoEClass, METHOD_INFO__METHOD_DESCRIPTOR);
		createEReference(method_infoEClass, METHOD_INFO__NAME);
		createEReference(method_infoEClass, METHOD_INFO__INTERFACEMETHOD_ATTRIBUTE_INFO);

		abstractCp_infoEClass = createEClass(ABSTRACT_CP_INFO);
		createEAttribute(abstractCp_infoEClass, ABSTRACT_CP_INFO__TAG);

		interfaceCONSTANT_Ref_infoEClass = createEClass(INTERFACE_CONSTANT_REF_INFO);

		constanT_Integer_infoEClass = createEClass(CONSTANT_INTEGER_INFO);
		createEAttribute(constanT_Integer_infoEClass, CONSTANT_INTEGER_INFO__BYTES);

		constanT_Float_infoEClass = createEClass(CONSTANT_FLOAT_INFO);
		createEAttribute(constanT_Float_infoEClass, CONSTANT_FLOAT_INFO__BYTES);

		constanT_Long_infoEClass = createEClass(CONSTANT_LONG_INFO);
		createEAttribute(constanT_Long_infoEClass, CONSTANT_LONG_INFO__HIGH_BYTES);
		createEAttribute(constanT_Long_infoEClass, CONSTANT_LONG_INFO__LOW_BYTES);

		constanT_Double_infoEClass = createEClass(CONSTANT_DOUBLE_INFO);
		createEAttribute(constanT_Double_infoEClass, CONSTANT_DOUBLE_INFO__HIGH_BYTES);
		createEAttribute(constanT_Double_infoEClass, CONSTANT_DOUBLE_INFO__LOW_BYTES);

		classFileEClass = createEClass(CLASS_FILE);
		createEAttribute(classFileEClass, CLASS_FILE__MAGIC);
		createEAttribute(classFileEClass, CLASS_FILE__MINOR_VERSION);
		createEAttribute(classFileEClass, CLASS_FILE__MAJOR_VERSION);
		createEAttribute(classFileEClass, CLASS_FILE__CONSTANT_POOL_COUNT);
		createEAttribute(classFileEClass, CLASS_FILE__ACCESS_FLAGS);
		createEAttribute(classFileEClass, CLASS_FILE__INTERFACES_COUNT);
		createEAttribute(classFileEClass, CLASS_FILE__FIELDS_COUNT);
		createEAttribute(classFileEClass, CLASS_FILE__METHODS_COUNT);
		createEAttribute(classFileEClass, CLASS_FILE__ATTRIBUTES_COUNT);
		createEReference(classFileEClass, CLASS_FILE__CP_INFO);
		createEReference(classFileEClass, CLASS_FILE__FIELD_INFO);
		createEReference(classFileEClass, CLASS_FILE__METHOD_INFO);
		createEReference(classFileEClass, CLASS_FILE__SUPER_CLASS);
		createEReference(classFileEClass, CLASS_FILE__INTERFACES);
		createEReference(classFileEClass, CLASS_FILE__THIS_CLASS);
		createEReference(classFileEClass, CLASS_FILE__INTERFACECLASS_ATTRIBUTE_INFO);

		abstractAttribute_infoEClass = createEClass(ABSTRACT_ATTRIBUTE_INFO);
		createEAttribute(abstractAttribute_infoEClass, ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_LENGTH);
		createEReference(abstractAttribute_infoEClass, ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_NAME);

		interfaceMethod_attribute_infoEClass = createEClass(INTERFACE_METHOD_ATTRIBUTE_INFO);

		interfaceField_attribute_infoEClass = createEClass(INTERFACE_FIELD_ATTRIBUTE_INFO);

		interfaceClass_attribute_infoEClass = createEClass(INTERFACE_CLASS_ATTRIBUTE_INFO);

		constantValue_attributeEClass = createEClass(CONSTANT_VALUE_ATTRIBUTE);
		createEReference(constantValue_attributeEClass, CONSTANT_VALUE_ATTRIBUTE__INTERFACECONSTANT_VALUE);

		interfaceCONSTANT_ValueEClass = createEClass(INTERFACE_CONSTANT_VALUE);

		code_attributeEClass = createEClass(CODE_ATTRIBUTE);
		createEAttribute(code_attributeEClass, CODE_ATTRIBUTE__MAX_STACK);
		createEAttribute(code_attributeEClass, CODE_ATTRIBUTE__MAX_LOCALS);
		createEAttribute(code_attributeEClass, CODE_ATTRIBUTE__CODE_LENGTH);
		createEAttribute(code_attributeEClass, CODE_ATTRIBUTE__CODE);
		createEReference(code_attributeEClass, CODE_ATTRIBUTE__EXCEPTION_TABLE);
		createEAttribute(code_attributeEClass, CODE_ATTRIBUTE__EXCEPTION_TABLE_LENGTH);
		createEAttribute(code_attributeEClass, CODE_ATTRIBUTE__ATTRIBUTES_COUNT);
		createEReference(code_attributeEClass, CODE_ATTRIBUTE__INTERFACECODE_ATTRIBUTE);

		exception_table_entryEClass = createEClass(EXCEPTION_TABLE_ENTRY);
		createEAttribute(exception_table_entryEClass, EXCEPTION_TABLE_ENTRY__START_PC);
		createEAttribute(exception_table_entryEClass, EXCEPTION_TABLE_ENTRY__END_PC);
		createEAttribute(exception_table_entryEClass, EXCEPTION_TABLE_ENTRY__HANDLER_PC);
		createEReference(exception_table_entryEClass, EXCEPTION_TABLE_ENTRY__CATCH_TYPE);

		stackMapTable_attributeEClass = createEClass(STACK_MAP_TABLE_ATTRIBUTE);
		createEAttribute(stackMapTable_attributeEClass, STACK_MAP_TABLE_ATTRIBUTE__NUMBER_OF_ENTRIES);
		createEReference(stackMapTable_attributeEClass, STACK_MAP_TABLE_ATTRIBUTE__ABSTRACTSTACK_MAP_FRAME);

		abstractVerification_type_infoEClass = createEClass(ABSTRACT_VERIFICATION_TYPE_INFO);
		createEAttribute(abstractVerification_type_infoEClass, ABSTRACT_VERIFICATION_TYPE_INFO__TAG);

		top_variable_infoEClass = createEClass(TOP_VARIABLE_INFO);

		integer_variable_infoEClass = createEClass(INTEGER_VARIABLE_INFO);

		float_variable_infoEClass = createEClass(FLOAT_VARIABLE_INFO);

		null_variable_infoEClass = createEClass(NULL_VARIABLE_INFO);

		uninitializedThis_variable_infoEClass = createEClass(UNINITIALIZED_THIS_VARIABLE_INFO);

		object_variable_infoEClass = createEClass(OBJECT_VARIABLE_INFO);
		createEReference(object_variable_infoEClass, OBJECT_VARIABLE_INFO__CONSTANT_CLASS_INFO);

		newEClass36EClass = createEClass(NEW_ECLASS36);

		uninitialized_variable_infoEClass = createEClass(UNINITIALIZED_VARIABLE_INFO);
		createEAttribute(uninitialized_variable_infoEClass, UNINITIALIZED_VARIABLE_INFO__OFFSET);

		long_variable_infoEClass = createEClass(LONG_VARIABLE_INFO);

		double_variable_infoEClass = createEClass(DOUBLE_VARIABLE_INFO);

		abstractStack_map_frameEClass = createEClass(ABSTRACT_STACK_MAP_FRAME);
		createEAttribute(abstractStack_map_frameEClass, ABSTRACT_STACK_MAP_FRAME__FRAME_TYPE);

		same_frameEClass = createEClass(SAME_FRAME);

		same_locals_1_stack_item_frameEClass = createEClass(SAME_LOCALS_1STACK_ITEM_FRAME);
		createEReference(same_locals_1_stack_item_frameEClass, SAME_LOCALS_1STACK_ITEM_FRAME__STACK);

		same_locals_1_stack_item_frame_extendedEClass = createEClass(SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED);
		createEAttribute(same_locals_1_stack_item_frame_extendedEClass,
				SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__OFFSET_DELTA);
		createEReference(same_locals_1_stack_item_frame_extendedEClass, SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__STACK);

		same_frame_extendedEClass = createEClass(SAME_FRAME_EXTENDED);
		createEAttribute(same_frame_extendedEClass, SAME_FRAME_EXTENDED__OFFSET_DELTA);

		chop_frameEClass = createEClass(CHOP_FRAME);
		createEAttribute(chop_frameEClass, CHOP_FRAME__OFFSET_DELTA);

		append_frameEClass = createEClass(APPEND_FRAME);
		createEAttribute(append_frameEClass, APPEND_FRAME__OFFSET_DELTA);
		createEReference(append_frameEClass, APPEND_FRAME__LOCALS);

		full_frameEClass = createEClass(FULL_FRAME);
		createEAttribute(full_frameEClass, FULL_FRAME__OFFSET_DELTA);
		createEAttribute(full_frameEClass, FULL_FRAME__NUMBER_OF_LOCALS);
		createEAttribute(full_frameEClass, FULL_FRAME__NUMBER_OF_STACK_ITEMS);
		createEReference(full_frameEClass, FULL_FRAME__LOCALS);
		createEReference(full_frameEClass, FULL_FRAME__STACK);

		exceptions_attributeEClass = createEClass(EXCEPTIONS_ATTRIBUTE);
		createEReference(exceptions_attributeEClass, EXCEPTIONS_ATTRIBUTE__EXCEPTION_TABLE);
		createEAttribute(exceptions_attributeEClass, EXCEPTIONS_ATTRIBUTE__NUMBER_OF_EXCEPTIONS);

		interfaceCode_attributeEClass = createEClass(INTERFACE_CODE_ATTRIBUTE);

		innerClasses_attributeEClass = createEClass(INNER_CLASSES_ATTRIBUTE);
		createEAttribute(innerClasses_attributeEClass, INNER_CLASSES_ATTRIBUTE__NUMBER_OF_CLASSES);
		createEReference(innerClasses_attributeEClass, INNER_CLASSES_ATTRIBUTE__CLASSES);

		class_memberEClass = createEClass(CLASS_MEMBER);
		createEAttribute(class_memberEClass, CLASS_MEMBER__INNER_CLASS_ACCESS_FLAGS);
		createEReference(class_memberEClass, CLASS_MEMBER__INNER_CLASS_INFO);
		createEReference(class_memberEClass, CLASS_MEMBER__OUTER_CLASS_INFO);
		createEReference(class_memberEClass, CLASS_MEMBER__INNER_NAME);

		synthetic_attributeEClass = createEClass(SYNTHETIC_ATTRIBUTE);

		signature_attributeEClass = createEClass(SIGNATURE_ATTRIBUTE);
		createEReference(signature_attributeEClass, SIGNATURE_ATTRIBUTE__SIGNATURE);

		newEClass54EClass = createEClass(NEW_ECLASS54);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage) EPackage.Registry.INSTANCE
				.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		constanT_MethodHandle_InfoEClass.getESuperTypes().add(this.getAbstractCp_info());
		constanT_Fieldref_infoEClass.getESuperTypes().add(this.getInterfaceCONSTANT_Ref_info());
		constanT_Fieldref_infoEClass.getESuperTypes().add(this.getAbstractCp_info());
		constanT_Methodref_infoEClass.getESuperTypes().add(this.getInterfaceCONSTANT_Ref_info());
		constanT_Methodref_infoEClass.getESuperTypes().add(this.getAbstractCp_info());
		constanT_InterfaceMethodref_infoEClass.getESuperTypes().add(this.getInterfaceCONSTANT_Ref_info());
		constanT_InterfaceMethodref_infoEClass.getESuperTypes().add(this.getAbstractCp_info());
		constanT_Class_infoEClass.getESuperTypes().add(this.getAbstractCp_info());
		constanT_NameAndType_infoEClass.getESuperTypes().add(this.getAbstractCp_info());
		constanT_Utf8_infoEClass.getESuperTypes().add(this.getAbstractCp_info());
		constanT_MethodType_infoEClass.getESuperTypes().add(this.getAbstractCp_info());
		constanT_String_infoEClass.getESuperTypes().add(this.getAbstractCp_info());
		constanT_String_infoEClass.getESuperTypes().add(this.getInterfaceCONSTANT_Value());
		constanT_InvokeDynamic_infoEClass.getESuperTypes().add(this.getAbstractCp_info());
		constanT_Integer_infoEClass.getESuperTypes().add(this.getAbstractCp_info());
		constanT_Integer_infoEClass.getESuperTypes().add(this.getInterfaceCONSTANT_Value());
		constanT_Float_infoEClass.getESuperTypes().add(this.getAbstractCp_info());
		constanT_Float_infoEClass.getESuperTypes().add(this.getInterfaceCONSTANT_Value());
		constanT_Long_infoEClass.getESuperTypes().add(this.getAbstractCp_info());
		constanT_Long_infoEClass.getESuperTypes().add(this.getInterfaceCONSTANT_Value());
		constanT_Double_infoEClass.getESuperTypes().add(this.getAbstractCp_info());
		constanT_Double_infoEClass.getESuperTypes().add(this.getInterfaceCONSTANT_Value());
		constantValue_attributeEClass.getESuperTypes().add(this.getAbstractAttribute_info());
		constantValue_attributeEClass.getESuperTypes().add(this.getInterfaceField_attribute_info());
		code_attributeEClass.getESuperTypes().add(this.getAbstractAttribute_info());
		code_attributeEClass.getESuperTypes().add(this.getInterfaceMethod_attribute_info());
		stackMapTable_attributeEClass.getESuperTypes().add(this.getAbstractAttribute_info());
		stackMapTable_attributeEClass.getESuperTypes().add(this.getInterfaceCode_attribute());
		top_variable_infoEClass.getESuperTypes().add(this.getAbstractVerification_type_info());
		integer_variable_infoEClass.getESuperTypes().add(this.getAbstractVerification_type_info());
		float_variable_infoEClass.getESuperTypes().add(this.getAbstractVerification_type_info());
		null_variable_infoEClass.getESuperTypes().add(this.getAbstractVerification_type_info());
		uninitializedThis_variable_infoEClass.getESuperTypes().add(this.getAbstractVerification_type_info());
		object_variable_infoEClass.getESuperTypes().add(this.getAbstractVerification_type_info());
		uninitialized_variable_infoEClass.getESuperTypes().add(this.getAbstractVerification_type_info());
		long_variable_infoEClass.getESuperTypes().add(this.getAbstractVerification_type_info());
		double_variable_infoEClass.getESuperTypes().add(this.getAbstractVerification_type_info());
		same_frameEClass.getESuperTypes().add(this.getAbstractStack_map_frame());
		same_locals_1_stack_item_frameEClass.getESuperTypes().add(this.getAbstractStack_map_frame());
		same_locals_1_stack_item_frame_extendedEClass.getESuperTypes().add(this.getAbstractStack_map_frame());
		same_frame_extendedEClass.getESuperTypes().add(this.getAbstractStack_map_frame());
		chop_frameEClass.getESuperTypes().add(this.getAbstractStack_map_frame());
		append_frameEClass.getESuperTypes().add(this.getAbstractStack_map_frame());
		full_frameEClass.getESuperTypes().add(this.getAbstractStack_map_frame());
		exceptions_attributeEClass.getESuperTypes().add(this.getInterfaceMethod_attribute_info());
		exceptions_attributeEClass.getESuperTypes().add(this.getAbstractAttribute_info());
		innerClasses_attributeEClass.getESuperTypes().add(this.getInterfaceClass_attribute_info());
		synthetic_attributeEClass.getESuperTypes().add(this.getAbstractAttribute_info());
		synthetic_attributeEClass.getESuperTypes().add(this.getInterfaceClass_attribute_info());
		synthetic_attributeEClass.getESuperTypes().add(this.getInterfaceField_attribute_info());
		synthetic_attributeEClass.getESuperTypes().add(this.getInterfaceMethod_attribute_info());
		signature_attributeEClass.getESuperTypes().add(this.getInterfaceClass_attribute_info());
		signature_attributeEClass.getESuperTypes().add(this.getInterfaceField_attribute_info());
		signature_attributeEClass.getESuperTypes().add(this.getInterfaceMethod_attribute_info());
		signature_attributeEClass.getESuperTypes().add(this.getAbstractAttribute_info());

		// Initialize classes, features, and operations; add parameters
		initEClass(constanT_MethodHandle_InfoEClass, CONSTANT_MethodHandle_Info.class, "CONSTANT_MethodHandle_Info",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCONSTANT_MethodHandle_Info_Reference_kind(), theXMLTypePackage.getUnsignedByte(),
				"reference_kind", null, 0, 1, CONSTANT_MethodHandle_Info.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCONSTANT_MethodHandle_Info_Constant_ref_info(), this.getInterfaceCONSTANT_Ref_info(), null,
				"constant_ref_info", null, 1, 1, CONSTANT_MethodHandle_Info.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constanT_Fieldref_infoEClass, CONSTANT_Fieldref_info.class, "CONSTANT_Fieldref_info", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCONSTANT_Fieldref_info_Constant_class_info(), this.getCONSTANT_Class_info(), null,
				"constant_class_info", null, 1, 1, CONSTANT_Fieldref_info.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCONSTANT_Fieldref_info_Constant_nameandtype_info(), this.getCONSTANT_NameAndType_info(), null,
				"constant_nameandtype_info", null, 1, 1, CONSTANT_Fieldref_info.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constanT_Methodref_infoEClass, CONSTANT_Methodref_info.class, "CONSTANT_Methodref_info",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCONSTANT_Methodref_info_Constant_class_info(), this.getCONSTANT_Class_info(), null,
				"constant_class_info", null, 1, 1, CONSTANT_Methodref_info.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCONSTANT_Methodref_info_Constant_nameandtype_info(), this.getCONSTANT_NameAndType_info(),
				null, "constant_nameandtype_info", null, 1, 1, CONSTANT_Methodref_info.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(constanT_InterfaceMethodref_infoEClass, CONSTANT_InterfaceMethodref_info.class,
				"CONSTANT_InterfaceMethodref_info", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCONSTANT_InterfaceMethodref_info_Constant_class_info(), this.getCONSTANT_Class_info(), null,
				"constant_class_info", null, 1, 1, CONSTANT_InterfaceMethodref_info.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCONSTANT_InterfaceMethodref_info_Constant_nameandtype_info(),
				this.getCONSTANT_NameAndType_info(), null, "constant_nameandtype_info", null, 1, 1,
				CONSTANT_InterfaceMethodref_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constanT_Class_infoEClass, CONSTANT_Class_info.class, "CONSTANT_Class_info", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCONSTANT_Class_info_Name(), this.getCONSTANT_Utf8_info(), null, "name", null, 1, 1,
				CONSTANT_Class_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constanT_NameAndType_infoEClass, CONSTANT_NameAndType_info.class, "CONSTANT_NameAndType_info",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCONSTANT_NameAndType_info_Name(), this.getCONSTANT_Utf8_info(), null, "name", null, 1, 1,
				CONSTANT_NameAndType_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCONSTANT_NameAndType_info_Field_method_descriptor(), this.getCONSTANT_Utf8_info(), null,
				"field_method_descriptor", null, 1, 1, CONSTANT_NameAndType_info.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constanT_Utf8_infoEClass, CONSTANT_Utf8_info.class, "CONSTANT_Utf8_info", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCONSTANT_Utf8_info_Length(), theXMLTypePackage.getUnsignedShort(), "length", null, 0, 1,
				CONSTANT_Utf8_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getCONSTANT_Utf8_info_Bytes(), ecorePackage.getEByteArray(), "bytes", null, 0, 1,
				CONSTANT_Utf8_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(constanT_MethodType_infoEClass, CONSTANT_MethodType_info.class, "CONSTANT_MethodType_info",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCONSTANT_MethodType_info_Method_descriptor(), this.getCONSTANT_Utf8_info(), null,
				"method_descriptor", null, 1, 1, CONSTANT_MethodType_info.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constanT_String_infoEClass, CONSTANT_String_info.class, "CONSTANT_String_info", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCONSTANT_String_info_Constant_utf8_info(), this.getCONSTANT_Utf8_info(), null,
				"constant_utf8_info", null, 1, 1, CONSTANT_String_info.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constanT_InvokeDynamic_infoEClass, CONSTANT_InvokeDynamic_info.class, "CONSTANT_InvokeDynamic_info",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCONSTANT_InvokeDynamic_info_Constant_nameandtype_info(), this.getCONSTANT_NameAndType_info(),
				null, "constant_nameandtype_info", null, 1, 1, CONSTANT_InvokeDynamic_info.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(field_infoEClass, Field_info.class, "Field_info", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getField_info_Name(), this.getCONSTANT_Utf8_info(), null, "name", null, 1, 1, Field_info.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getField_info_Field_descriptor(), this.getCONSTANT_Utf8_info(), null, "field_descriptor", null,
				1, 1, Field_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getField_info_Access_flags(), theXMLTypePackage.getUnsignedShort(), "access_flags", null, 0, 1,
				Field_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getField_info_Attributes_count(), theXMLTypePackage.getUnsignedShort(), "attributes_count", null,
				0, 1, Field_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getField_info_Interfacefield_attribute_info(), this.getInterfaceField_attribute_info(), null,
				"interfacefield_attribute_info", null, 0, -1, Field_info.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(method_infoEClass, Method_info.class, "Method_info", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMethod_info_Access_flags(), theXMLTypePackage.getUnsignedShort(), "access_flags", null, 0, 1,
				Method_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMethod_info_Attributes_count(), theXMLTypePackage.getUnsignedShort(), "attributes_count",
				null, 0, 1, Method_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMethod_info_Method_descriptor(), this.getCONSTANT_Utf8_info(), null, "method_descriptor",
				null, 1, 1, Method_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMethod_info_Name(), this.getCONSTANT_Utf8_info(), null, "name", null, 1, 1, Method_info.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMethod_info_Interfacemethod_attribute_info(), this.getInterfaceMethod_attribute_info(), null,
				"interfacemethod_attribute_info", null, 0, -1, Method_info.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractCp_infoEClass, AbstractCp_info.class, "AbstractCp_info", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractCp_info_Tag(), theXMLTypePackage.getUnsignedByte(), "tag", null, 0, 1,
				AbstractCp_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(interfaceCONSTANT_Ref_infoEClass, InterfaceCONSTANT_Ref_info.class, "InterfaceCONSTANT_Ref_info",
				IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(constanT_Integer_infoEClass, CONSTANT_Integer_info.class, "CONSTANT_Integer_info", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCONSTANT_Integer_info_Bytes(), theXMLTypePackage.getUnsignedInt(), "bytes", null, 0, 1,
				CONSTANT_Integer_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constanT_Float_infoEClass, CONSTANT_Float_info.class, "CONSTANT_Float_info", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCONSTANT_Float_info_Bytes(), theXMLTypePackage.getUnsignedInt(), "bytes", null, 0, 1,
				CONSTANT_Float_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constanT_Long_infoEClass, CONSTANT_Long_info.class, "CONSTANT_Long_info", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCONSTANT_Long_info_High_bytes(), theXMLTypePackage.getUnsignedInt(), "high_bytes", null, 0, 1,
				CONSTANT_Long_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getCONSTANT_Long_info_Low_bytes(), theXMLTypePackage.getUnsignedInt(), "low_bytes", null, 0, 1,
				CONSTANT_Long_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(constanT_Double_infoEClass, CONSTANT_Double_info.class, "CONSTANT_Double_info", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCONSTANT_Double_info_High_bytes(), theXMLTypePackage.getUnsignedInt(), "high_bytes", null, 0,
				1, CONSTANT_Double_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCONSTANT_Double_info_Low_bytes(), theXMLTypePackage.getUnsignedInt(), "low_bytes", null, 0, 1,
				CONSTANT_Double_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(classFileEClass, ClassFile.class, "ClassFile", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getClassFile_Magic(), theXMLTypePackage.getUnsignedInt(), "magic", null, 0, 1, ClassFile.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClassFile_Minor_version(), theXMLTypePackage.getUnsignedShort(), "minor_version", null, 0, 1,
				ClassFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getClassFile_Major_version(), theXMLTypePackage.getUnsignedShort(), "major_version", null, 0, 1,
				ClassFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getClassFile_Constant_pool_count(), theXMLTypePackage.getUnsignedShort(), "constant_pool_count",
				null, 0, 1, ClassFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClassFile_Access_flags(), theXMLTypePackage.getUnsignedShort(), "access_flags", null, 0, 1,
				ClassFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getClassFile_Interfaces_count(), theXMLTypePackage.getUnsignedShort(), "interfaces_count", null,
				0, 1, ClassFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getClassFile_Fields_count(), theXMLTypePackage.getUnsignedShort(), "fields_count", null, 0, 1,
				ClassFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getClassFile_Methods_count(), theXMLTypePackage.getUnsignedShort(), "methods_count", null, 0, 1,
				ClassFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getClassFile_Attributes_count(), theXMLTypePackage.getUnsignedShort(), "attributes_count", null,
				0, 1, ClassFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getClassFile_Cp_info(), this.getAbstractCp_info(), null, "cp_info", null, 0, -1, ClassFile.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassFile_Field_info(), this.getField_info(), null, "field_info", null, 0, -1,
				ClassFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassFile_Method_info(), this.getMethod_info(), null, "method_info", null, 0, -1,
				ClassFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassFile_Super_class(), this.getCONSTANT_Class_info(), null, "super_class", null, 0, 1,
				ClassFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassFile_Interfaces(), this.getCONSTANT_Class_info(), null, "interfaces", null, 0, -1,
				ClassFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassFile_This_class(), this.getCONSTANT_Class_info(), null, "this_class", null, 1, 1,
				ClassFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassFile_Interfaceclass_attribute_info(), this.getInterfaceClass_attribute_info(), null,
				"interfaceclass_attribute_info", null, 0, -1, ClassFile.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractAttribute_infoEClass, AbstractAttribute_info.class, "AbstractAttribute_info", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractAttribute_info_Attribute_length(), theXMLTypePackage.getUnsignedInt(),
				"attribute_length", null, 0, 1, AbstractAttribute_info.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractAttribute_info_Attribute_name(), this.getCONSTANT_Utf8_info(), null, "attribute_name",
				null, 1, 1, AbstractAttribute_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interfaceMethod_attribute_infoEClass, InterfaceMethod_attribute_info.class,
				"InterfaceMethod_attribute_info", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(interfaceField_attribute_infoEClass, InterfaceField_attribute_info.class,
				"InterfaceField_attribute_info", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(interfaceClass_attribute_infoEClass, InterfaceClass_attribute_info.class,
				"InterfaceClass_attribute_info", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(constantValue_attributeEClass, ConstantValue_attribute.class, "ConstantValue_attribute",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConstantValue_attribute_Interfaceconstant_value(), this.getInterfaceCONSTANT_Value(), null,
				"interfaceconstant_value", null, 1, 1, ConstantValue_attribute.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interfaceCONSTANT_ValueEClass, InterfaceCONSTANT_Value.class, "InterfaceCONSTANT_Value", IS_ABSTRACT,
				IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(code_attributeEClass, Code_attribute.class, "Code_attribute", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCode_attribute_Max_stack(), theXMLTypePackage.getUnsignedShort(), "max_stack", null, 0, 1,
				Code_attribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getCode_attribute_Max_locals(), theXMLTypePackage.getUnsignedShort(), "max_locals", null, 0, 1,
				Code_attribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getCode_attribute_Code_length(), theXMLTypePackage.getUnsignedInt(), "code_length", null, 0, 1,
				Code_attribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getCode_attribute_Code(), ecorePackage.getEByteArray(), "code", null, 0, 1, Code_attribute.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCode_attribute_Exception_table(), this.getexception_table_entry(), null, "exception_table",
				null, 0, -1, Code_attribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCode_attribute_Exception_table_length(), theXMLTypePackage.getUnsignedShort(),
				"exception_table_length", null, 0, 1, Code_attribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCode_attribute_Attributes_count(), theXMLTypePackage.getUnsignedShort(), "attributes_count",
				null, 0, 1, Code_attribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCode_attribute_Interfacecode_attribute(), this.getInterfaceCode_attribute(), null,
				"interfacecode_attribute", null, 0, -1, Code_attribute.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(exception_table_entryEClass, exception_table_entry.class, "exception_table_entry", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getexception_table_entry_Start_pc(), theXMLTypePackage.getUnsignedShort(), "start_pc", null, 0,
				1, exception_table_entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getexception_table_entry_End_pc(), theXMLTypePackage.getUnsignedShort(), "end_pc", null, 0, 1,
				exception_table_entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getexception_table_entry_Handler_pc(), theXMLTypePackage.getUnsignedShort(), "handler_pc", null,
				0, 1, exception_table_entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getexception_table_entry_Catch_type(), this.getCONSTANT_Class_info(), null, "catch_type", null,
				0, 1, exception_table_entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stackMapTable_attributeEClass, StackMapTable_attribute.class, "StackMapTable_attribute",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStackMapTable_attribute_Number_of_entries(), theXMLTypePackage.getUnsignedShort(),
				"number_of_entries", null, 0, 1, StackMapTable_attribute.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStackMapTable_attribute_Abstractstack_map_frame(), this.getAbstractStack_map_frame(), null,
				"abstractstack_map_frame", null, 0, -1, StackMapTable_attribute.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractVerification_type_infoEClass, AbstractVerification_type_info.class,
				"AbstractVerification_type_info", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractVerification_type_info_Tag(), theXMLTypePackage.getUnsignedByte(), "tag", null, 0, 1,
				AbstractVerification_type_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(top_variable_infoEClass, Top_variable_info.class, "Top_variable_info", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(integer_variable_infoEClass, Integer_variable_info.class, "Integer_variable_info", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(float_variable_infoEClass, Float_variable_info.class, "Float_variable_info", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(null_variable_infoEClass, Null_variable_info.class, "Null_variable_info", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(uninitializedThis_variable_infoEClass, UninitializedThis_variable_info.class,
				"UninitializedThis_variable_info", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(object_variable_infoEClass, Object_variable_info.class, "Object_variable_info", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getObject_variable_info_Constant_class_info(), this.getCONSTANT_Class_info(), null,
				"constant_class_info", null, 0, 1, Object_variable_info.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(newEClass36EClass, NewEClass36.class, "NewEClass36", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(uninitialized_variable_infoEClass, Uninitialized_variable_info.class, "Uninitialized_variable_info",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUninitialized_variable_info_Offset(), theXMLTypePackage.getUnsignedShort(), "offset", null, 0,
				1, Uninitialized_variable_info.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(long_variable_infoEClass, Long_variable_info.class, "Long_variable_info", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(double_variable_infoEClass, Double_variable_info.class, "Double_variable_info", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(abstractStack_map_frameEClass, AbstractStack_map_frame.class, "AbstractStack_map_frame", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractStack_map_frame_Frame_type(), theXMLTypePackage.getUnsignedByte(), "frame_type", null,
				0, 1, AbstractStack_map_frame.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(same_frameEClass, same_frame.class, "same_frame", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(same_locals_1_stack_item_frameEClass, same_locals_1_stack_item_frame.class,
				"same_locals_1_stack_item_frame", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getsame_locals_1_stack_item_frame_Stack(), this.getAbstractVerification_type_info(), null,
				"stack", null, 1, 1, same_locals_1_stack_item_frame.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(same_locals_1_stack_item_frame_extendedEClass, same_locals_1_stack_item_frame_extended.class,
				"same_locals_1_stack_item_frame_extended", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getsame_locals_1_stack_item_frame_extended_Offset_delta(), theXMLTypePackage.getUnsignedShort(),
				"offset_delta", null, 0, 1, same_locals_1_stack_item_frame_extended.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getsame_locals_1_stack_item_frame_extended_Stack(), this.getAbstractVerification_type_info(),
				null, "stack", null, 1, 1, same_locals_1_stack_item_frame_extended.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(same_frame_extendedEClass, same_frame_extended.class, "same_frame_extended", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getsame_frame_extended_Offset_delta(), theXMLTypePackage.getUnsignedShort(), "offset_delta",
				null, 0, 1, same_frame_extended.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(chop_frameEClass, chop_frame.class, "chop_frame", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getchop_frame_Offset_delta(), theXMLTypePackage.getUnsignedShort(), "offset_delta", null, 0, 1,
				chop_frame.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(append_frameEClass, append_frame.class, "append_frame", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getappend_frame_Offset_delta(), theXMLTypePackage.getUnsignedShort(), "offset_delta", null, 0, 1,
				append_frame.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getappend_frame_Locals(), this.getAbstractVerification_type_info(), null, "locals", null, 1, 3,
				append_frame.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(full_frameEClass, full_frame.class, "full_frame", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getfull_frame_Offset_delta(), theXMLTypePackage.getUnsignedShort(), "offset_delta", null, 0, 1,
				full_frame.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getfull_frame_Number_of_locals(), theXMLTypePackage.getUnsignedShort(), "number_of_locals", null,
				0, 1, full_frame.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getfull_frame_Number_of_stack_items(), theXMLTypePackage.getUnsignedShort(),
				"number_of_stack_items", null, 0, 1, full_frame.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getfull_frame_Locals(), this.getAbstractVerification_type_info(), null, "locals", null, 0, -1,
				full_frame.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getfull_frame_Stack(), this.getAbstractVerification_type_info(), null, "stack", null, 0, -1,
				full_frame.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(exceptions_attributeEClass, Exceptions_attribute.class, "Exceptions_attribute", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExceptions_attribute_Exception_table(), this.getCONSTANT_Class_info(), null,
				"exception_table", null, 0, -1, Exceptions_attribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExceptions_attribute_Number_of_exceptions(), theXMLTypePackage.getUnsignedShort(),
				"number_of_exceptions", null, 0, 1, Exceptions_attribute.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interfaceCode_attributeEClass, InterfaceCode_attribute.class, "InterfaceCode_attribute", IS_ABSTRACT,
				IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(innerClasses_attributeEClass, InnerClasses_attribute.class, "InnerClasses_attribute", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInnerClasses_attribute_Number_of_classes(), theXMLTypePackage.getUnsignedShort(),
				"number_of_classes", null, 0, 1, InnerClasses_attribute.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInnerClasses_attribute_Classes(), this.getClass_member(), null, "classes", null, 0, -1,
				InnerClasses_attribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(class_memberEClass, Class_member.class, "Class_member", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getClass_member_Inner_class_access_flags(), theXMLTypePackage.getUnsignedShort(),
				"inner_class_access_flags", null, 0, 1, Class_member.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClass_member_Inner_class_info(), this.getCONSTANT_Class_info(), null, "inner_class_info",
				null, 1, 1, Class_member.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClass_member_Outer_class_info(), this.getCONSTANT_Class_info(), null, "outer_class_info",
				null, 1, 1, Class_member.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClass_member_Inner_name(), this.getCONSTANT_Utf8_info(), null, "inner_name", null, 1, 1,
				Class_member.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(synthetic_attributeEClass, Synthetic_attribute.class, "Synthetic_attribute", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(signature_attributeEClass, Signature_attribute.class, "Signature_attribute", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSignature_attribute_Signature(), this.getCONSTANT_Utf8_info(), null, "signature", null, 1, 1,
				Signature_attribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(newEClass54EClass, NewEClass54.class, "NewEClass54", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //EcoregraphPackageImpl
