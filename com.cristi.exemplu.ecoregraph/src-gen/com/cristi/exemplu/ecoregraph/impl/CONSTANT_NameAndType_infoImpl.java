/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CONSTANT Name And Type info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_NameAndType_infoImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_NameAndType_infoImpl#getField_method_descriptor <em>Field method descriptor</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CONSTANT_NameAndType_infoImpl extends AbstractCp_infoImpl implements CONSTANT_NameAndType_info {
	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Utf8_info name;

	/**
	 * The cached value of the '{@link #getField_method_descriptor() <em>Field method descriptor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getField_method_descriptor()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Utf8_info field_method_descriptor;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CONSTANT_NameAndType_infoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.CONSTANT_NAME_AND_TYPE_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info getName() {
		if (name != null && name.eIsProxy()) {
			InternalEObject oldName = (InternalEObject) name;
			name = (CONSTANT_Utf8_info) eResolveProxy(oldName);
			if (name != oldName) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.CONSTANT_NAME_AND_TYPE_INFO__NAME, oldName, name));
			}
		}
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info basicGetName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(CONSTANT_Utf8_info newName) {
		CONSTANT_Utf8_info oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CONSTANT_NAME_AND_TYPE_INFO__NAME,
					oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info getField_method_descriptor() {
		if (field_method_descriptor != null && field_method_descriptor.eIsProxy()) {
			InternalEObject oldField_method_descriptor = (InternalEObject) field_method_descriptor;
			field_method_descriptor = (CONSTANT_Utf8_info) eResolveProxy(oldField_method_descriptor);
			if (field_method_descriptor != oldField_method_descriptor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.CONSTANT_NAME_AND_TYPE_INFO__FIELD_METHOD_DESCRIPTOR,
							oldField_method_descriptor, field_method_descriptor));
			}
		}
		return field_method_descriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info basicGetField_method_descriptor() {
		return field_method_descriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setField_method_descriptor(CONSTANT_Utf8_info newField_method_descriptor) {
		CONSTANT_Utf8_info oldField_method_descriptor = field_method_descriptor;
		field_method_descriptor = newField_method_descriptor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.CONSTANT_NAME_AND_TYPE_INFO__FIELD_METHOD_DESCRIPTOR, oldField_method_descriptor,
					field_method_descriptor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_NAME_AND_TYPE_INFO__NAME:
			if (resolve)
				return getName();
			return basicGetName();
		case EcoregraphPackage.CONSTANT_NAME_AND_TYPE_INFO__FIELD_METHOD_DESCRIPTOR:
			if (resolve)
				return getField_method_descriptor();
			return basicGetField_method_descriptor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_NAME_AND_TYPE_INFO__NAME:
			setName((CONSTANT_Utf8_info) newValue);
			return;
		case EcoregraphPackage.CONSTANT_NAME_AND_TYPE_INFO__FIELD_METHOD_DESCRIPTOR:
			setField_method_descriptor((CONSTANT_Utf8_info) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_NAME_AND_TYPE_INFO__NAME:
			setName((CONSTANT_Utf8_info) null);
			return;
		case EcoregraphPackage.CONSTANT_NAME_AND_TYPE_INFO__FIELD_METHOD_DESCRIPTOR:
			setField_method_descriptor((CONSTANT_Utf8_info) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_NAME_AND_TYPE_INFO__NAME:
			return name != null;
		case EcoregraphPackage.CONSTANT_NAME_AND_TYPE_INFO__FIELD_METHOD_DESCRIPTOR:
			return field_method_descriptor != null;
		}
		return super.eIsSet(featureID);
	}

} //CONSTANT_NameAndType_infoImpl
