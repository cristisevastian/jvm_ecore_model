/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.NewEClass54;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>New EClass54</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NewEClass54Impl extends MinimalEObjectImpl.Container implements NewEClass54 {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NewEClass54Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.NEW_ECLASS54;
	}

} //NewEClass54Impl
