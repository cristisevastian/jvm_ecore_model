/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.Code_attribute;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.InterfaceCode_attribute;
import com.cristi.exemplu.ecoregraph.exception_table_entry;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Code attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Code_attributeImpl#getMax_stack <em>Max stack</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Code_attributeImpl#getMax_locals <em>Max locals</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Code_attributeImpl#getCode_length <em>Code length</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Code_attributeImpl#getCode <em>Code</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Code_attributeImpl#getException_table <em>Exception table</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Code_attributeImpl#getException_table_length <em>Exception table length</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Code_attributeImpl#getAttributes_count <em>Attributes count</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Code_attributeImpl#getInterfacecode_attribute <em>Interfacecode attribute</em>}</li>
 * </ul>
 *
 * @generated
 */
public class Code_attributeImpl extends AbstractAttribute_infoImpl implements Code_attribute {
	/**
	 * The default value of the '{@link #getMax_stack() <em>Max stack</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_stack()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_STACK_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMax_stack() <em>Max stack</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_stack()
	 * @generated
	 * @ordered
	 */
	protected int max_stack = MAX_STACK_EDEFAULT;

	/**
	 * The default value of the '{@link #getMax_locals() <em>Max locals</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_locals()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_LOCALS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMax_locals() <em>Max locals</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_locals()
	 * @generated
	 * @ordered
	 */
	protected int max_locals = MAX_LOCALS_EDEFAULT;

	/**
	 * The default value of the '{@link #getCode_length() <em>Code length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode_length()
	 * @generated
	 * @ordered
	 */
	protected static final long CODE_LENGTH_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getCode_length() <em>Code length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode_length()
	 * @generated
	 * @ordered
	 */
	protected long code_length = CODE_LENGTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected byte[] code = CODE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getException_table() <em>Exception table</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getException_table()
	 * @generated
	 * @ordered
	 */
	protected EList<exception_table_entry> exception_table;

	/**
	 * The default value of the '{@link #getException_table_length() <em>Exception table length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getException_table_length()
	 * @generated
	 * @ordered
	 */
	protected static final int EXCEPTION_TABLE_LENGTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getException_table_length() <em>Exception table length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getException_table_length()
	 * @generated
	 * @ordered
	 */
	protected int exception_table_length = EXCEPTION_TABLE_LENGTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getAttributes_count() <em>Attributes count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributes_count()
	 * @generated
	 * @ordered
	 */
	protected static final int ATTRIBUTES_COUNT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getAttributes_count() <em>Attributes count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributes_count()
	 * @generated
	 * @ordered
	 */
	protected int attributes_count = ATTRIBUTES_COUNT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInterfacecode_attribute() <em>Interfacecode attribute</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfacecode_attribute()
	 * @generated
	 * @ordered
	 */
	protected EList<InterfaceCode_attribute> interfacecode_attribute;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Code_attributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.CODE_ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMax_stack() {
		return max_stack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax_stack(int newMax_stack) {
		int oldMax_stack = max_stack;
		max_stack = newMax_stack;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CODE_ATTRIBUTE__MAX_STACK,
					oldMax_stack, max_stack));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMax_locals() {
		return max_locals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax_locals(int newMax_locals) {
		int oldMax_locals = max_locals;
		max_locals = newMax_locals;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CODE_ATTRIBUTE__MAX_LOCALS,
					oldMax_locals, max_locals));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getCode_length() {
		return code_length;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode_length(long newCode_length) {
		long oldCode_length = code_length;
		code_length = newCode_length;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CODE_ATTRIBUTE__CODE_LENGTH,
					oldCode_length, code_length));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public byte[] getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(byte[] newCode) {
		byte[] oldCode = code;
		code = newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CODE_ATTRIBUTE__CODE, oldCode,
					code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<exception_table_entry> getException_table() {
		if (exception_table == null) {
			exception_table = new EObjectContainmentEList<exception_table_entry>(exception_table_entry.class, this,
					EcoregraphPackage.CODE_ATTRIBUTE__EXCEPTION_TABLE);
		}
		return exception_table;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getException_table_length() {
		return exception_table_length;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setException_table_length(int newException_table_length) {
		int oldException_table_length = exception_table_length;
		exception_table_length = newException_table_length;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.CODE_ATTRIBUTE__EXCEPTION_TABLE_LENGTH, oldException_table_length,
					exception_table_length));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAttributes_count() {
		return attributes_count;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributes_count(int newAttributes_count) {
		int oldAttributes_count = attributes_count;
		attributes_count = newAttributes_count;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CODE_ATTRIBUTE__ATTRIBUTES_COUNT,
					oldAttributes_count, attributes_count));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterfaceCode_attribute> getInterfacecode_attribute() {
		if (interfacecode_attribute == null) {
			interfacecode_attribute = new EObjectContainmentEList<InterfaceCode_attribute>(
					InterfaceCode_attribute.class, this, EcoregraphPackage.CODE_ATTRIBUTE__INTERFACECODE_ATTRIBUTE);
		}
		return interfacecode_attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case EcoregraphPackage.CODE_ATTRIBUTE__EXCEPTION_TABLE:
			return ((InternalEList<?>) getException_table()).basicRemove(otherEnd, msgs);
		case EcoregraphPackage.CODE_ATTRIBUTE__INTERFACECODE_ATTRIBUTE:
			return ((InternalEList<?>) getInterfacecode_attribute()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.CODE_ATTRIBUTE__MAX_STACK:
			return getMax_stack();
		case EcoregraphPackage.CODE_ATTRIBUTE__MAX_LOCALS:
			return getMax_locals();
		case EcoregraphPackage.CODE_ATTRIBUTE__CODE_LENGTH:
			return getCode_length();
		case EcoregraphPackage.CODE_ATTRIBUTE__CODE:
			return getCode();
		case EcoregraphPackage.CODE_ATTRIBUTE__EXCEPTION_TABLE:
			return getException_table();
		case EcoregraphPackage.CODE_ATTRIBUTE__EXCEPTION_TABLE_LENGTH:
			return getException_table_length();
		case EcoregraphPackage.CODE_ATTRIBUTE__ATTRIBUTES_COUNT:
			return getAttributes_count();
		case EcoregraphPackage.CODE_ATTRIBUTE__INTERFACECODE_ATTRIBUTE:
			return getInterfacecode_attribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.CODE_ATTRIBUTE__MAX_STACK:
			setMax_stack((Integer) newValue);
			return;
		case EcoregraphPackage.CODE_ATTRIBUTE__MAX_LOCALS:
			setMax_locals((Integer) newValue);
			return;
		case EcoregraphPackage.CODE_ATTRIBUTE__CODE_LENGTH:
			setCode_length((Long) newValue);
			return;
		case EcoregraphPackage.CODE_ATTRIBUTE__CODE:
			setCode((byte[]) newValue);
			return;
		case EcoregraphPackage.CODE_ATTRIBUTE__EXCEPTION_TABLE:
			getException_table().clear();
			getException_table().addAll((Collection<? extends exception_table_entry>) newValue);
			return;
		case EcoregraphPackage.CODE_ATTRIBUTE__EXCEPTION_TABLE_LENGTH:
			setException_table_length((Integer) newValue);
			return;
		case EcoregraphPackage.CODE_ATTRIBUTE__ATTRIBUTES_COUNT:
			setAttributes_count((Integer) newValue);
			return;
		case EcoregraphPackage.CODE_ATTRIBUTE__INTERFACECODE_ATTRIBUTE:
			getInterfacecode_attribute().clear();
			getInterfacecode_attribute().addAll((Collection<? extends InterfaceCode_attribute>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CODE_ATTRIBUTE__MAX_STACK:
			setMax_stack(MAX_STACK_EDEFAULT);
			return;
		case EcoregraphPackage.CODE_ATTRIBUTE__MAX_LOCALS:
			setMax_locals(MAX_LOCALS_EDEFAULT);
			return;
		case EcoregraphPackage.CODE_ATTRIBUTE__CODE_LENGTH:
			setCode_length(CODE_LENGTH_EDEFAULT);
			return;
		case EcoregraphPackage.CODE_ATTRIBUTE__CODE:
			setCode(CODE_EDEFAULT);
			return;
		case EcoregraphPackage.CODE_ATTRIBUTE__EXCEPTION_TABLE:
			getException_table().clear();
			return;
		case EcoregraphPackage.CODE_ATTRIBUTE__EXCEPTION_TABLE_LENGTH:
			setException_table_length(EXCEPTION_TABLE_LENGTH_EDEFAULT);
			return;
		case EcoregraphPackage.CODE_ATTRIBUTE__ATTRIBUTES_COUNT:
			setAttributes_count(ATTRIBUTES_COUNT_EDEFAULT);
			return;
		case EcoregraphPackage.CODE_ATTRIBUTE__INTERFACECODE_ATTRIBUTE:
			getInterfacecode_attribute().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CODE_ATTRIBUTE__MAX_STACK:
			return max_stack != MAX_STACK_EDEFAULT;
		case EcoregraphPackage.CODE_ATTRIBUTE__MAX_LOCALS:
			return max_locals != MAX_LOCALS_EDEFAULT;
		case EcoregraphPackage.CODE_ATTRIBUTE__CODE_LENGTH:
			return code_length != CODE_LENGTH_EDEFAULT;
		case EcoregraphPackage.CODE_ATTRIBUTE__CODE:
			return CODE_EDEFAULT == null ? code != null : !CODE_EDEFAULT.equals(code);
		case EcoregraphPackage.CODE_ATTRIBUTE__EXCEPTION_TABLE:
			return exception_table != null && !exception_table.isEmpty();
		case EcoregraphPackage.CODE_ATTRIBUTE__EXCEPTION_TABLE_LENGTH:
			return exception_table_length != EXCEPTION_TABLE_LENGTH_EDEFAULT;
		case EcoregraphPackage.CODE_ATTRIBUTE__ATTRIBUTES_COUNT:
			return attributes_count != ATTRIBUTES_COUNT_EDEFAULT;
		case EcoregraphPackage.CODE_ATTRIBUTE__INTERFACECODE_ATTRIBUTE:
			return interfacecode_attribute != null && !interfacecode_attribute.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (max_stack: ");
		result.append(max_stack);
		result.append(", max_locals: ");
		result.append(max_locals);
		result.append(", code_length: ");
		result.append(code_length);
		result.append(", code: ");
		result.append(code);
		result.append(", exception_table_length: ");
		result.append(exception_table_length);
		result.append(", attributes_count: ");
		result.append(attributes_count);
		result.append(')');
		return result.toString();
	}

} //Code_attributeImpl
