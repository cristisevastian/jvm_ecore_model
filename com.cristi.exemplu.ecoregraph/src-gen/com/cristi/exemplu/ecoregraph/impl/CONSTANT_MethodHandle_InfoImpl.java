/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.CONSTANT_MethodHandle_Info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Ref_info;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CONSTANT Method Handle Info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_MethodHandle_InfoImpl#getReference_kind <em>Reference kind</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_MethodHandle_InfoImpl#getConstant_ref_info <em>Constant ref info</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CONSTANT_MethodHandle_InfoImpl extends AbstractCp_infoImpl implements CONSTANT_MethodHandle_Info {
	/**
	 * The default value of the '{@link #getReference_kind() <em>Reference kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReference_kind()
	 * @generated
	 * @ordered
	 */
	protected static final short REFERENCE_KIND_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getReference_kind() <em>Reference kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReference_kind()
	 * @generated
	 * @ordered
	 */
	protected short reference_kind = REFERENCE_KIND_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConstant_ref_info() <em>Constant ref info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant_ref_info()
	 * @generated
	 * @ordered
	 */
	protected InterfaceCONSTANT_Ref_info constant_ref_info;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CONSTANT_MethodHandle_InfoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.CONSTANT_METHOD_HANDLE_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public short getReference_kind() {
		return reference_kind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReference_kind(short newReference_kind) {
		short oldReference_kind = reference_kind;
		reference_kind = newReference_kind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.CONSTANT_METHOD_HANDLE_INFO__REFERENCE_KIND, oldReference_kind, reference_kind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceCONSTANT_Ref_info getConstant_ref_info() {
		if (constant_ref_info != null && constant_ref_info.eIsProxy()) {
			InternalEObject oldConstant_ref_info = (InternalEObject) constant_ref_info;
			constant_ref_info = (InterfaceCONSTANT_Ref_info) eResolveProxy(oldConstant_ref_info);
			if (constant_ref_info != oldConstant_ref_info) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.CONSTANT_METHOD_HANDLE_INFO__CONSTANT_REF_INFO, oldConstant_ref_info,
							constant_ref_info));
			}
		}
		return constant_ref_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceCONSTANT_Ref_info basicGetConstant_ref_info() {
		return constant_ref_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstant_ref_info(InterfaceCONSTANT_Ref_info newConstant_ref_info) {
		InterfaceCONSTANT_Ref_info oldConstant_ref_info = constant_ref_info;
		constant_ref_info = newConstant_ref_info;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.CONSTANT_METHOD_HANDLE_INFO__CONSTANT_REF_INFO, oldConstant_ref_info,
					constant_ref_info));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_METHOD_HANDLE_INFO__REFERENCE_KIND:
			return getReference_kind();
		case EcoregraphPackage.CONSTANT_METHOD_HANDLE_INFO__CONSTANT_REF_INFO:
			if (resolve)
				return getConstant_ref_info();
			return basicGetConstant_ref_info();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_METHOD_HANDLE_INFO__REFERENCE_KIND:
			setReference_kind((Short) newValue);
			return;
		case EcoregraphPackage.CONSTANT_METHOD_HANDLE_INFO__CONSTANT_REF_INFO:
			setConstant_ref_info((InterfaceCONSTANT_Ref_info) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_METHOD_HANDLE_INFO__REFERENCE_KIND:
			setReference_kind(REFERENCE_KIND_EDEFAULT);
			return;
		case EcoregraphPackage.CONSTANT_METHOD_HANDLE_INFO__CONSTANT_REF_INFO:
			setConstant_ref_info((InterfaceCONSTANT_Ref_info) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_METHOD_HANDLE_INFO__REFERENCE_KIND:
			return reference_kind != REFERENCE_KIND_EDEFAULT;
		case EcoregraphPackage.CONSTANT_METHOD_HANDLE_INFO__CONSTANT_REF_INFO:
			return constant_ref_info != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (reference_kind: ");
		result.append(reference_kind);
		result.append(')');
		return result.toString();
	}

} //CONSTANT_MethodHandle_InfoImpl
