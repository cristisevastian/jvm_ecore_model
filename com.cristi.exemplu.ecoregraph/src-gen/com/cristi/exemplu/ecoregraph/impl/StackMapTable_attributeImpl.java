/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.AbstractStack_map_frame;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.StackMapTable_attribute;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Stack Map Table attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.StackMapTable_attributeImpl#getNumber_of_entries <em>Number of entries</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.StackMapTable_attributeImpl#getAbstractstack_map_frame <em>Abstractstack map frame</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StackMapTable_attributeImpl extends AbstractAttribute_infoImpl implements StackMapTable_attribute {
	/**
	 * The default value of the '{@link #getNumber_of_entries() <em>Number of entries</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber_of_entries()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_ENTRIES_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumber_of_entries() <em>Number of entries</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber_of_entries()
	 * @generated
	 * @ordered
	 */
	protected int number_of_entries = NUMBER_OF_ENTRIES_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAbstractstack_map_frame() <em>Abstractstack map frame</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstractstack_map_frame()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractStack_map_frame> abstractstack_map_frame;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StackMapTable_attributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.STACK_MAP_TABLE_ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumber_of_entries() {
		return number_of_entries;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumber_of_entries(int newNumber_of_entries) {
		int oldNumber_of_entries = number_of_entries;
		number_of_entries = newNumber_of_entries;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.STACK_MAP_TABLE_ATTRIBUTE__NUMBER_OF_ENTRIES, oldNumber_of_entries,
					number_of_entries));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractStack_map_frame> getAbstractstack_map_frame() {
		if (abstractstack_map_frame == null) {
			abstractstack_map_frame = new EObjectContainmentEList<AbstractStack_map_frame>(
					AbstractStack_map_frame.class, this,
					EcoregraphPackage.STACK_MAP_TABLE_ATTRIBUTE__ABSTRACTSTACK_MAP_FRAME);
		}
		return abstractstack_map_frame;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case EcoregraphPackage.STACK_MAP_TABLE_ATTRIBUTE__ABSTRACTSTACK_MAP_FRAME:
			return ((InternalEList<?>) getAbstractstack_map_frame()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.STACK_MAP_TABLE_ATTRIBUTE__NUMBER_OF_ENTRIES:
			return getNumber_of_entries();
		case EcoregraphPackage.STACK_MAP_TABLE_ATTRIBUTE__ABSTRACTSTACK_MAP_FRAME:
			return getAbstractstack_map_frame();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.STACK_MAP_TABLE_ATTRIBUTE__NUMBER_OF_ENTRIES:
			setNumber_of_entries((Integer) newValue);
			return;
		case EcoregraphPackage.STACK_MAP_TABLE_ATTRIBUTE__ABSTRACTSTACK_MAP_FRAME:
			getAbstractstack_map_frame().clear();
			getAbstractstack_map_frame().addAll((Collection<? extends AbstractStack_map_frame>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.STACK_MAP_TABLE_ATTRIBUTE__NUMBER_OF_ENTRIES:
			setNumber_of_entries(NUMBER_OF_ENTRIES_EDEFAULT);
			return;
		case EcoregraphPackage.STACK_MAP_TABLE_ATTRIBUTE__ABSTRACTSTACK_MAP_FRAME:
			getAbstractstack_map_frame().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.STACK_MAP_TABLE_ATTRIBUTE__NUMBER_OF_ENTRIES:
			return number_of_entries != NUMBER_OF_ENTRIES_EDEFAULT;
		case EcoregraphPackage.STACK_MAP_TABLE_ATTRIBUTE__ABSTRACTSTACK_MAP_FRAME:
			return abstractstack_map_frame != null && !abstractstack_map_frame.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (number_of_entries: ");
		result.append(number_of_entries);
		result.append(')');
		return result.toString();
	}

} //StackMapTable_attributeImpl
