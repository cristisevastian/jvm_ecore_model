/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.CONSTANT_Class_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info;
import com.cristi.exemplu.ecoregraph.Class_member;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class member</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Class_memberImpl#getInner_class_access_flags <em>Inner class access flags</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Class_memberImpl#getInner_class_info <em>Inner class info</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Class_memberImpl#getOuter_class_info <em>Outer class info</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Class_memberImpl#getInner_name <em>Inner name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class Class_memberImpl extends MinimalEObjectImpl.Container implements Class_member {
	/**
	 * The default value of the '{@link #getInner_class_access_flags() <em>Inner class access flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInner_class_access_flags()
	 * @generated
	 * @ordered
	 */
	protected static final int INNER_CLASS_ACCESS_FLAGS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getInner_class_access_flags() <em>Inner class access flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInner_class_access_flags()
	 * @generated
	 * @ordered
	 */
	protected int inner_class_access_flags = INNER_CLASS_ACCESS_FLAGS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInner_class_info() <em>Inner class info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInner_class_info()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Class_info inner_class_info;

	/**
	 * The cached value of the '{@link #getOuter_class_info() <em>Outer class info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOuter_class_info()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Class_info outer_class_info;

	/**
	 * The cached value of the '{@link #getInner_name() <em>Inner name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInner_name()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Utf8_info inner_name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Class_memberImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.CLASS_MEMBER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getInner_class_access_flags() {
		return inner_class_access_flags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInner_class_access_flags(int newInner_class_access_flags) {
		int oldInner_class_access_flags = inner_class_access_flags;
		inner_class_access_flags = newInner_class_access_flags;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.CLASS_MEMBER__INNER_CLASS_ACCESS_FLAGS, oldInner_class_access_flags,
					inner_class_access_flags));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Class_info getInner_class_info() {
		if (inner_class_info != null && inner_class_info.eIsProxy()) {
			InternalEObject oldInner_class_info = (InternalEObject) inner_class_info;
			inner_class_info = (CONSTANT_Class_info) eResolveProxy(oldInner_class_info);
			if (inner_class_info != oldInner_class_info) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.CLASS_MEMBER__INNER_CLASS_INFO, oldInner_class_info, inner_class_info));
			}
		}
		return inner_class_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Class_info basicGetInner_class_info() {
		return inner_class_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInner_class_info(CONSTANT_Class_info newInner_class_info) {
		CONSTANT_Class_info oldInner_class_info = inner_class_info;
		inner_class_info = newInner_class_info;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CLASS_MEMBER__INNER_CLASS_INFO,
					oldInner_class_info, inner_class_info));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Class_info getOuter_class_info() {
		if (outer_class_info != null && outer_class_info.eIsProxy()) {
			InternalEObject oldOuter_class_info = (InternalEObject) outer_class_info;
			outer_class_info = (CONSTANT_Class_info) eResolveProxy(oldOuter_class_info);
			if (outer_class_info != oldOuter_class_info) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.CLASS_MEMBER__OUTER_CLASS_INFO, oldOuter_class_info, outer_class_info));
			}
		}
		return outer_class_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Class_info basicGetOuter_class_info() {
		return outer_class_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOuter_class_info(CONSTANT_Class_info newOuter_class_info) {
		CONSTANT_Class_info oldOuter_class_info = outer_class_info;
		outer_class_info = newOuter_class_info;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CLASS_MEMBER__OUTER_CLASS_INFO,
					oldOuter_class_info, outer_class_info));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info getInner_name() {
		if (inner_name != null && inner_name.eIsProxy()) {
			InternalEObject oldInner_name = (InternalEObject) inner_name;
			inner_name = (CONSTANT_Utf8_info) eResolveProxy(oldInner_name);
			if (inner_name != oldInner_name) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.CLASS_MEMBER__INNER_NAME, oldInner_name, inner_name));
			}
		}
		return inner_name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info basicGetInner_name() {
		return inner_name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInner_name(CONSTANT_Utf8_info newInner_name) {
		CONSTANT_Utf8_info oldInner_name = inner_name;
		inner_name = newInner_name;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.CLASS_MEMBER__INNER_NAME,
					oldInner_name, inner_name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.CLASS_MEMBER__INNER_CLASS_ACCESS_FLAGS:
			return getInner_class_access_flags();
		case EcoregraphPackage.CLASS_MEMBER__INNER_CLASS_INFO:
			if (resolve)
				return getInner_class_info();
			return basicGetInner_class_info();
		case EcoregraphPackage.CLASS_MEMBER__OUTER_CLASS_INFO:
			if (resolve)
				return getOuter_class_info();
			return basicGetOuter_class_info();
		case EcoregraphPackage.CLASS_MEMBER__INNER_NAME:
			if (resolve)
				return getInner_name();
			return basicGetInner_name();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.CLASS_MEMBER__INNER_CLASS_ACCESS_FLAGS:
			setInner_class_access_flags((Integer) newValue);
			return;
		case EcoregraphPackage.CLASS_MEMBER__INNER_CLASS_INFO:
			setInner_class_info((CONSTANT_Class_info) newValue);
			return;
		case EcoregraphPackage.CLASS_MEMBER__OUTER_CLASS_INFO:
			setOuter_class_info((CONSTANT_Class_info) newValue);
			return;
		case EcoregraphPackage.CLASS_MEMBER__INNER_NAME:
			setInner_name((CONSTANT_Utf8_info) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CLASS_MEMBER__INNER_CLASS_ACCESS_FLAGS:
			setInner_class_access_flags(INNER_CLASS_ACCESS_FLAGS_EDEFAULT);
			return;
		case EcoregraphPackage.CLASS_MEMBER__INNER_CLASS_INFO:
			setInner_class_info((CONSTANT_Class_info) null);
			return;
		case EcoregraphPackage.CLASS_MEMBER__OUTER_CLASS_INFO:
			setOuter_class_info((CONSTANT_Class_info) null);
			return;
		case EcoregraphPackage.CLASS_MEMBER__INNER_NAME:
			setInner_name((CONSTANT_Utf8_info) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CLASS_MEMBER__INNER_CLASS_ACCESS_FLAGS:
			return inner_class_access_flags != INNER_CLASS_ACCESS_FLAGS_EDEFAULT;
		case EcoregraphPackage.CLASS_MEMBER__INNER_CLASS_INFO:
			return inner_class_info != null;
		case EcoregraphPackage.CLASS_MEMBER__OUTER_CLASS_INFO:
			return outer_class_info != null;
		case EcoregraphPackage.CLASS_MEMBER__INNER_NAME:
			return inner_name != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (inner_class_access_flags: ");
		result.append(inner_class_access_flags);
		result.append(')');
		return result.toString();
	}

} //Class_memberImpl
