/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.CONSTANT_String_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CONSTANT String info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_String_infoImpl#getConstant_utf8_info <em>Constant utf8 info</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CONSTANT_String_infoImpl extends AbstractCp_infoImpl implements CONSTANT_String_info {
	/**
	 * The cached value of the '{@link #getConstant_utf8_info() <em>Constant utf8 info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant_utf8_info()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Utf8_info constant_utf8_info;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CONSTANT_String_infoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.CONSTANT_STRING_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info getConstant_utf8_info() {
		if (constant_utf8_info != null && constant_utf8_info.eIsProxy()) {
			InternalEObject oldConstant_utf8_info = (InternalEObject) constant_utf8_info;
			constant_utf8_info = (CONSTANT_Utf8_info) eResolveProxy(oldConstant_utf8_info);
			if (constant_utf8_info != oldConstant_utf8_info) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.CONSTANT_STRING_INFO__CONSTANT_UTF8_INFO, oldConstant_utf8_info,
							constant_utf8_info));
			}
		}
		return constant_utf8_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info basicGetConstant_utf8_info() {
		return constant_utf8_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstant_utf8_info(CONSTANT_Utf8_info newConstant_utf8_info) {
		CONSTANT_Utf8_info oldConstant_utf8_info = constant_utf8_info;
		constant_utf8_info = newConstant_utf8_info;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.CONSTANT_STRING_INFO__CONSTANT_UTF8_INFO, oldConstant_utf8_info,
					constant_utf8_info));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_STRING_INFO__CONSTANT_UTF8_INFO:
			if (resolve)
				return getConstant_utf8_info();
			return basicGetConstant_utf8_info();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_STRING_INFO__CONSTANT_UTF8_INFO:
			setConstant_utf8_info((CONSTANT_Utf8_info) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_STRING_INFO__CONSTANT_UTF8_INFO:
			setConstant_utf8_info((CONSTANT_Utf8_info) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.CONSTANT_STRING_INFO__CONSTANT_UTF8_INFO:
			return constant_utf8_info != null;
		}
		return super.eIsSet(featureID);
	}

} //CONSTANT_String_infoImpl
