/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.AbstractVerification_type_info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.full_frame;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>full frame</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.full_frameImpl#getOffset_delta <em>Offset delta</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.full_frameImpl#getNumber_of_locals <em>Number of locals</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.full_frameImpl#getNumber_of_stack_items <em>Number of stack items</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.full_frameImpl#getLocals <em>Locals</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.full_frameImpl#getStack <em>Stack</em>}</li>
 * </ul>
 *
 * @generated
 */
public class full_frameImpl extends AbstractStack_map_frameImpl implements full_frame {
	/**
	 * The default value of the '{@link #getOffset_delta() <em>Offset delta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset_delta()
	 * @generated
	 * @ordered
	 */
	protected static final int OFFSET_DELTA_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getOffset_delta() <em>Offset delta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset_delta()
	 * @generated
	 * @ordered
	 */
	protected int offset_delta = OFFSET_DELTA_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumber_of_locals() <em>Number of locals</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber_of_locals()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_LOCALS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumber_of_locals() <em>Number of locals</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber_of_locals()
	 * @generated
	 * @ordered
	 */
	protected int number_of_locals = NUMBER_OF_LOCALS_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumber_of_stack_items() <em>Number of stack items</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber_of_stack_items()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_STACK_ITEMS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumber_of_stack_items() <em>Number of stack items</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber_of_stack_items()
	 * @generated
	 * @ordered
	 */
	protected int number_of_stack_items = NUMBER_OF_STACK_ITEMS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLocals() <em>Locals</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocals()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractVerification_type_info> locals;

	/**
	 * The cached value of the '{@link #getStack() <em>Stack</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStack()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractVerification_type_info> stack;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected full_frameImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.FULL_FRAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOffset_delta() {
		return offset_delta;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOffset_delta(int newOffset_delta) {
		int oldOffset_delta = offset_delta;
		offset_delta = newOffset_delta;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.FULL_FRAME__OFFSET_DELTA,
					oldOffset_delta, offset_delta));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumber_of_locals() {
		return number_of_locals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumber_of_locals(int newNumber_of_locals) {
		int oldNumber_of_locals = number_of_locals;
		number_of_locals = newNumber_of_locals;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.FULL_FRAME__NUMBER_OF_LOCALS,
					oldNumber_of_locals, number_of_locals));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumber_of_stack_items() {
		return number_of_stack_items;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumber_of_stack_items(int newNumber_of_stack_items) {
		int oldNumber_of_stack_items = number_of_stack_items;
		number_of_stack_items = newNumber_of_stack_items;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EcoregraphPackage.FULL_FRAME__NUMBER_OF_STACK_ITEMS,
					oldNumber_of_stack_items, number_of_stack_items));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractVerification_type_info> getLocals() {
		if (locals == null) {
			locals = new EObjectContainmentEList<AbstractVerification_type_info>(AbstractVerification_type_info.class,
					this, EcoregraphPackage.FULL_FRAME__LOCALS);
		}
		return locals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractVerification_type_info> getStack() {
		if (stack == null) {
			stack = new EObjectContainmentEList<AbstractVerification_type_info>(AbstractVerification_type_info.class,
					this, EcoregraphPackage.FULL_FRAME__STACK);
		}
		return stack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case EcoregraphPackage.FULL_FRAME__LOCALS:
			return ((InternalEList<?>) getLocals()).basicRemove(otherEnd, msgs);
		case EcoregraphPackage.FULL_FRAME__STACK:
			return ((InternalEList<?>) getStack()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.FULL_FRAME__OFFSET_DELTA:
			return getOffset_delta();
		case EcoregraphPackage.FULL_FRAME__NUMBER_OF_LOCALS:
			return getNumber_of_locals();
		case EcoregraphPackage.FULL_FRAME__NUMBER_OF_STACK_ITEMS:
			return getNumber_of_stack_items();
		case EcoregraphPackage.FULL_FRAME__LOCALS:
			return getLocals();
		case EcoregraphPackage.FULL_FRAME__STACK:
			return getStack();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.FULL_FRAME__OFFSET_DELTA:
			setOffset_delta((Integer) newValue);
			return;
		case EcoregraphPackage.FULL_FRAME__NUMBER_OF_LOCALS:
			setNumber_of_locals((Integer) newValue);
			return;
		case EcoregraphPackage.FULL_FRAME__NUMBER_OF_STACK_ITEMS:
			setNumber_of_stack_items((Integer) newValue);
			return;
		case EcoregraphPackage.FULL_FRAME__LOCALS:
			getLocals().clear();
			getLocals().addAll((Collection<? extends AbstractVerification_type_info>) newValue);
			return;
		case EcoregraphPackage.FULL_FRAME__STACK:
			getStack().clear();
			getStack().addAll((Collection<? extends AbstractVerification_type_info>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.FULL_FRAME__OFFSET_DELTA:
			setOffset_delta(OFFSET_DELTA_EDEFAULT);
			return;
		case EcoregraphPackage.FULL_FRAME__NUMBER_OF_LOCALS:
			setNumber_of_locals(NUMBER_OF_LOCALS_EDEFAULT);
			return;
		case EcoregraphPackage.FULL_FRAME__NUMBER_OF_STACK_ITEMS:
			setNumber_of_stack_items(NUMBER_OF_STACK_ITEMS_EDEFAULT);
			return;
		case EcoregraphPackage.FULL_FRAME__LOCALS:
			getLocals().clear();
			return;
		case EcoregraphPackage.FULL_FRAME__STACK:
			getStack().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.FULL_FRAME__OFFSET_DELTA:
			return offset_delta != OFFSET_DELTA_EDEFAULT;
		case EcoregraphPackage.FULL_FRAME__NUMBER_OF_LOCALS:
			return number_of_locals != NUMBER_OF_LOCALS_EDEFAULT;
		case EcoregraphPackage.FULL_FRAME__NUMBER_OF_STACK_ITEMS:
			return number_of_stack_items != NUMBER_OF_STACK_ITEMS_EDEFAULT;
		case EcoregraphPackage.FULL_FRAME__LOCALS:
			return locals != null && !locals.isEmpty();
		case EcoregraphPackage.FULL_FRAME__STACK:
			return stack != null && !stack.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (offset_delta: ");
		result.append(offset_delta);
		result.append(", number_of_locals: ");
		result.append(number_of_locals);
		result.append(", number_of_stack_items: ");
		result.append(number_of_stack_items);
		result.append(')');
		return result.toString();
	}

} //full_frameImpl
