/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.AbstractVerification_type_info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>same locals 1stack item frame</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.same_locals_1_stack_item_frameImpl#getStack <em>Stack</em>}</li>
 * </ul>
 *
 * @generated
 */
public class same_locals_1_stack_item_frameImpl extends AbstractStack_map_frameImpl
		implements same_locals_1_stack_item_frame {
	/**
	 * The cached value of the '{@link #getStack() <em>Stack</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStack()
	 * @generated
	 * @ordered
	 */
	protected AbstractVerification_type_info stack;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected same_locals_1_stack_item_frameImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.SAME_LOCALS_1STACK_ITEM_FRAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractVerification_type_info getStack() {
		return stack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStack(AbstractVerification_type_info newStack, NotificationChain msgs) {
		AbstractVerification_type_info oldStack = stack;
		stack = newStack;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME__STACK, oldStack, newStack);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStack(AbstractVerification_type_info newStack) {
		if (newStack != stack) {
			NotificationChain msgs = null;
			if (stack != null)
				msgs = ((InternalEObject) stack).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME__STACK, null, msgs);
			if (newStack != null)
				msgs = ((InternalEObject) newStack).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME__STACK, null, msgs);
			msgs = basicSetStack(newStack, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME__STACK, newStack, newStack));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME__STACK:
			return basicSetStack(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME__STACK:
			return getStack();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME__STACK:
			setStack((AbstractVerification_type_info) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME__STACK:
			setStack((AbstractVerification_type_info) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME__STACK:
			return stack != null;
		}
		return super.eIsSet(featureID);
	}

} //same_locals_1_stack_item_frameImpl
