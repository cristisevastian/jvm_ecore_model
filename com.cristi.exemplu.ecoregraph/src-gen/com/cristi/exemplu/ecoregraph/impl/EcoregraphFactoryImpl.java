/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EcoregraphFactoryImpl extends EFactoryImpl implements EcoregraphFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EcoregraphFactory init() {
		try {
			EcoregraphFactory theEcoregraphFactory = (EcoregraphFactory) EPackage.Registry.INSTANCE
					.getEFactory(EcoregraphPackage.eNS_URI);
			if (theEcoregraphFactory != null) {
				return theEcoregraphFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new EcoregraphFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EcoregraphFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case EcoregraphPackage.CONSTANT_METHOD_HANDLE_INFO:
			return createCONSTANT_MethodHandle_Info();
		case EcoregraphPackage.CONSTANT_FIELDREF_INFO:
			return createCONSTANT_Fieldref_info();
		case EcoregraphPackage.CONSTANT_METHODREF_INFO:
			return createCONSTANT_Methodref_info();
		case EcoregraphPackage.CONSTANT_INTERFACE_METHODREF_INFO:
			return createCONSTANT_InterfaceMethodref_info();
		case EcoregraphPackage.CONSTANT_CLASS_INFO:
			return createCONSTANT_Class_info();
		case EcoregraphPackage.CONSTANT_NAME_AND_TYPE_INFO:
			return createCONSTANT_NameAndType_info();
		case EcoregraphPackage.CONSTANT_UTF8_INFO:
			return createCONSTANT_Utf8_info();
		case EcoregraphPackage.CONSTANT_METHOD_TYPE_INFO:
			return createCONSTANT_MethodType_info();
		case EcoregraphPackage.CONSTANT_STRING_INFO:
			return createCONSTANT_String_info();
		case EcoregraphPackage.CONSTANT_INVOKE_DYNAMIC_INFO:
			return createCONSTANT_InvokeDynamic_info();
		case EcoregraphPackage.FIELD_INFO:
			return createField_info();
		case EcoregraphPackage.METHOD_INFO:
			return createMethod_info();
		case EcoregraphPackage.CONSTANT_INTEGER_INFO:
			return createCONSTANT_Integer_info();
		case EcoregraphPackage.CONSTANT_FLOAT_INFO:
			return createCONSTANT_Float_info();
		case EcoregraphPackage.CONSTANT_LONG_INFO:
			return createCONSTANT_Long_info();
		case EcoregraphPackage.CONSTANT_DOUBLE_INFO:
			return createCONSTANT_Double_info();
		case EcoregraphPackage.CLASS_FILE:
			return createClassFile();
		case EcoregraphPackage.CONSTANT_VALUE_ATTRIBUTE:
			return createConstantValue_attribute();
		case EcoregraphPackage.CODE_ATTRIBUTE:
			return createCode_attribute();
		case EcoregraphPackage.EXCEPTION_TABLE_ENTRY:
			return createexception_table_entry();
		case EcoregraphPackage.STACK_MAP_TABLE_ATTRIBUTE:
			return createStackMapTable_attribute();
		case EcoregraphPackage.TOP_VARIABLE_INFO:
			return createTop_variable_info();
		case EcoregraphPackage.INTEGER_VARIABLE_INFO:
			return createInteger_variable_info();
		case EcoregraphPackage.FLOAT_VARIABLE_INFO:
			return createFloat_variable_info();
		case EcoregraphPackage.NULL_VARIABLE_INFO:
			return createNull_variable_info();
		case EcoregraphPackage.UNINITIALIZED_THIS_VARIABLE_INFO:
			return createUninitializedThis_variable_info();
		case EcoregraphPackage.OBJECT_VARIABLE_INFO:
			return createObject_variable_info();
		case EcoregraphPackage.NEW_ECLASS36:
			return createNewEClass36();
		case EcoregraphPackage.UNINITIALIZED_VARIABLE_INFO:
			return createUninitialized_variable_info();
		case EcoregraphPackage.LONG_VARIABLE_INFO:
			return createLong_variable_info();
		case EcoregraphPackage.DOUBLE_VARIABLE_INFO:
			return createDouble_variable_info();
		case EcoregraphPackage.SAME_FRAME:
			return createsame_frame();
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME:
			return createsame_locals_1_stack_item_frame();
		case EcoregraphPackage.SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED:
			return createsame_locals_1_stack_item_frame_extended();
		case EcoregraphPackage.SAME_FRAME_EXTENDED:
			return createsame_frame_extended();
		case EcoregraphPackage.CHOP_FRAME:
			return createchop_frame();
		case EcoregraphPackage.APPEND_FRAME:
			return createappend_frame();
		case EcoregraphPackage.FULL_FRAME:
			return createfull_frame();
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE:
			return createExceptions_attribute();
		case EcoregraphPackage.INNER_CLASSES_ATTRIBUTE:
			return createInnerClasses_attribute();
		case EcoregraphPackage.CLASS_MEMBER:
			return createClass_member();
		case EcoregraphPackage.SYNTHETIC_ATTRIBUTE:
			return createSynthetic_attribute();
		case EcoregraphPackage.SIGNATURE_ATTRIBUTE:
			return createSignature_attribute();
		case EcoregraphPackage.NEW_ECLASS54:
			return createNewEClass54();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_MethodHandle_Info createCONSTANT_MethodHandle_Info() {
		CONSTANT_MethodHandle_InfoImpl constanT_MethodHandle_Info = new CONSTANT_MethodHandle_InfoImpl();
		return constanT_MethodHandle_Info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Fieldref_info createCONSTANT_Fieldref_info() {
		CONSTANT_Fieldref_infoImpl constanT_Fieldref_info = new CONSTANT_Fieldref_infoImpl();
		return constanT_Fieldref_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Methodref_info createCONSTANT_Methodref_info() {
		CONSTANT_Methodref_infoImpl constanT_Methodref_info = new CONSTANT_Methodref_infoImpl();
		return constanT_Methodref_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_InterfaceMethodref_info createCONSTANT_InterfaceMethodref_info() {
		CONSTANT_InterfaceMethodref_infoImpl constanT_InterfaceMethodref_info = new CONSTANT_InterfaceMethodref_infoImpl();
		return constanT_InterfaceMethodref_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Class_info createCONSTANT_Class_info() {
		CONSTANT_Class_infoImpl constanT_Class_info = new CONSTANT_Class_infoImpl();
		return constanT_Class_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_NameAndType_info createCONSTANT_NameAndType_info() {
		CONSTANT_NameAndType_infoImpl constanT_NameAndType_info = new CONSTANT_NameAndType_infoImpl();
		return constanT_NameAndType_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info createCONSTANT_Utf8_info() {
		CONSTANT_Utf8_infoImpl constanT_Utf8_info = new CONSTANT_Utf8_infoImpl();
		return constanT_Utf8_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_MethodType_info createCONSTANT_MethodType_info() {
		CONSTANT_MethodType_infoImpl constanT_MethodType_info = new CONSTANT_MethodType_infoImpl();
		return constanT_MethodType_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_String_info createCONSTANT_String_info() {
		CONSTANT_String_infoImpl constanT_String_info = new CONSTANT_String_infoImpl();
		return constanT_String_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_InvokeDynamic_info createCONSTANT_InvokeDynamic_info() {
		CONSTANT_InvokeDynamic_infoImpl constanT_InvokeDynamic_info = new CONSTANT_InvokeDynamic_infoImpl();
		return constanT_InvokeDynamic_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Field_info createField_info() {
		Field_infoImpl field_info = new Field_infoImpl();
		return field_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Method_info createMethod_info() {
		Method_infoImpl method_info = new Method_infoImpl();
		return method_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Integer_info createCONSTANT_Integer_info() {
		CONSTANT_Integer_infoImpl constanT_Integer_info = new CONSTANT_Integer_infoImpl();
		return constanT_Integer_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Float_info createCONSTANT_Float_info() {
		CONSTANT_Float_infoImpl constanT_Float_info = new CONSTANT_Float_infoImpl();
		return constanT_Float_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Long_info createCONSTANT_Long_info() {
		CONSTANT_Long_infoImpl constanT_Long_info = new CONSTANT_Long_infoImpl();
		return constanT_Long_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Double_info createCONSTANT_Double_info() {
		CONSTANT_Double_infoImpl constanT_Double_info = new CONSTANT_Double_infoImpl();
		return constanT_Double_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassFile createClassFile() {
		ClassFileImpl classFile = new ClassFileImpl();
		return classFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstantValue_attribute createConstantValue_attribute() {
		ConstantValue_attributeImpl constantValue_attribute = new ConstantValue_attributeImpl();
		return constantValue_attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Code_attribute createCode_attribute() {
		Code_attributeImpl code_attribute = new Code_attributeImpl();
		return code_attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public exception_table_entry createexception_table_entry() {
		exception_table_entryImpl exception_table_entry = new exception_table_entryImpl();
		return exception_table_entry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StackMapTable_attribute createStackMapTable_attribute() {
		StackMapTable_attributeImpl stackMapTable_attribute = new StackMapTable_attributeImpl();
		return stackMapTable_attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Top_variable_info createTop_variable_info() {
		Top_variable_infoImpl top_variable_info = new Top_variable_infoImpl();
		return top_variable_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer_variable_info createInteger_variable_info() {
		Integer_variable_infoImpl integer_variable_info = new Integer_variable_infoImpl();
		return integer_variable_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Float_variable_info createFloat_variable_info() {
		Float_variable_infoImpl float_variable_info = new Float_variable_infoImpl();
		return float_variable_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Null_variable_info createNull_variable_info() {
		Null_variable_infoImpl null_variable_info = new Null_variable_infoImpl();
		return null_variable_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UninitializedThis_variable_info createUninitializedThis_variable_info() {
		UninitializedThis_variable_infoImpl uninitializedThis_variable_info = new UninitializedThis_variable_infoImpl();
		return uninitializedThis_variable_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object_variable_info createObject_variable_info() {
		Object_variable_infoImpl object_variable_info = new Object_variable_infoImpl();
		return object_variable_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NewEClass36 createNewEClass36() {
		NewEClass36Impl newEClass36 = new NewEClass36Impl();
		return newEClass36;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Uninitialized_variable_info createUninitialized_variable_info() {
		Uninitialized_variable_infoImpl uninitialized_variable_info = new Uninitialized_variable_infoImpl();
		return uninitialized_variable_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Long_variable_info createLong_variable_info() {
		Long_variable_infoImpl long_variable_info = new Long_variable_infoImpl();
		return long_variable_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double_variable_info createDouble_variable_info() {
		Double_variable_infoImpl double_variable_info = new Double_variable_infoImpl();
		return double_variable_info;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public same_frame createsame_frame() {
		same_frameImpl same_frame = new same_frameImpl();
		return same_frame;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public same_locals_1_stack_item_frame createsame_locals_1_stack_item_frame() {
		same_locals_1_stack_item_frameImpl same_locals_1_stack_item_frame = new same_locals_1_stack_item_frameImpl();
		return same_locals_1_stack_item_frame;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public same_locals_1_stack_item_frame_extended createsame_locals_1_stack_item_frame_extended() {
		same_locals_1_stack_item_frame_extendedImpl same_locals_1_stack_item_frame_extended = new same_locals_1_stack_item_frame_extendedImpl();
		return same_locals_1_stack_item_frame_extended;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public same_frame_extended createsame_frame_extended() {
		same_frame_extendedImpl same_frame_extended = new same_frame_extendedImpl();
		return same_frame_extended;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public chop_frame createchop_frame() {
		chop_frameImpl chop_frame = new chop_frameImpl();
		return chop_frame;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public append_frame createappend_frame() {
		append_frameImpl append_frame = new append_frameImpl();
		return append_frame;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public full_frame createfull_frame() {
		full_frameImpl full_frame = new full_frameImpl();
		return full_frame;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Exceptions_attribute createExceptions_attribute() {
		Exceptions_attributeImpl exceptions_attribute = new Exceptions_attributeImpl();
		return exceptions_attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InnerClasses_attribute createInnerClasses_attribute() {
		InnerClasses_attributeImpl innerClasses_attribute = new InnerClasses_attributeImpl();
		return innerClasses_attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Class_member createClass_member() {
		Class_memberImpl class_member = new Class_memberImpl();
		return class_member;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Synthetic_attribute createSynthetic_attribute() {
		Synthetic_attributeImpl synthetic_attribute = new Synthetic_attributeImpl();
		return synthetic_attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Signature_attribute createSignature_attribute() {
		Signature_attributeImpl signature_attribute = new Signature_attributeImpl();
		return signature_attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NewEClass54 createNewEClass54() {
		NewEClass54Impl newEClass54 = new NewEClass54Impl();
		return newEClass54;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EcoregraphPackage getEcoregraphPackage() {
		return (EcoregraphPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static EcoregraphPackage getPackage() {
		return EcoregraphPackage.eINSTANCE;
	}

} //EcoregraphFactoryImpl
