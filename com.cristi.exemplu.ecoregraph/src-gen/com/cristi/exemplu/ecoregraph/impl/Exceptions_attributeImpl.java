/**
 */
package com.cristi.exemplu.ecoregraph.impl;

import com.cristi.exemplu.ecoregraph.AbstractAttribute_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Class_info;
import com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info;
import com.cristi.exemplu.ecoregraph.EcoregraphPackage;
import com.cristi.exemplu.ecoregraph.Exceptions_attribute;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exceptions attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Exceptions_attributeImpl#getAttribute_length <em>Attribute length</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Exceptions_attributeImpl#getAttribute_name <em>Attribute name</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Exceptions_attributeImpl#getException_table <em>Exception table</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.impl.Exceptions_attributeImpl#getNumber_of_exceptions <em>Number of exceptions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class Exceptions_attributeImpl extends MinimalEObjectImpl.Container implements Exceptions_attribute {
	/**
	 * The default value of the '{@link #getAttribute_length() <em>Attribute length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttribute_length()
	 * @generated
	 * @ordered
	 */
	protected static final long ATTRIBUTE_LENGTH_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getAttribute_length() <em>Attribute length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttribute_length()
	 * @generated
	 * @ordered
	 */
	protected long attribute_length = ATTRIBUTE_LENGTH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAttribute_name() <em>Attribute name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttribute_name()
	 * @generated
	 * @ordered
	 */
	protected CONSTANT_Utf8_info attribute_name;

	/**
	 * The cached value of the '{@link #getException_table() <em>Exception table</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getException_table()
	 * @generated
	 * @ordered
	 */
	protected EList<CONSTANT_Class_info> exception_table;

	/**
	 * The default value of the '{@link #getNumber_of_exceptions() <em>Number of exceptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber_of_exceptions()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_EXCEPTIONS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumber_of_exceptions() <em>Number of exceptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber_of_exceptions()
	 * @generated
	 * @ordered
	 */
	protected int number_of_exceptions = NUMBER_OF_EXCEPTIONS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Exceptions_attributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EcoregraphPackage.Literals.EXCEPTIONS_ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getAttribute_length() {
		return attribute_length;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttribute_length(long newAttribute_length) {
		long oldAttribute_length = attribute_length;
		attribute_length = newAttribute_length;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_LENGTH, oldAttribute_length, attribute_length));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info getAttribute_name() {
		if (attribute_name != null && attribute_name.eIsProxy()) {
			InternalEObject oldAttribute_name = (InternalEObject) attribute_name;
			attribute_name = (CONSTANT_Utf8_info) eResolveProxy(oldAttribute_name);
			if (attribute_name != oldAttribute_name) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_NAME, oldAttribute_name, attribute_name));
			}
		}
		return attribute_name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CONSTANT_Utf8_info basicGetAttribute_name() {
		return attribute_name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttribute_name(CONSTANT_Utf8_info newAttribute_name) {
		CONSTANT_Utf8_info oldAttribute_name = attribute_name;
		attribute_name = newAttribute_name;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_NAME, oldAttribute_name, attribute_name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CONSTANT_Class_info> getException_table() {
		if (exception_table == null) {
			exception_table = new EObjectResolvingEList<CONSTANT_Class_info>(CONSTANT_Class_info.class, this,
					EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__EXCEPTION_TABLE);
		}
		return exception_table;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumber_of_exceptions() {
		return number_of_exceptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumber_of_exceptions(int newNumber_of_exceptions) {
		int oldNumber_of_exceptions = number_of_exceptions;
		number_of_exceptions = newNumber_of_exceptions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__NUMBER_OF_EXCEPTIONS, oldNumber_of_exceptions,
					number_of_exceptions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_LENGTH:
			return getAttribute_length();
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_NAME:
			if (resolve)
				return getAttribute_name();
			return basicGetAttribute_name();
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__EXCEPTION_TABLE:
			return getException_table();
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__NUMBER_OF_EXCEPTIONS:
			return getNumber_of_exceptions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_LENGTH:
			setAttribute_length((Long) newValue);
			return;
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_NAME:
			setAttribute_name((CONSTANT_Utf8_info) newValue);
			return;
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__EXCEPTION_TABLE:
			getException_table().clear();
			getException_table().addAll((Collection<? extends CONSTANT_Class_info>) newValue);
			return;
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__NUMBER_OF_EXCEPTIONS:
			setNumber_of_exceptions((Integer) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_LENGTH:
			setAttribute_length(ATTRIBUTE_LENGTH_EDEFAULT);
			return;
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_NAME:
			setAttribute_name((CONSTANT_Utf8_info) null);
			return;
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__EXCEPTION_TABLE:
			getException_table().clear();
			return;
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__NUMBER_OF_EXCEPTIONS:
			setNumber_of_exceptions(NUMBER_OF_EXCEPTIONS_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_LENGTH:
			return attribute_length != ATTRIBUTE_LENGTH_EDEFAULT;
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_NAME:
			return attribute_name != null;
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__EXCEPTION_TABLE:
			return exception_table != null && !exception_table.isEmpty();
		case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__NUMBER_OF_EXCEPTIONS:
			return number_of_exceptions != NUMBER_OF_EXCEPTIONS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AbstractAttribute_info.class) {
			switch (derivedFeatureID) {
			case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_LENGTH:
				return EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_LENGTH;
			case EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_NAME:
				return EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_NAME;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AbstractAttribute_info.class) {
			switch (baseFeatureID) {
			case EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_LENGTH:
				return EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_LENGTH;
			case EcoregraphPackage.ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_NAME:
				return EcoregraphPackage.EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_NAME;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (attribute_length: ");
		result.append(attribute_length);
		result.append(", number_of_exceptions: ");
		result.append(number_of_exceptions);
		result.append(')');
		return result.toString();
	}

} //Exceptions_attributeImpl
