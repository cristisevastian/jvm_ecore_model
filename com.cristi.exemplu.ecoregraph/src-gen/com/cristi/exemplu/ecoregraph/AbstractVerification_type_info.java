/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Verification type info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.AbstractVerification_type_info#getTag <em>Tag</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getAbstractVerification_type_info()
 * @model abstract="true"
 * @generated
 */
public interface AbstractVerification_type_info extends EObject {
	/**
	 * Returns the value of the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag</em>' attribute.
	 * @see #setTag(short)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getAbstractVerification_type_info_Tag()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedByte"
	 * @generated
	 */
	short getTag();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.AbstractVerification_type_info#getTag <em>Tag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tag</em>' attribute.
	 * @see #getTag()
	 * @generated
	 */
	void setTag(short value);

} // AbstractVerification_type_info
