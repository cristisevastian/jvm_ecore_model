/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CONSTANT Long info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.CONSTANT_Long_info#getHigh_bytes <em>High bytes</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.CONSTANT_Long_info#getLow_bytes <em>Low bytes</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_Long_info()
 * @model
 * @generated
 */
public interface CONSTANT_Long_info extends AbstractCp_info, InterfaceCONSTANT_Value {
	/**
	 * Returns the value of the '<em><b>High bytes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>High bytes</em>' attribute.
	 * @see #setHigh_bytes(long)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_Long_info_High_bytes()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 * @generated
	 */
	long getHigh_bytes();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Long_info#getHigh_bytes <em>High bytes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>High bytes</em>' attribute.
	 * @see #getHigh_bytes()
	 * @generated
	 */
	void setHigh_bytes(long value);

	/**
	 * Returns the value of the '<em><b>Low bytes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Low bytes</em>' attribute.
	 * @see #setLow_bytes(long)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_Long_info_Low_bytes()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 * @generated
	 */
	long getLow_bytes();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Long_info#getLow_bytes <em>Low bytes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Low bytes</em>' attribute.
	 * @see #getLow_bytes()
	 * @generated
	 */
	void setLow_bytes(long value);

} // CONSTANT_Long_info
