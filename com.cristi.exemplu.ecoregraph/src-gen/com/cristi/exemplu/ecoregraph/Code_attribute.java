/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Code attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Code_attribute#getMax_stack <em>Max stack</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Code_attribute#getMax_locals <em>Max locals</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Code_attribute#getCode_length <em>Code length</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Code_attribute#getCode <em>Code</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Code_attribute#getException_table <em>Exception table</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Code_attribute#getException_table_length <em>Exception table length</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Code_attribute#getAttributes_count <em>Attributes count</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Code_attribute#getInterfacecode_attribute <em>Interfacecode attribute</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCode_attribute()
 * @model
 * @generated
 */
public interface Code_attribute extends AbstractAttribute_info, InterfaceMethod_attribute_info {
	/**
	 * Returns the value of the '<em><b>Max stack</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max stack</em>' attribute.
	 * @see #setMax_stack(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCode_attribute_Max_stack()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getMax_stack();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Code_attribute#getMax_stack <em>Max stack</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max stack</em>' attribute.
	 * @see #getMax_stack()
	 * @generated
	 */
	void setMax_stack(int value);

	/**
	 * Returns the value of the '<em><b>Max locals</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max locals</em>' attribute.
	 * @see #setMax_locals(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCode_attribute_Max_locals()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getMax_locals();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Code_attribute#getMax_locals <em>Max locals</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max locals</em>' attribute.
	 * @see #getMax_locals()
	 * @generated
	 */
	void setMax_locals(int value);

	/**
	 * Returns the value of the '<em><b>Code length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code length</em>' attribute.
	 * @see #setCode_length(long)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCode_attribute_Code_length()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 * @generated
	 */
	long getCode_length();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Code_attribute#getCode_length <em>Code length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code length</em>' attribute.
	 * @see #getCode_length()
	 * @generated
	 */
	void setCode_length(long value);

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(byte[])
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCode_attribute_Code()
	 * @model
	 * @generated
	 */
	byte[] getCode();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Code_attribute#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(byte[] value);

	/**
	 * Returns the value of the '<em><b>Exception table</b></em>' containment reference list.
	 * The list contents are of type {@link com.cristi.exemplu.ecoregraph.exception_table_entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception table</em>' containment reference list.
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCode_attribute_Exception_table()
	 * @model containment="true"
	 * @generated
	 */
	EList<exception_table_entry> getException_table();

	/**
	 * Returns the value of the '<em><b>Exception table length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception table length</em>' attribute.
	 * @see #setException_table_length(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCode_attribute_Exception_table_length()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getException_table_length();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Code_attribute#getException_table_length <em>Exception table length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exception table length</em>' attribute.
	 * @see #getException_table_length()
	 * @generated
	 */
	void setException_table_length(int value);

	/**
	 * Returns the value of the '<em><b>Attributes count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes count</em>' attribute.
	 * @see #setAttributes_count(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCode_attribute_Attributes_count()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getAttributes_count();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Code_attribute#getAttributes_count <em>Attributes count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attributes count</em>' attribute.
	 * @see #getAttributes_count()
	 * @generated
	 */
	void setAttributes_count(int value);

	/**
	 * Returns the value of the '<em><b>Interfacecode attribute</b></em>' containment reference list.
	 * The list contents are of type {@link com.cristi.exemplu.ecoregraph.InterfaceCode_attribute}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interfacecode attribute</em>' containment reference list.
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCode_attribute_Interfacecode_attribute()
	 * @model containment="true"
	 * @generated
	 */
	EList<InterfaceCode_attribute> getInterfacecode_attribute();

} // Code_attribute
