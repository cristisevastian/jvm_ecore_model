/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>same locals 1stack item frame</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame#getStack <em>Stack</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getsame_locals_1_stack_item_frame()
 * @model
 * @generated
 */
public interface same_locals_1_stack_item_frame extends AbstractStack_map_frame {
	/**
	 * Returns the value of the '<em><b>Stack</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stack</em>' containment reference.
	 * @see #setStack(AbstractVerification_type_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getsame_locals_1_stack_item_frame_Stack()
	 * @model containment="true" required="true"
	 * @generated
	 */
	AbstractVerification_type_info getStack();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame#getStack <em>Stack</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stack</em>' containment reference.
	 * @see #getStack()
	 * @generated
	 */
	void setStack(AbstractVerification_type_info value);

} // same_locals_1_stack_item_frame
