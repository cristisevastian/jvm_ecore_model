/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Method info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Method_info#getAccess_flags <em>Access flags</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Method_info#getAttributes_count <em>Attributes count</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Method_info#getMethod_descriptor <em>Method descriptor</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Method_info#getName <em>Name</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Method_info#getInterfacemethod_attribute_info <em>Interfacemethod attribute info</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getMethod_info()
 * @model
 * @generated
 */
public interface Method_info extends EObject {
	/**
	 * Returns the value of the '<em><b>Access flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access flags</em>' attribute.
	 * @see #setAccess_flags(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getMethod_info_Access_flags()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getAccess_flags();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Method_info#getAccess_flags <em>Access flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access flags</em>' attribute.
	 * @see #getAccess_flags()
	 * @generated
	 */
	void setAccess_flags(int value);

	/**
	 * Returns the value of the '<em><b>Attributes count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes count</em>' attribute.
	 * @see #setAttributes_count(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getMethod_info_Attributes_count()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getAttributes_count();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Method_info#getAttributes_count <em>Attributes count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attributes count</em>' attribute.
	 * @see #getAttributes_count()
	 * @generated
	 */
	void setAttributes_count(int value);

	/**
	 * Returns the value of the '<em><b>Method descriptor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method descriptor</em>' reference.
	 * @see #setMethod_descriptor(CONSTANT_Utf8_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getMethod_info_Method_descriptor()
	 * @model required="true"
	 * @generated
	 */
	CONSTANT_Utf8_info getMethod_descriptor();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Method_info#getMethod_descriptor <em>Method descriptor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method descriptor</em>' reference.
	 * @see #getMethod_descriptor()
	 * @generated
	 */
	void setMethod_descriptor(CONSTANT_Utf8_info value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' reference.
	 * @see #setName(CONSTANT_Utf8_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getMethod_info_Name()
	 * @model required="true"
	 * @generated
	 */
	CONSTANT_Utf8_info getName();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Method_info#getName <em>Name</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' reference.
	 * @see #getName()
	 * @generated
	 */
	void setName(CONSTANT_Utf8_info value);

	/**
	 * Returns the value of the '<em><b>Interfacemethod attribute info</b></em>' containment reference list.
	 * The list contents are of type {@link com.cristi.exemplu.ecoregraph.InterfaceMethod_attribute_info}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interfacemethod attribute info</em>' containment reference list.
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getMethod_info_Interfacemethod_attribute_info()
	 * @model containment="true"
	 * @generated
	 */
	EList<InterfaceMethod_attribute_info> getInterfacemethod_attribute_info();

} // Method_info
