/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Double variable info</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getDouble_variable_info()
 * @model
 * @generated
 */
public interface Double_variable_info extends AbstractVerification_type_info {
} // Double_variable_info
