/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Stack map frame</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.AbstractStack_map_frame#getFrame_type <em>Frame type</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getAbstractStack_map_frame()
 * @model abstract="true"
 * @generated
 */
public interface AbstractStack_map_frame extends EObject {
	/**
	 * Returns the value of the '<em><b>Frame type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Frame type</em>' attribute.
	 * @see #setFrame_type(short)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getAbstractStack_map_frame_Frame_type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedByte"
	 * @generated
	 */
	short getFrame_type();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.AbstractStack_map_frame#getFrame_type <em>Frame type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Frame type</em>' attribute.
	 * @see #getFrame_type()
	 * @generated
	 */
	void setFrame_type(short value);

} // AbstractStack_map_frame
