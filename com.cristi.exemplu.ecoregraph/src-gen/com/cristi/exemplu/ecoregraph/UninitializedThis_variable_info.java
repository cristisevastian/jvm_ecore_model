/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uninitialized This variable info</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getUninitializedThis_variable_info()
 * @model
 * @generated
 */
public interface UninitializedThis_variable_info extends AbstractVerification_type_info {
} // UninitializedThis_variable_info
