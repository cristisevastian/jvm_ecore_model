/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>append frame</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.append_frame#getOffset_delta <em>Offset delta</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.append_frame#getLocals <em>Locals</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getappend_frame()
 * @model
 * @generated
 */
public interface append_frame extends AbstractStack_map_frame {
	/**
	 * Returns the value of the '<em><b>Offset delta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset delta</em>' attribute.
	 * @see #setOffset_delta(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getappend_frame_Offset_delta()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getOffset_delta();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.append_frame#getOffset_delta <em>Offset delta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Offset delta</em>' attribute.
	 * @see #getOffset_delta()
	 * @generated
	 */
	void setOffset_delta(int value);

	/**
	 * Returns the value of the '<em><b>Locals</b></em>' containment reference list.
	 * The list contents are of type {@link com.cristi.exemplu.ecoregraph.AbstractVerification_type_info}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locals</em>' containment reference list.
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getappend_frame_Locals()
	 * @model containment="true" required="true" upper="3"
	 * @generated
	 */
	EList<AbstractVerification_type_info> getLocals();

} // append_frame
