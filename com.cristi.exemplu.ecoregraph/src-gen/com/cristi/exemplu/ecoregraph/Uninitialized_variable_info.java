/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uninitialized variable info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Uninitialized_variable_info#getOffset <em>Offset</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getUninitialized_variable_info()
 * @model
 * @generated
 */
public interface Uninitialized_variable_info extends AbstractVerification_type_info {
	/**
	 * Returns the value of the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset</em>' attribute.
	 * @see #setOffset(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getUninitialized_variable_info_Offset()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getOffset();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Uninitialized_variable_info#getOffset <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Offset</em>' attribute.
	 * @see #getOffset()
	 * @generated
	 */
	void setOffset(int value);

} // Uninitialized_variable_info
