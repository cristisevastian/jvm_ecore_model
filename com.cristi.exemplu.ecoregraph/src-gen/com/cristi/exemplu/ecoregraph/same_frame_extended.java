/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>same frame extended</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.same_frame_extended#getOffset_delta <em>Offset delta</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getsame_frame_extended()
 * @model
 * @generated
 */
public interface same_frame_extended extends AbstractStack_map_frame {
	/**
	 * Returns the value of the '<em><b>Offset delta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset delta</em>' attribute.
	 * @see #setOffset_delta(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getsame_frame_extended_Offset_delta()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getOffset_delta();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.same_frame_extended#getOffset_delta <em>Offset delta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Offset delta</em>' attribute.
	 * @see #getOffset_delta()
	 * @generated
	 */
	void setOffset_delta(int value);

} // same_frame_extended
