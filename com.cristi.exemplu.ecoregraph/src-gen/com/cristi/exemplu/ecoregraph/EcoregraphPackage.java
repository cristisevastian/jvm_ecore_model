/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.cristi.exemplu.ecoregraph.EcoregraphFactory
 * @model kind="package"
 * @generated
 */
public interface EcoregraphPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ecoregraph";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/ecoregraph";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ecoregraph";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EcoregraphPackage eINSTANCE = com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.AbstractCp_infoImpl <em>Abstract Cp info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.AbstractCp_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getAbstractCp_info()
	 * @generated
	 */
	int ABSTRACT_CP_INFO = 12;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CP_INFO__TAG = 0;

	/**
	 * The number of structural features of the '<em>Abstract Cp info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CP_INFO_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Abstract Cp info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CP_INFO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_MethodHandle_InfoImpl <em>CONSTANT Method Handle Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_MethodHandle_InfoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_MethodHandle_Info()
	 * @generated
	 */
	int CONSTANT_METHOD_HANDLE_INFO = 0;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_METHOD_HANDLE_INFO__TAG = ABSTRACT_CP_INFO__TAG;

	/**
	 * The feature id for the '<em><b>Reference kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_METHOD_HANDLE_INFO__REFERENCE_KIND = ABSTRACT_CP_INFO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constant ref info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_METHOD_HANDLE_INFO__CONSTANT_REF_INFO = ABSTRACT_CP_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>CONSTANT Method Handle Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_METHOD_HANDLE_INFO_FEATURE_COUNT = ABSTRACT_CP_INFO_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>CONSTANT Method Handle Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_METHOD_HANDLE_INFO_OPERATION_COUNT = ABSTRACT_CP_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Ref_info <em>Interface CONSTANT Ref info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Ref_info
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInterfaceCONSTANT_Ref_info()
	 * @generated
	 */
	int INTERFACE_CONSTANT_REF_INFO = 13;

	/**
	 * The number of structural features of the '<em>Interface CONSTANT Ref info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_CONSTANT_REF_INFO_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Interface CONSTANT Ref info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_CONSTANT_REF_INFO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Fieldref_infoImpl <em>CONSTANT Fieldref info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Fieldref_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Fieldref_info()
	 * @generated
	 */
	int CONSTANT_FIELDREF_INFO = 1;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_FIELDREF_INFO__TAG = INTERFACE_CONSTANT_REF_INFO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constant class info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_FIELDREF_INFO__CONSTANT_CLASS_INFO = INTERFACE_CONSTANT_REF_INFO_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Constant nameandtype info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_FIELDREF_INFO__CONSTANT_NAMEANDTYPE_INFO = INTERFACE_CONSTANT_REF_INFO_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>CONSTANT Fieldref info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_FIELDREF_INFO_FEATURE_COUNT = INTERFACE_CONSTANT_REF_INFO_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>CONSTANT Fieldref info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_FIELDREF_INFO_OPERATION_COUNT = INTERFACE_CONSTANT_REF_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Methodref_infoImpl <em>CONSTANT Methodref info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Methodref_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Methodref_info()
	 * @generated
	 */
	int CONSTANT_METHODREF_INFO = 2;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_METHODREF_INFO__TAG = INTERFACE_CONSTANT_REF_INFO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constant class info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_METHODREF_INFO__CONSTANT_CLASS_INFO = INTERFACE_CONSTANT_REF_INFO_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Constant nameandtype info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_METHODREF_INFO__CONSTANT_NAMEANDTYPE_INFO = INTERFACE_CONSTANT_REF_INFO_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>CONSTANT Methodref info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_METHODREF_INFO_FEATURE_COUNT = INTERFACE_CONSTANT_REF_INFO_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>CONSTANT Methodref info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_METHODREF_INFO_OPERATION_COUNT = INTERFACE_CONSTANT_REF_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_InterfaceMethodref_infoImpl <em>CONSTANT Interface Methodref info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_InterfaceMethodref_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_InterfaceMethodref_info()
	 * @generated
	 */
	int CONSTANT_INTERFACE_METHODREF_INFO = 3;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INTERFACE_METHODREF_INFO__TAG = INTERFACE_CONSTANT_REF_INFO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constant class info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INTERFACE_METHODREF_INFO__CONSTANT_CLASS_INFO = INTERFACE_CONSTANT_REF_INFO_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Constant nameandtype info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INTERFACE_METHODREF_INFO__CONSTANT_NAMEANDTYPE_INFO = INTERFACE_CONSTANT_REF_INFO_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>CONSTANT Interface Methodref info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INTERFACE_METHODREF_INFO_FEATURE_COUNT = INTERFACE_CONSTANT_REF_INFO_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>CONSTANT Interface Methodref info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INTERFACE_METHODREF_INFO_OPERATION_COUNT = INTERFACE_CONSTANT_REF_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Class_infoImpl <em>CONSTANT Class info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Class_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Class_info()
	 * @generated
	 */
	int CONSTANT_CLASS_INFO = 4;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_CLASS_INFO__TAG = ABSTRACT_CP_INFO__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_CLASS_INFO__NAME = ABSTRACT_CP_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>CONSTANT Class info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_CLASS_INFO_FEATURE_COUNT = ABSTRACT_CP_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>CONSTANT Class info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_CLASS_INFO_OPERATION_COUNT = ABSTRACT_CP_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_NameAndType_infoImpl <em>CONSTANT Name And Type info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_NameAndType_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_NameAndType_info()
	 * @generated
	 */
	int CONSTANT_NAME_AND_TYPE_INFO = 5;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_NAME_AND_TYPE_INFO__TAG = ABSTRACT_CP_INFO__TAG;

	/**
	 * The feature id for the '<em><b>Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_NAME_AND_TYPE_INFO__NAME = ABSTRACT_CP_INFO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Field method descriptor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_NAME_AND_TYPE_INFO__FIELD_METHOD_DESCRIPTOR = ABSTRACT_CP_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>CONSTANT Name And Type info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_NAME_AND_TYPE_INFO_FEATURE_COUNT = ABSTRACT_CP_INFO_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>CONSTANT Name And Type info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_NAME_AND_TYPE_INFO_OPERATION_COUNT = ABSTRACT_CP_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Utf8_infoImpl <em>CONSTANT Utf8 info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Utf8_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Utf8_info()
	 * @generated
	 */
	int CONSTANT_UTF8_INFO = 6;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_UTF8_INFO__TAG = ABSTRACT_CP_INFO__TAG;

	/**
	 * The feature id for the '<em><b>Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_UTF8_INFO__LENGTH = ABSTRACT_CP_INFO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bytes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_UTF8_INFO__BYTES = ABSTRACT_CP_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>CONSTANT Utf8 info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_UTF8_INFO_FEATURE_COUNT = ABSTRACT_CP_INFO_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>CONSTANT Utf8 info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_UTF8_INFO_OPERATION_COUNT = ABSTRACT_CP_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_MethodType_infoImpl <em>CONSTANT Method Type info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_MethodType_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_MethodType_info()
	 * @generated
	 */
	int CONSTANT_METHOD_TYPE_INFO = 7;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_METHOD_TYPE_INFO__TAG = ABSTRACT_CP_INFO__TAG;

	/**
	 * The feature id for the '<em><b>Method descriptor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_METHOD_TYPE_INFO__METHOD_DESCRIPTOR = ABSTRACT_CP_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>CONSTANT Method Type info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_METHOD_TYPE_INFO_FEATURE_COUNT = ABSTRACT_CP_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>CONSTANT Method Type info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_METHOD_TYPE_INFO_OPERATION_COUNT = ABSTRACT_CP_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_String_infoImpl <em>CONSTANT String info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_String_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_String_info()
	 * @generated
	 */
	int CONSTANT_STRING_INFO = 8;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_STRING_INFO__TAG = ABSTRACT_CP_INFO__TAG;

	/**
	 * The feature id for the '<em><b>Constant utf8 info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_STRING_INFO__CONSTANT_UTF8_INFO = ABSTRACT_CP_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>CONSTANT String info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_STRING_INFO_FEATURE_COUNT = ABSTRACT_CP_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>CONSTANT String info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_STRING_INFO_OPERATION_COUNT = ABSTRACT_CP_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_InvokeDynamic_infoImpl <em>CONSTANT Invoke Dynamic info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_InvokeDynamic_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_InvokeDynamic_info()
	 * @generated
	 */
	int CONSTANT_INVOKE_DYNAMIC_INFO = 9;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INVOKE_DYNAMIC_INFO__TAG = ABSTRACT_CP_INFO__TAG;

	/**
	 * The feature id for the '<em><b>Constant nameandtype info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INVOKE_DYNAMIC_INFO__CONSTANT_NAMEANDTYPE_INFO = ABSTRACT_CP_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>CONSTANT Invoke Dynamic info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INVOKE_DYNAMIC_INFO_FEATURE_COUNT = ABSTRACT_CP_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>CONSTANT Invoke Dynamic info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INVOKE_DYNAMIC_INFO_OPERATION_COUNT = ABSTRACT_CP_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.Field_infoImpl <em>Field info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.Field_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getField_info()
	 * @generated
	 */
	int FIELD_INFO = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INFO__NAME = 0;

	/**
	 * The feature id for the '<em><b>Field descriptor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INFO__FIELD_DESCRIPTOR = 1;

	/**
	 * The feature id for the '<em><b>Access flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INFO__ACCESS_FLAGS = 2;

	/**
	 * The feature id for the '<em><b>Attributes count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INFO__ATTRIBUTES_COUNT = 3;

	/**
	 * The feature id for the '<em><b>Interfacefield attribute info</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INFO__INTERFACEFIELD_ATTRIBUTE_INFO = 4;

	/**
	 * The number of structural features of the '<em>Field info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INFO_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Field info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INFO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.Method_infoImpl <em>Method info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.Method_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getMethod_info()
	 * @generated
	 */
	int METHOD_INFO = 11;

	/**
	 * The feature id for the '<em><b>Access flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INFO__ACCESS_FLAGS = 0;

	/**
	 * The feature id for the '<em><b>Attributes count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INFO__ATTRIBUTES_COUNT = 1;

	/**
	 * The feature id for the '<em><b>Method descriptor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INFO__METHOD_DESCRIPTOR = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INFO__NAME = 3;

	/**
	 * The feature id for the '<em><b>Interfacemethod attribute info</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INFO__INTERFACEMETHOD_ATTRIBUTE_INFO = 4;

	/**
	 * The number of structural features of the '<em>Method info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INFO_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Method info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INFO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Integer_infoImpl <em>CONSTANT Integer info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Integer_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Integer_info()
	 * @generated
	 */
	int CONSTANT_INTEGER_INFO = 14;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INTEGER_INFO__TAG = ABSTRACT_CP_INFO__TAG;

	/**
	 * The feature id for the '<em><b>Bytes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INTEGER_INFO__BYTES = ABSTRACT_CP_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>CONSTANT Integer info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INTEGER_INFO_FEATURE_COUNT = ABSTRACT_CP_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>CONSTANT Integer info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INTEGER_INFO_OPERATION_COUNT = ABSTRACT_CP_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Float_infoImpl <em>CONSTANT Float info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Float_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Float_info()
	 * @generated
	 */
	int CONSTANT_FLOAT_INFO = 15;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_FLOAT_INFO__TAG = ABSTRACT_CP_INFO__TAG;

	/**
	 * The feature id for the '<em><b>Bytes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_FLOAT_INFO__BYTES = ABSTRACT_CP_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>CONSTANT Float info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_FLOAT_INFO_FEATURE_COUNT = ABSTRACT_CP_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>CONSTANT Float info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_FLOAT_INFO_OPERATION_COUNT = ABSTRACT_CP_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Long_infoImpl <em>CONSTANT Long info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Long_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Long_info()
	 * @generated
	 */
	int CONSTANT_LONG_INFO = 16;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LONG_INFO__TAG = ABSTRACT_CP_INFO__TAG;

	/**
	 * The feature id for the '<em><b>High bytes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LONG_INFO__HIGH_BYTES = ABSTRACT_CP_INFO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Low bytes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LONG_INFO__LOW_BYTES = ABSTRACT_CP_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>CONSTANT Long info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LONG_INFO_FEATURE_COUNT = ABSTRACT_CP_INFO_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>CONSTANT Long info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_LONG_INFO_OPERATION_COUNT = ABSTRACT_CP_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Double_infoImpl <em>CONSTANT Double info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Double_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Double_info()
	 * @generated
	 */
	int CONSTANT_DOUBLE_INFO = 17;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_DOUBLE_INFO__TAG = ABSTRACT_CP_INFO__TAG;

	/**
	 * The feature id for the '<em><b>High bytes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_DOUBLE_INFO__HIGH_BYTES = ABSTRACT_CP_INFO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Low bytes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_DOUBLE_INFO__LOW_BYTES = ABSTRACT_CP_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>CONSTANT Double info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_DOUBLE_INFO_FEATURE_COUNT = ABSTRACT_CP_INFO_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>CONSTANT Double info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_DOUBLE_INFO_OPERATION_COUNT = ABSTRACT_CP_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl <em>Class File</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.ClassFileImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getClassFile()
	 * @generated
	 */
	int CLASS_FILE = 18;

	/**
	 * The feature id for the '<em><b>Magic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__MAGIC = 0;

	/**
	 * The feature id for the '<em><b>Minor version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__MINOR_VERSION = 1;

	/**
	 * The feature id for the '<em><b>Major version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__MAJOR_VERSION = 2;

	/**
	 * The feature id for the '<em><b>Constant pool count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__CONSTANT_POOL_COUNT = 3;

	/**
	 * The feature id for the '<em><b>Access flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__ACCESS_FLAGS = 4;

	/**
	 * The feature id for the '<em><b>Interfaces count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__INTERFACES_COUNT = 5;

	/**
	 * The feature id for the '<em><b>Fields count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__FIELDS_COUNT = 6;

	/**
	 * The feature id for the '<em><b>Methods count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__METHODS_COUNT = 7;

	/**
	 * The feature id for the '<em><b>Attributes count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__ATTRIBUTES_COUNT = 8;

	/**
	 * The feature id for the '<em><b>Cp info</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__CP_INFO = 9;

	/**
	 * The feature id for the '<em><b>Field info</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__FIELD_INFO = 10;

	/**
	 * The feature id for the '<em><b>Method info</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__METHOD_INFO = 11;

	/**
	 * The feature id for the '<em><b>Super class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__SUPER_CLASS = 12;

	/**
	 * The feature id for the '<em><b>Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__INTERFACES = 13;

	/**
	 * The feature id for the '<em><b>This class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__THIS_CLASS = 14;

	/**
	 * The feature id for the '<em><b>Interfaceclass attribute info</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE__INTERFACECLASS_ATTRIBUTE_INFO = 15;

	/**
	 * The number of structural features of the '<em>Class File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE_FEATURE_COUNT = 16;

	/**
	 * The number of operations of the '<em>Class File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.AbstractAttribute_infoImpl <em>Abstract Attribute info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.AbstractAttribute_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getAbstractAttribute_info()
	 * @generated
	 */
	int ABSTRACT_ATTRIBUTE_INFO = 19;

	/**
	 * The feature id for the '<em><b>Attribute length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_LENGTH = 0;

	/**
	 * The feature id for the '<em><b>Attribute name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_NAME = 1;

	/**
	 * The number of structural features of the '<em>Abstract Attribute info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Abstract Attribute info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ATTRIBUTE_INFO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.InterfaceMethod_attribute_info <em>Interface Method attribute info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.InterfaceMethod_attribute_info
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInterfaceMethod_attribute_info()
	 * @generated
	 */
	int INTERFACE_METHOD_ATTRIBUTE_INFO = 20;

	/**
	 * The number of structural features of the '<em>Interface Method attribute info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_METHOD_ATTRIBUTE_INFO_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Interface Method attribute info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_METHOD_ATTRIBUTE_INFO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.InterfaceField_attribute_info <em>Interface Field attribute info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.InterfaceField_attribute_info
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInterfaceField_attribute_info()
	 * @generated
	 */
	int INTERFACE_FIELD_ATTRIBUTE_INFO = 21;

	/**
	 * The number of structural features of the '<em>Interface Field attribute info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_FIELD_ATTRIBUTE_INFO_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Interface Field attribute info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_FIELD_ATTRIBUTE_INFO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.InterfaceClass_attribute_info <em>Interface Class attribute info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.InterfaceClass_attribute_info
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInterfaceClass_attribute_info()
	 * @generated
	 */
	int INTERFACE_CLASS_ATTRIBUTE_INFO = 22;

	/**
	 * The number of structural features of the '<em>Interface Class attribute info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_CLASS_ATTRIBUTE_INFO_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Interface Class attribute info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_CLASS_ATTRIBUTE_INFO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.ConstantValue_attributeImpl <em>Constant Value attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.ConstantValue_attributeImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getConstantValue_attribute()
	 * @generated
	 */
	int CONSTANT_VALUE_ATTRIBUTE = 23;

	/**
	 * The feature id for the '<em><b>Attribute length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_VALUE_ATTRIBUTE__ATTRIBUTE_LENGTH = ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_LENGTH;

	/**
	 * The feature id for the '<em><b>Attribute name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_VALUE_ATTRIBUTE__ATTRIBUTE_NAME = ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_NAME;

	/**
	 * The feature id for the '<em><b>Interfaceconstant value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_VALUE_ATTRIBUTE__INTERFACECONSTANT_VALUE = ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Constant Value attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_VALUE_ATTRIBUTE_FEATURE_COUNT = ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Constant Value attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_VALUE_ATTRIBUTE_OPERATION_COUNT = ABSTRACT_ATTRIBUTE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Value <em>Interface CONSTANT Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Value
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInterfaceCONSTANT_Value()
	 * @generated
	 */
	int INTERFACE_CONSTANT_VALUE = 24;

	/**
	 * The number of structural features of the '<em>Interface CONSTANT Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_CONSTANT_VALUE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Interface CONSTANT Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_CONSTANT_VALUE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.Code_attributeImpl <em>Code attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.Code_attributeImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCode_attribute()
	 * @generated
	 */
	int CODE_ATTRIBUTE = 25;

	/**
	 * The feature id for the '<em><b>Attribute length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_ATTRIBUTE__ATTRIBUTE_LENGTH = ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_LENGTH;

	/**
	 * The feature id for the '<em><b>Attribute name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_ATTRIBUTE__ATTRIBUTE_NAME = ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_NAME;

	/**
	 * The feature id for the '<em><b>Max stack</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_ATTRIBUTE__MAX_STACK = ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max locals</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_ATTRIBUTE__MAX_LOCALS = ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Code length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_ATTRIBUTE__CODE_LENGTH = ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_ATTRIBUTE__CODE = ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Exception table</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_ATTRIBUTE__EXCEPTION_TABLE = ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Exception table length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_ATTRIBUTE__EXCEPTION_TABLE_LENGTH = ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Attributes count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_ATTRIBUTE__ATTRIBUTES_COUNT = ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Interfacecode attribute</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_ATTRIBUTE__INTERFACECODE_ATTRIBUTE = ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Code attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_ATTRIBUTE_FEATURE_COUNT = ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Code attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_ATTRIBUTE_OPERATION_COUNT = ABSTRACT_ATTRIBUTE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.exception_table_entryImpl <em>exception table entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.exception_table_entryImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getexception_table_entry()
	 * @generated
	 */
	int EXCEPTION_TABLE_ENTRY = 26;

	/**
	 * The feature id for the '<em><b>Start pc</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_TABLE_ENTRY__START_PC = 0;

	/**
	 * The feature id for the '<em><b>End pc</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_TABLE_ENTRY__END_PC = 1;

	/**
	 * The feature id for the '<em><b>Handler pc</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_TABLE_ENTRY__HANDLER_PC = 2;

	/**
	 * The feature id for the '<em><b>Catch type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_TABLE_ENTRY__CATCH_TYPE = 3;

	/**
	 * The number of structural features of the '<em>exception table entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_TABLE_ENTRY_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>exception table entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_TABLE_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.StackMapTable_attributeImpl <em>Stack Map Table attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.StackMapTable_attributeImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getStackMapTable_attribute()
	 * @generated
	 */
	int STACK_MAP_TABLE_ATTRIBUTE = 27;

	/**
	 * The feature id for the '<em><b>Attribute length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STACK_MAP_TABLE_ATTRIBUTE__ATTRIBUTE_LENGTH = ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_LENGTH;

	/**
	 * The feature id for the '<em><b>Attribute name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STACK_MAP_TABLE_ATTRIBUTE__ATTRIBUTE_NAME = ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_NAME;

	/**
	 * The feature id for the '<em><b>Number of entries</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STACK_MAP_TABLE_ATTRIBUTE__NUMBER_OF_ENTRIES = ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Abstractstack map frame</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STACK_MAP_TABLE_ATTRIBUTE__ABSTRACTSTACK_MAP_FRAME = ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Stack Map Table attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STACK_MAP_TABLE_ATTRIBUTE_FEATURE_COUNT = ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Stack Map Table attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STACK_MAP_TABLE_ATTRIBUTE_OPERATION_COUNT = ABSTRACT_ATTRIBUTE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.AbstractVerification_type_infoImpl <em>Abstract Verification type info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.AbstractVerification_type_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getAbstractVerification_type_info()
	 * @generated
	 */
	int ABSTRACT_VERIFICATION_TYPE_INFO = 28;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_VERIFICATION_TYPE_INFO__TAG = 0;

	/**
	 * The number of structural features of the '<em>Abstract Verification type info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_VERIFICATION_TYPE_INFO_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Abstract Verification type info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_VERIFICATION_TYPE_INFO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.Top_variable_infoImpl <em>Top variable info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.Top_variable_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getTop_variable_info()
	 * @generated
	 */
	int TOP_VARIABLE_INFO = 29;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_VARIABLE_INFO__TAG = ABSTRACT_VERIFICATION_TYPE_INFO__TAG;

	/**
	 * The number of structural features of the '<em>Top variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_VARIABLE_INFO_FEATURE_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Top variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_VARIABLE_INFO_OPERATION_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.Integer_variable_infoImpl <em>Integer variable info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.Integer_variable_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInteger_variable_info()
	 * @generated
	 */
	int INTEGER_VARIABLE_INFO = 30;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VARIABLE_INFO__TAG = ABSTRACT_VERIFICATION_TYPE_INFO__TAG;

	/**
	 * The number of structural features of the '<em>Integer variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VARIABLE_INFO_FEATURE_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Integer variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VARIABLE_INFO_OPERATION_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.Float_variable_infoImpl <em>Float variable info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.Float_variable_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getFloat_variable_info()
	 * @generated
	 */
	int FLOAT_VARIABLE_INFO = 31;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VARIABLE_INFO__TAG = ABSTRACT_VERIFICATION_TYPE_INFO__TAG;

	/**
	 * The number of structural features of the '<em>Float variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VARIABLE_INFO_FEATURE_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Float variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VARIABLE_INFO_OPERATION_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.Null_variable_infoImpl <em>Null variable info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.Null_variable_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getNull_variable_info()
	 * @generated
	 */
	int NULL_VARIABLE_INFO = 32;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VARIABLE_INFO__TAG = ABSTRACT_VERIFICATION_TYPE_INFO__TAG;

	/**
	 * The number of structural features of the '<em>Null variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VARIABLE_INFO_FEATURE_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Null variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VARIABLE_INFO_OPERATION_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.UninitializedThis_variable_infoImpl <em>Uninitialized This variable info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.UninitializedThis_variable_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getUninitializedThis_variable_info()
	 * @generated
	 */
	int UNINITIALIZED_THIS_VARIABLE_INFO = 33;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNINITIALIZED_THIS_VARIABLE_INFO__TAG = ABSTRACT_VERIFICATION_TYPE_INFO__TAG;

	/**
	 * The number of structural features of the '<em>Uninitialized This variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNINITIALIZED_THIS_VARIABLE_INFO_FEATURE_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Uninitialized This variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNINITIALIZED_THIS_VARIABLE_INFO_OPERATION_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.Object_variable_infoImpl <em>Object variable info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.Object_variable_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getObject_variable_info()
	 * @generated
	 */
	int OBJECT_VARIABLE_INFO = 34;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_VARIABLE_INFO__TAG = ABSTRACT_VERIFICATION_TYPE_INFO__TAG;

	/**
	 * The feature id for the '<em><b>Constant class info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_VARIABLE_INFO__CONSTANT_CLASS_INFO = ABSTRACT_VERIFICATION_TYPE_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Object variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_VARIABLE_INFO_FEATURE_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Object variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_VARIABLE_INFO_OPERATION_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.NewEClass36Impl <em>New EClass36</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.NewEClass36Impl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getNewEClass36()
	 * @generated
	 */
	int NEW_ECLASS36 = 35;

	/**
	 * The number of structural features of the '<em>New EClass36</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_ECLASS36_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>New EClass36</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_ECLASS36_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.Uninitialized_variable_infoImpl <em>Uninitialized variable info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.Uninitialized_variable_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getUninitialized_variable_info()
	 * @generated
	 */
	int UNINITIALIZED_VARIABLE_INFO = 36;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNINITIALIZED_VARIABLE_INFO__TAG = ABSTRACT_VERIFICATION_TYPE_INFO__TAG;

	/**
	 * The feature id for the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNINITIALIZED_VARIABLE_INFO__OFFSET = ABSTRACT_VERIFICATION_TYPE_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Uninitialized variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNINITIALIZED_VARIABLE_INFO_FEATURE_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Uninitialized variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNINITIALIZED_VARIABLE_INFO_OPERATION_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.Long_variable_infoImpl <em>Long variable info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.Long_variable_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getLong_variable_info()
	 * @generated
	 */
	int LONG_VARIABLE_INFO = 37;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LONG_VARIABLE_INFO__TAG = ABSTRACT_VERIFICATION_TYPE_INFO__TAG;

	/**
	 * The number of structural features of the '<em>Long variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LONG_VARIABLE_INFO_FEATURE_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Long variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LONG_VARIABLE_INFO_OPERATION_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.Double_variable_infoImpl <em>Double variable info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.Double_variable_infoImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getDouble_variable_info()
	 * @generated
	 */
	int DOUBLE_VARIABLE_INFO = 38;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_VARIABLE_INFO__TAG = ABSTRACT_VERIFICATION_TYPE_INFO__TAG;

	/**
	 * The number of structural features of the '<em>Double variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_VARIABLE_INFO_FEATURE_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Double variable info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_VARIABLE_INFO_OPERATION_COUNT = ABSTRACT_VERIFICATION_TYPE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.AbstractStack_map_frameImpl <em>Abstract Stack map frame</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.AbstractStack_map_frameImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getAbstractStack_map_frame()
	 * @generated
	 */
	int ABSTRACT_STACK_MAP_FRAME = 39;

	/**
	 * The feature id for the '<em><b>Frame type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STACK_MAP_FRAME__FRAME_TYPE = 0;

	/**
	 * The number of structural features of the '<em>Abstract Stack map frame</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Abstract Stack map frame</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STACK_MAP_FRAME_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.same_frameImpl <em>same frame</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.same_frameImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getsame_frame()
	 * @generated
	 */
	int SAME_FRAME = 40;

	/**
	 * The feature id for the '<em><b>Frame type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_FRAME__FRAME_TYPE = ABSTRACT_STACK_MAP_FRAME__FRAME_TYPE;

	/**
	 * The number of structural features of the '<em>same frame</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_FRAME_FEATURE_COUNT = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>same frame</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_FRAME_OPERATION_COUNT = ABSTRACT_STACK_MAP_FRAME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.same_locals_1_stack_item_frameImpl <em>same locals 1stack item frame</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.same_locals_1_stack_item_frameImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getsame_locals_1_stack_item_frame()
	 * @generated
	 */
	int SAME_LOCALS_1STACK_ITEM_FRAME = 41;

	/**
	 * The feature id for the '<em><b>Frame type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_LOCALS_1STACK_ITEM_FRAME__FRAME_TYPE = ABSTRACT_STACK_MAP_FRAME__FRAME_TYPE;

	/**
	 * The feature id for the '<em><b>Stack</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_LOCALS_1STACK_ITEM_FRAME__STACK = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>same locals 1stack item frame</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_LOCALS_1STACK_ITEM_FRAME_FEATURE_COUNT = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>same locals 1stack item frame</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_LOCALS_1STACK_ITEM_FRAME_OPERATION_COUNT = ABSTRACT_STACK_MAP_FRAME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.same_locals_1_stack_item_frame_extendedImpl <em>same locals 1stack item frame extended</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.same_locals_1_stack_item_frame_extendedImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getsame_locals_1_stack_item_frame_extended()
	 * @generated
	 */
	int SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED = 42;

	/**
	 * The feature id for the '<em><b>Frame type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__FRAME_TYPE = ABSTRACT_STACK_MAP_FRAME__FRAME_TYPE;

	/**
	 * The feature id for the '<em><b>Offset delta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__OFFSET_DELTA = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Stack</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__STACK = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>same locals 1stack item frame extended</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED_FEATURE_COUNT = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>same locals 1stack item frame extended</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED_OPERATION_COUNT = ABSTRACT_STACK_MAP_FRAME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.same_frame_extendedImpl <em>same frame extended</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.same_frame_extendedImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getsame_frame_extended()
	 * @generated
	 */
	int SAME_FRAME_EXTENDED = 43;

	/**
	 * The feature id for the '<em><b>Frame type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_FRAME_EXTENDED__FRAME_TYPE = ABSTRACT_STACK_MAP_FRAME__FRAME_TYPE;

	/**
	 * The feature id for the '<em><b>Offset delta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_FRAME_EXTENDED__OFFSET_DELTA = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>same frame extended</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_FRAME_EXTENDED_FEATURE_COUNT = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>same frame extended</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_FRAME_EXTENDED_OPERATION_COUNT = ABSTRACT_STACK_MAP_FRAME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.chop_frameImpl <em>chop frame</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.chop_frameImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getchop_frame()
	 * @generated
	 */
	int CHOP_FRAME = 44;

	/**
	 * The feature id for the '<em><b>Frame type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOP_FRAME__FRAME_TYPE = ABSTRACT_STACK_MAP_FRAME__FRAME_TYPE;

	/**
	 * The feature id for the '<em><b>Offset delta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOP_FRAME__OFFSET_DELTA = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>chop frame</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOP_FRAME_FEATURE_COUNT = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>chop frame</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOP_FRAME_OPERATION_COUNT = ABSTRACT_STACK_MAP_FRAME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.append_frameImpl <em>append frame</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.append_frameImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getappend_frame()
	 * @generated
	 */
	int APPEND_FRAME = 45;

	/**
	 * The feature id for the '<em><b>Frame type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPEND_FRAME__FRAME_TYPE = ABSTRACT_STACK_MAP_FRAME__FRAME_TYPE;

	/**
	 * The feature id for the '<em><b>Offset delta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPEND_FRAME__OFFSET_DELTA = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Locals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPEND_FRAME__LOCALS = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>append frame</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPEND_FRAME_FEATURE_COUNT = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>append frame</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPEND_FRAME_OPERATION_COUNT = ABSTRACT_STACK_MAP_FRAME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.full_frameImpl <em>full frame</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.full_frameImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getfull_frame()
	 * @generated
	 */
	int FULL_FRAME = 46;

	/**
	 * The feature id for the '<em><b>Frame type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FULL_FRAME__FRAME_TYPE = ABSTRACT_STACK_MAP_FRAME__FRAME_TYPE;

	/**
	 * The feature id for the '<em><b>Offset delta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FULL_FRAME__OFFSET_DELTA = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Number of locals</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FULL_FRAME__NUMBER_OF_LOCALS = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Number of stack items</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FULL_FRAME__NUMBER_OF_STACK_ITEMS = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Locals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FULL_FRAME__LOCALS = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Stack</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FULL_FRAME__STACK = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>full frame</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FULL_FRAME_FEATURE_COUNT = ABSTRACT_STACK_MAP_FRAME_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>full frame</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FULL_FRAME_OPERATION_COUNT = ABSTRACT_STACK_MAP_FRAME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.Exceptions_attributeImpl <em>Exceptions attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.Exceptions_attributeImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getExceptions_attribute()
	 * @generated
	 */
	int EXCEPTIONS_ATTRIBUTE = 47;

	/**
	 * The feature id for the '<em><b>Attribute length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_LENGTH = INTERFACE_METHOD_ATTRIBUTE_INFO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attribute name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTIONS_ATTRIBUTE__ATTRIBUTE_NAME = INTERFACE_METHOD_ATTRIBUTE_INFO_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Exception table</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTIONS_ATTRIBUTE__EXCEPTION_TABLE = INTERFACE_METHOD_ATTRIBUTE_INFO_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Number of exceptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTIONS_ATTRIBUTE__NUMBER_OF_EXCEPTIONS = INTERFACE_METHOD_ATTRIBUTE_INFO_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Exceptions attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTIONS_ATTRIBUTE_FEATURE_COUNT = INTERFACE_METHOD_ATTRIBUTE_INFO_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Exceptions attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTIONS_ATTRIBUTE_OPERATION_COUNT = INTERFACE_METHOD_ATTRIBUTE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.InterfaceCode_attribute <em>Interface Code attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.InterfaceCode_attribute
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInterfaceCode_attribute()
	 * @generated
	 */
	int INTERFACE_CODE_ATTRIBUTE = 48;

	/**
	 * The number of structural features of the '<em>Interface Code attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_CODE_ATTRIBUTE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Interface Code attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_CODE_ATTRIBUTE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.InnerClasses_attributeImpl <em>Inner Classes attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.InnerClasses_attributeImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInnerClasses_attribute()
	 * @generated
	 */
	int INNER_CLASSES_ATTRIBUTE = 49;

	/**
	 * The feature id for the '<em><b>Number of classes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INNER_CLASSES_ATTRIBUTE__NUMBER_OF_CLASSES = INTERFACE_CLASS_ATTRIBUTE_INFO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Classes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INNER_CLASSES_ATTRIBUTE__CLASSES = INTERFACE_CLASS_ATTRIBUTE_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Inner Classes attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INNER_CLASSES_ATTRIBUTE_FEATURE_COUNT = INTERFACE_CLASS_ATTRIBUTE_INFO_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Inner Classes attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INNER_CLASSES_ATTRIBUTE_OPERATION_COUNT = INTERFACE_CLASS_ATTRIBUTE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.Class_memberImpl <em>Class member</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.Class_memberImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getClass_member()
	 * @generated
	 */
	int CLASS_MEMBER = 50;

	/**
	 * The feature id for the '<em><b>Inner class access flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_MEMBER__INNER_CLASS_ACCESS_FLAGS = 0;

	/**
	 * The feature id for the '<em><b>Inner class info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_MEMBER__INNER_CLASS_INFO = 1;

	/**
	 * The feature id for the '<em><b>Outer class info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_MEMBER__OUTER_CLASS_INFO = 2;

	/**
	 * The feature id for the '<em><b>Inner name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_MEMBER__INNER_NAME = 3;

	/**
	 * The number of structural features of the '<em>Class member</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_MEMBER_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Class member</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_MEMBER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.Synthetic_attributeImpl <em>Synthetic attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.Synthetic_attributeImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getSynthetic_attribute()
	 * @generated
	 */
	int SYNTHETIC_ATTRIBUTE = 51;

	/**
	 * The feature id for the '<em><b>Attribute length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNTHETIC_ATTRIBUTE__ATTRIBUTE_LENGTH = ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_LENGTH;

	/**
	 * The feature id for the '<em><b>Attribute name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNTHETIC_ATTRIBUTE__ATTRIBUTE_NAME = ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_NAME;

	/**
	 * The number of structural features of the '<em>Synthetic attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNTHETIC_ATTRIBUTE_FEATURE_COUNT = ABSTRACT_ATTRIBUTE_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Synthetic attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNTHETIC_ATTRIBUTE_OPERATION_COUNT = ABSTRACT_ATTRIBUTE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.Signature_attributeImpl <em>Signature attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.Signature_attributeImpl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getSignature_attribute()
	 * @generated
	 */
	int SIGNATURE_ATTRIBUTE = 52;

	/**
	 * The feature id for the '<em><b>Attribute length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE_ATTRIBUTE__ATTRIBUTE_LENGTH = INTERFACE_CLASS_ATTRIBUTE_INFO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attribute name</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE_ATTRIBUTE__ATTRIBUTE_NAME = INTERFACE_CLASS_ATTRIBUTE_INFO_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE_ATTRIBUTE__SIGNATURE = INTERFACE_CLASS_ATTRIBUTE_INFO_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Signature attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE_ATTRIBUTE_FEATURE_COUNT = INTERFACE_CLASS_ATTRIBUTE_INFO_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Signature attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE_ATTRIBUTE_OPERATION_COUNT = INTERFACE_CLASS_ATTRIBUTE_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.cristi.exemplu.ecoregraph.impl.NewEClass54Impl <em>New EClass54</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.cristi.exemplu.ecoregraph.impl.NewEClass54Impl
	 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getNewEClass54()
	 * @generated
	 */
	int NEW_ECLASS54 = 53;

	/**
	 * The number of structural features of the '<em>New EClass54</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_ECLASS54_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>New EClass54</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_ECLASS54_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_MethodHandle_Info <em>CONSTANT Method Handle Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CONSTANT Method Handle Info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_MethodHandle_Info
	 * @generated
	 */
	EClass getCONSTANT_MethodHandle_Info();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.CONSTANT_MethodHandle_Info#getReference_kind <em>Reference kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reference kind</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_MethodHandle_Info#getReference_kind()
	 * @see #getCONSTANT_MethodHandle_Info()
	 * @generated
	 */
	EAttribute getCONSTANT_MethodHandle_Info_Reference_kind();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.CONSTANT_MethodHandle_Info#getConstant_ref_info <em>Constant ref info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constant ref info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_MethodHandle_Info#getConstant_ref_info()
	 * @see #getCONSTANT_MethodHandle_Info()
	 * @generated
	 */
	EReference getCONSTANT_MethodHandle_Info_Constant_ref_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Fieldref_info <em>CONSTANT Fieldref info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CONSTANT Fieldref info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Fieldref_info
	 * @generated
	 */
	EClass getCONSTANT_Fieldref_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Fieldref_info#getConstant_class_info <em>Constant class info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constant class info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Fieldref_info#getConstant_class_info()
	 * @see #getCONSTANT_Fieldref_info()
	 * @generated
	 */
	EReference getCONSTANT_Fieldref_info_Constant_class_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Fieldref_info#getConstant_nameandtype_info <em>Constant nameandtype info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constant nameandtype info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Fieldref_info#getConstant_nameandtype_info()
	 * @see #getCONSTANT_Fieldref_info()
	 * @generated
	 */
	EReference getCONSTANT_Fieldref_info_Constant_nameandtype_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Methodref_info <em>CONSTANT Methodref info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CONSTANT Methodref info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Methodref_info
	 * @generated
	 */
	EClass getCONSTANT_Methodref_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Methodref_info#getConstant_class_info <em>Constant class info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constant class info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Methodref_info#getConstant_class_info()
	 * @see #getCONSTANT_Methodref_info()
	 * @generated
	 */
	EReference getCONSTANT_Methodref_info_Constant_class_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Methodref_info#getConstant_nameandtype_info <em>Constant nameandtype info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constant nameandtype info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Methodref_info#getConstant_nameandtype_info()
	 * @see #getCONSTANT_Methodref_info()
	 * @generated
	 */
	EReference getCONSTANT_Methodref_info_Constant_nameandtype_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_InterfaceMethodref_info <em>CONSTANT Interface Methodref info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CONSTANT Interface Methodref info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_InterfaceMethodref_info
	 * @generated
	 */
	EClass getCONSTANT_InterfaceMethodref_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.CONSTANT_InterfaceMethodref_info#getConstant_class_info <em>Constant class info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constant class info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_InterfaceMethodref_info#getConstant_class_info()
	 * @see #getCONSTANT_InterfaceMethodref_info()
	 * @generated
	 */
	EReference getCONSTANT_InterfaceMethodref_info_Constant_class_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.CONSTANT_InterfaceMethodref_info#getConstant_nameandtype_info <em>Constant nameandtype info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constant nameandtype info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_InterfaceMethodref_info#getConstant_nameandtype_info()
	 * @see #getCONSTANT_InterfaceMethodref_info()
	 * @generated
	 */
	EReference getCONSTANT_InterfaceMethodref_info_Constant_nameandtype_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Class_info <em>CONSTANT Class info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CONSTANT Class info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Class_info
	 * @generated
	 */
	EClass getCONSTANT_Class_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Class_info#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Name</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Class_info#getName()
	 * @see #getCONSTANT_Class_info()
	 * @generated
	 */
	EReference getCONSTANT_Class_info_Name();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info <em>CONSTANT Name And Type info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CONSTANT Name And Type info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info
	 * @generated
	 */
	EClass getCONSTANT_NameAndType_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Name</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info#getName()
	 * @see #getCONSTANT_NameAndType_info()
	 * @generated
	 */
	EReference getCONSTANT_NameAndType_info_Name();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info#getField_method_descriptor <em>Field method descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Field method descriptor</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_NameAndType_info#getField_method_descriptor()
	 * @see #getCONSTANT_NameAndType_info()
	 * @generated
	 */
	EReference getCONSTANT_NameAndType_info_Field_method_descriptor();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info <em>CONSTANT Utf8 info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CONSTANT Utf8 info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info
	 * @generated
	 */
	EClass getCONSTANT_Utf8_info();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info#getLength <em>Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Length</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info#getLength()
	 * @see #getCONSTANT_Utf8_info()
	 * @generated
	 */
	EAttribute getCONSTANT_Utf8_info_Length();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info#getBytes <em>Bytes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bytes</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Utf8_info#getBytes()
	 * @see #getCONSTANT_Utf8_info()
	 * @generated
	 */
	EAttribute getCONSTANT_Utf8_info_Bytes();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_MethodType_info <em>CONSTANT Method Type info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CONSTANT Method Type info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_MethodType_info
	 * @generated
	 */
	EClass getCONSTANT_MethodType_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.CONSTANT_MethodType_info#getMethod_descriptor <em>Method descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Method descriptor</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_MethodType_info#getMethod_descriptor()
	 * @see #getCONSTANT_MethodType_info()
	 * @generated
	 */
	EReference getCONSTANT_MethodType_info_Method_descriptor();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_String_info <em>CONSTANT String info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CONSTANT String info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_String_info
	 * @generated
	 */
	EClass getCONSTANT_String_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.CONSTANT_String_info#getConstant_utf8_info <em>Constant utf8 info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constant utf8 info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_String_info#getConstant_utf8_info()
	 * @see #getCONSTANT_String_info()
	 * @generated
	 */
	EReference getCONSTANT_String_info_Constant_utf8_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_InvokeDynamic_info <em>CONSTANT Invoke Dynamic info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CONSTANT Invoke Dynamic info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_InvokeDynamic_info
	 * @generated
	 */
	EClass getCONSTANT_InvokeDynamic_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.CONSTANT_InvokeDynamic_info#getConstant_nameandtype_info <em>Constant nameandtype info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constant nameandtype info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_InvokeDynamic_info#getConstant_nameandtype_info()
	 * @see #getCONSTANT_InvokeDynamic_info()
	 * @generated
	 */
	EReference getCONSTANT_InvokeDynamic_info_Constant_nameandtype_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.Field_info <em>Field info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Field_info
	 * @generated
	 */
	EClass getField_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.Field_info#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Name</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Field_info#getName()
	 * @see #getField_info()
	 * @generated
	 */
	EReference getField_info_Name();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.Field_info#getField_descriptor <em>Field descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Field descriptor</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Field_info#getField_descriptor()
	 * @see #getField_info()
	 * @generated
	 */
	EReference getField_info_Field_descriptor();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.Field_info#getAccess_flags <em>Access flags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Access flags</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Field_info#getAccess_flags()
	 * @see #getField_info()
	 * @generated
	 */
	EAttribute getField_info_Access_flags();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.Field_info#getAttributes_count <em>Attributes count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attributes count</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Field_info#getAttributes_count()
	 * @see #getField_info()
	 * @generated
	 */
	EAttribute getField_info_Attributes_count();

	/**
	 * Returns the meta object for the containment reference list '{@link com.cristi.exemplu.ecoregraph.Field_info#getInterfacefield_attribute_info <em>Interfacefield attribute info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interfacefield attribute info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Field_info#getInterfacefield_attribute_info()
	 * @see #getField_info()
	 * @generated
	 */
	EReference getField_info_Interfacefield_attribute_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.Method_info <em>Method info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Method info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Method_info
	 * @generated
	 */
	EClass getMethod_info();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.Method_info#getAccess_flags <em>Access flags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Access flags</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Method_info#getAccess_flags()
	 * @see #getMethod_info()
	 * @generated
	 */
	EAttribute getMethod_info_Access_flags();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.Method_info#getAttributes_count <em>Attributes count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attributes count</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Method_info#getAttributes_count()
	 * @see #getMethod_info()
	 * @generated
	 */
	EAttribute getMethod_info_Attributes_count();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.Method_info#getMethod_descriptor <em>Method descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Method descriptor</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Method_info#getMethod_descriptor()
	 * @see #getMethod_info()
	 * @generated
	 */
	EReference getMethod_info_Method_descriptor();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.Method_info#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Name</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Method_info#getName()
	 * @see #getMethod_info()
	 * @generated
	 */
	EReference getMethod_info_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link com.cristi.exemplu.ecoregraph.Method_info#getInterfacemethod_attribute_info <em>Interfacemethod attribute info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interfacemethod attribute info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Method_info#getInterfacemethod_attribute_info()
	 * @see #getMethod_info()
	 * @generated
	 */
	EReference getMethod_info_Interfacemethod_attribute_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.AbstractCp_info <em>Abstract Cp info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Cp info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.AbstractCp_info
	 * @generated
	 */
	EClass getAbstractCp_info();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.AbstractCp_info#getTag <em>Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tag</em>'.
	 * @see com.cristi.exemplu.ecoregraph.AbstractCp_info#getTag()
	 * @see #getAbstractCp_info()
	 * @generated
	 */
	EAttribute getAbstractCp_info_Tag();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Ref_info <em>Interface CONSTANT Ref info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface CONSTANT Ref info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Ref_info
	 * @generated
	 */
	EClass getInterfaceCONSTANT_Ref_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Integer_info <em>CONSTANT Integer info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CONSTANT Integer info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Integer_info
	 * @generated
	 */
	EClass getCONSTANT_Integer_info();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Integer_info#getBytes <em>Bytes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bytes</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Integer_info#getBytes()
	 * @see #getCONSTANT_Integer_info()
	 * @generated
	 */
	EAttribute getCONSTANT_Integer_info_Bytes();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Float_info <em>CONSTANT Float info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CONSTANT Float info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Float_info
	 * @generated
	 */
	EClass getCONSTANT_Float_info();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Float_info#getBytes <em>Bytes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bytes</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Float_info#getBytes()
	 * @see #getCONSTANT_Float_info()
	 * @generated
	 */
	EAttribute getCONSTANT_Float_info_Bytes();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Long_info <em>CONSTANT Long info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CONSTANT Long info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Long_info
	 * @generated
	 */
	EClass getCONSTANT_Long_info();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Long_info#getHigh_bytes <em>High bytes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>High bytes</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Long_info#getHigh_bytes()
	 * @see #getCONSTANT_Long_info()
	 * @generated
	 */
	EAttribute getCONSTANT_Long_info_High_bytes();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Long_info#getLow_bytes <em>Low bytes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Low bytes</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Long_info#getLow_bytes()
	 * @see #getCONSTANT_Long_info()
	 * @generated
	 */
	EAttribute getCONSTANT_Long_info_Low_bytes();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Double_info <em>CONSTANT Double info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CONSTANT Double info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Double_info
	 * @generated
	 */
	EClass getCONSTANT_Double_info();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Double_info#getHigh_bytes <em>High bytes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>High bytes</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Double_info#getHigh_bytes()
	 * @see #getCONSTANT_Double_info()
	 * @generated
	 */
	EAttribute getCONSTANT_Double_info_High_bytes();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Double_info#getLow_bytes <em>Low bytes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Low bytes</em>'.
	 * @see com.cristi.exemplu.ecoregraph.CONSTANT_Double_info#getLow_bytes()
	 * @see #getCONSTANT_Double_info()
	 * @generated
	 */
	EAttribute getCONSTANT_Double_info_Low_bytes();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.ClassFile <em>Class File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class File</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile
	 * @generated
	 */
	EClass getClassFile();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.ClassFile#getMagic <em>Magic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Magic</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getMagic()
	 * @see #getClassFile()
	 * @generated
	 */
	EAttribute getClassFile_Magic();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.ClassFile#getMinor_version <em>Minor version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minor version</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getMinor_version()
	 * @see #getClassFile()
	 * @generated
	 */
	EAttribute getClassFile_Minor_version();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.ClassFile#getMajor_version <em>Major version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Major version</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getMajor_version()
	 * @see #getClassFile()
	 * @generated
	 */
	EAttribute getClassFile_Major_version();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.ClassFile#getConstant_pool_count <em>Constant pool count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constant pool count</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getConstant_pool_count()
	 * @see #getClassFile()
	 * @generated
	 */
	EAttribute getClassFile_Constant_pool_count();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.ClassFile#getAccess_flags <em>Access flags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Access flags</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getAccess_flags()
	 * @see #getClassFile()
	 * @generated
	 */
	EAttribute getClassFile_Access_flags();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.ClassFile#getInterfaces_count <em>Interfaces count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interfaces count</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getInterfaces_count()
	 * @see #getClassFile()
	 * @generated
	 */
	EAttribute getClassFile_Interfaces_count();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.ClassFile#getFields_count <em>Fields count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fields count</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getFields_count()
	 * @see #getClassFile()
	 * @generated
	 */
	EAttribute getClassFile_Fields_count();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.ClassFile#getMethods_count <em>Methods count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Methods count</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getMethods_count()
	 * @see #getClassFile()
	 * @generated
	 */
	EAttribute getClassFile_Methods_count();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.ClassFile#getAttributes_count <em>Attributes count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attributes count</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getAttributes_count()
	 * @see #getClassFile()
	 * @generated
	 */
	EAttribute getClassFile_Attributes_count();

	/**
	 * Returns the meta object for the containment reference list '{@link com.cristi.exemplu.ecoregraph.ClassFile#getCp_info <em>Cp info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Cp info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getCp_info()
	 * @see #getClassFile()
	 * @generated
	 */
	EReference getClassFile_Cp_info();

	/**
	 * Returns the meta object for the containment reference list '{@link com.cristi.exemplu.ecoregraph.ClassFile#getField_info <em>Field info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Field info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getField_info()
	 * @see #getClassFile()
	 * @generated
	 */
	EReference getClassFile_Field_info();

	/**
	 * Returns the meta object for the containment reference list '{@link com.cristi.exemplu.ecoregraph.ClassFile#getMethod_info <em>Method info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Method info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getMethod_info()
	 * @see #getClassFile()
	 * @generated
	 */
	EReference getClassFile_Method_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.ClassFile#getSuper_class <em>Super class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Super class</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getSuper_class()
	 * @see #getClassFile()
	 * @generated
	 */
	EReference getClassFile_Super_class();

	/**
	 * Returns the meta object for the reference list '{@link com.cristi.exemplu.ecoregraph.ClassFile#getInterfaces <em>Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Interfaces</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getInterfaces()
	 * @see #getClassFile()
	 * @generated
	 */
	EReference getClassFile_Interfaces();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.ClassFile#getThis_class <em>This class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>This class</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getThis_class()
	 * @see #getClassFile()
	 * @generated
	 */
	EReference getClassFile_This_class();

	/**
	 * Returns the meta object for the reference list '{@link com.cristi.exemplu.ecoregraph.ClassFile#getInterfaceclass_attribute_info <em>Interfaceclass attribute info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Interfaceclass attribute info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ClassFile#getInterfaceclass_attribute_info()
	 * @see #getClassFile()
	 * @generated
	 */
	EReference getClassFile_Interfaceclass_attribute_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.AbstractAttribute_info <em>Abstract Attribute info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Attribute info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.AbstractAttribute_info
	 * @generated
	 */
	EClass getAbstractAttribute_info();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.AbstractAttribute_info#getAttribute_length <em>Attribute length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute length</em>'.
	 * @see com.cristi.exemplu.ecoregraph.AbstractAttribute_info#getAttribute_length()
	 * @see #getAbstractAttribute_info()
	 * @generated
	 */
	EAttribute getAbstractAttribute_info_Attribute_length();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.AbstractAttribute_info#getAttribute_name <em>Attribute name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute name</em>'.
	 * @see com.cristi.exemplu.ecoregraph.AbstractAttribute_info#getAttribute_name()
	 * @see #getAbstractAttribute_info()
	 * @generated
	 */
	EReference getAbstractAttribute_info_Attribute_name();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.InterfaceMethod_attribute_info <em>Interface Method attribute info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface Method attribute info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.InterfaceMethod_attribute_info
	 * @generated
	 */
	EClass getInterfaceMethod_attribute_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.InterfaceField_attribute_info <em>Interface Field attribute info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface Field attribute info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.InterfaceField_attribute_info
	 * @generated
	 */
	EClass getInterfaceField_attribute_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.InterfaceClass_attribute_info <em>Interface Class attribute info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface Class attribute info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.InterfaceClass_attribute_info
	 * @generated
	 */
	EClass getInterfaceClass_attribute_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.ConstantValue_attribute <em>Constant Value attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant Value attribute</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ConstantValue_attribute
	 * @generated
	 */
	EClass getConstantValue_attribute();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.ConstantValue_attribute#getInterfaceconstant_value <em>Interfaceconstant value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Interfaceconstant value</em>'.
	 * @see com.cristi.exemplu.ecoregraph.ConstantValue_attribute#getInterfaceconstant_value()
	 * @see #getConstantValue_attribute()
	 * @generated
	 */
	EReference getConstantValue_attribute_Interfaceconstant_value();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Value <em>Interface CONSTANT Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface CONSTANT Value</em>'.
	 * @see com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Value
	 * @generated
	 */
	EClass getInterfaceCONSTANT_Value();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.Code_attribute <em>Code attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Code attribute</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Code_attribute
	 * @generated
	 */
	EClass getCode_attribute();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.Code_attribute#getMax_stack <em>Max stack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max stack</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Code_attribute#getMax_stack()
	 * @see #getCode_attribute()
	 * @generated
	 */
	EAttribute getCode_attribute_Max_stack();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.Code_attribute#getMax_locals <em>Max locals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max locals</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Code_attribute#getMax_locals()
	 * @see #getCode_attribute()
	 * @generated
	 */
	EAttribute getCode_attribute_Max_locals();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.Code_attribute#getCode_length <em>Code length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code length</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Code_attribute#getCode_length()
	 * @see #getCode_attribute()
	 * @generated
	 */
	EAttribute getCode_attribute_Code_length();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.Code_attribute#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Code_attribute#getCode()
	 * @see #getCode_attribute()
	 * @generated
	 */
	EAttribute getCode_attribute_Code();

	/**
	 * Returns the meta object for the containment reference list '{@link com.cristi.exemplu.ecoregraph.Code_attribute#getException_table <em>Exception table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Exception table</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Code_attribute#getException_table()
	 * @see #getCode_attribute()
	 * @generated
	 */
	EReference getCode_attribute_Exception_table();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.Code_attribute#getException_table_length <em>Exception table length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exception table length</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Code_attribute#getException_table_length()
	 * @see #getCode_attribute()
	 * @generated
	 */
	EAttribute getCode_attribute_Exception_table_length();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.Code_attribute#getAttributes_count <em>Attributes count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attributes count</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Code_attribute#getAttributes_count()
	 * @see #getCode_attribute()
	 * @generated
	 */
	EAttribute getCode_attribute_Attributes_count();

	/**
	 * Returns the meta object for the containment reference list '{@link com.cristi.exemplu.ecoregraph.Code_attribute#getInterfacecode_attribute <em>Interfacecode attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interfacecode attribute</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Code_attribute#getInterfacecode_attribute()
	 * @see #getCode_attribute()
	 * @generated
	 */
	EReference getCode_attribute_Interfacecode_attribute();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.exception_table_entry <em>exception table entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>exception table entry</em>'.
	 * @see com.cristi.exemplu.ecoregraph.exception_table_entry
	 * @generated
	 */
	EClass getexception_table_entry();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.exception_table_entry#getStart_pc <em>Start pc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start pc</em>'.
	 * @see com.cristi.exemplu.ecoregraph.exception_table_entry#getStart_pc()
	 * @see #getexception_table_entry()
	 * @generated
	 */
	EAttribute getexception_table_entry_Start_pc();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.exception_table_entry#getEnd_pc <em>End pc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End pc</em>'.
	 * @see com.cristi.exemplu.ecoregraph.exception_table_entry#getEnd_pc()
	 * @see #getexception_table_entry()
	 * @generated
	 */
	EAttribute getexception_table_entry_End_pc();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.exception_table_entry#getHandler_pc <em>Handler pc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Handler pc</em>'.
	 * @see com.cristi.exemplu.ecoregraph.exception_table_entry#getHandler_pc()
	 * @see #getexception_table_entry()
	 * @generated
	 */
	EAttribute getexception_table_entry_Handler_pc();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.exception_table_entry#getCatch_type <em>Catch type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Catch type</em>'.
	 * @see com.cristi.exemplu.ecoregraph.exception_table_entry#getCatch_type()
	 * @see #getexception_table_entry()
	 * @generated
	 */
	EReference getexception_table_entry_Catch_type();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.StackMapTable_attribute <em>Stack Map Table attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stack Map Table attribute</em>'.
	 * @see com.cristi.exemplu.ecoregraph.StackMapTable_attribute
	 * @generated
	 */
	EClass getStackMapTable_attribute();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.StackMapTable_attribute#getNumber_of_entries <em>Number of entries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number of entries</em>'.
	 * @see com.cristi.exemplu.ecoregraph.StackMapTable_attribute#getNumber_of_entries()
	 * @see #getStackMapTable_attribute()
	 * @generated
	 */
	EAttribute getStackMapTable_attribute_Number_of_entries();

	/**
	 * Returns the meta object for the containment reference list '{@link com.cristi.exemplu.ecoregraph.StackMapTable_attribute#getAbstractstack_map_frame <em>Abstractstack map frame</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Abstractstack map frame</em>'.
	 * @see com.cristi.exemplu.ecoregraph.StackMapTable_attribute#getAbstractstack_map_frame()
	 * @see #getStackMapTable_attribute()
	 * @generated
	 */
	EReference getStackMapTable_attribute_Abstractstack_map_frame();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.AbstractVerification_type_info <em>Abstract Verification type info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Verification type info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.AbstractVerification_type_info
	 * @generated
	 */
	EClass getAbstractVerification_type_info();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.AbstractVerification_type_info#getTag <em>Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tag</em>'.
	 * @see com.cristi.exemplu.ecoregraph.AbstractVerification_type_info#getTag()
	 * @see #getAbstractVerification_type_info()
	 * @generated
	 */
	EAttribute getAbstractVerification_type_info_Tag();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.Top_variable_info <em>Top variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Top variable info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Top_variable_info
	 * @generated
	 */
	EClass getTop_variable_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.Integer_variable_info <em>Integer variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer variable info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Integer_variable_info
	 * @generated
	 */
	EClass getInteger_variable_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.Float_variable_info <em>Float variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Float variable info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Float_variable_info
	 * @generated
	 */
	EClass getFloat_variable_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.Null_variable_info <em>Null variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Null variable info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Null_variable_info
	 * @generated
	 */
	EClass getNull_variable_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.UninitializedThis_variable_info <em>Uninitialized This variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uninitialized This variable info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.UninitializedThis_variable_info
	 * @generated
	 */
	EClass getUninitializedThis_variable_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.Object_variable_info <em>Object variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object variable info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Object_variable_info
	 * @generated
	 */
	EClass getObject_variable_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.Object_variable_info#getConstant_class_info <em>Constant class info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constant class info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Object_variable_info#getConstant_class_info()
	 * @see #getObject_variable_info()
	 * @generated
	 */
	EReference getObject_variable_info_Constant_class_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.NewEClass36 <em>New EClass36</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>New EClass36</em>'.
	 * @see com.cristi.exemplu.ecoregraph.NewEClass36
	 * @generated
	 */
	EClass getNewEClass36();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.Uninitialized_variable_info <em>Uninitialized variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uninitialized variable info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Uninitialized_variable_info
	 * @generated
	 */
	EClass getUninitialized_variable_info();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.Uninitialized_variable_info#getOffset <em>Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Uninitialized_variable_info#getOffset()
	 * @see #getUninitialized_variable_info()
	 * @generated
	 */
	EAttribute getUninitialized_variable_info_Offset();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.Long_variable_info <em>Long variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Long variable info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Long_variable_info
	 * @generated
	 */
	EClass getLong_variable_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.Double_variable_info <em>Double variable info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Double variable info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Double_variable_info
	 * @generated
	 */
	EClass getDouble_variable_info();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.AbstractStack_map_frame <em>Abstract Stack map frame</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Stack map frame</em>'.
	 * @see com.cristi.exemplu.ecoregraph.AbstractStack_map_frame
	 * @generated
	 */
	EClass getAbstractStack_map_frame();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.AbstractStack_map_frame#getFrame_type <em>Frame type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Frame type</em>'.
	 * @see com.cristi.exemplu.ecoregraph.AbstractStack_map_frame#getFrame_type()
	 * @see #getAbstractStack_map_frame()
	 * @generated
	 */
	EAttribute getAbstractStack_map_frame_Frame_type();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.same_frame <em>same frame</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>same frame</em>'.
	 * @see com.cristi.exemplu.ecoregraph.same_frame
	 * @generated
	 */
	EClass getsame_frame();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame <em>same locals 1stack item frame</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>same locals 1stack item frame</em>'.
	 * @see com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame
	 * @generated
	 */
	EClass getsame_locals_1_stack_item_frame();

	/**
	 * Returns the meta object for the containment reference '{@link com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame#getStack <em>Stack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Stack</em>'.
	 * @see com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame#getStack()
	 * @see #getsame_locals_1_stack_item_frame()
	 * @generated
	 */
	EReference getsame_locals_1_stack_item_frame_Stack();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame_extended <em>same locals 1stack item frame extended</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>same locals 1stack item frame extended</em>'.
	 * @see com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame_extended
	 * @generated
	 */
	EClass getsame_locals_1_stack_item_frame_extended();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame_extended#getOffset_delta <em>Offset delta</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset delta</em>'.
	 * @see com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame_extended#getOffset_delta()
	 * @see #getsame_locals_1_stack_item_frame_extended()
	 * @generated
	 */
	EAttribute getsame_locals_1_stack_item_frame_extended_Offset_delta();

	/**
	 * Returns the meta object for the containment reference '{@link com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame_extended#getStack <em>Stack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Stack</em>'.
	 * @see com.cristi.exemplu.ecoregraph.same_locals_1_stack_item_frame_extended#getStack()
	 * @see #getsame_locals_1_stack_item_frame_extended()
	 * @generated
	 */
	EReference getsame_locals_1_stack_item_frame_extended_Stack();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.same_frame_extended <em>same frame extended</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>same frame extended</em>'.
	 * @see com.cristi.exemplu.ecoregraph.same_frame_extended
	 * @generated
	 */
	EClass getsame_frame_extended();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.same_frame_extended#getOffset_delta <em>Offset delta</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset delta</em>'.
	 * @see com.cristi.exemplu.ecoregraph.same_frame_extended#getOffset_delta()
	 * @see #getsame_frame_extended()
	 * @generated
	 */
	EAttribute getsame_frame_extended_Offset_delta();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.chop_frame <em>chop frame</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>chop frame</em>'.
	 * @see com.cristi.exemplu.ecoregraph.chop_frame
	 * @generated
	 */
	EClass getchop_frame();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.chop_frame#getOffset_delta <em>Offset delta</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset delta</em>'.
	 * @see com.cristi.exemplu.ecoregraph.chop_frame#getOffset_delta()
	 * @see #getchop_frame()
	 * @generated
	 */
	EAttribute getchop_frame_Offset_delta();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.append_frame <em>append frame</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>append frame</em>'.
	 * @see com.cristi.exemplu.ecoregraph.append_frame
	 * @generated
	 */
	EClass getappend_frame();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.append_frame#getOffset_delta <em>Offset delta</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset delta</em>'.
	 * @see com.cristi.exemplu.ecoregraph.append_frame#getOffset_delta()
	 * @see #getappend_frame()
	 * @generated
	 */
	EAttribute getappend_frame_Offset_delta();

	/**
	 * Returns the meta object for the containment reference list '{@link com.cristi.exemplu.ecoregraph.append_frame#getLocals <em>Locals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Locals</em>'.
	 * @see com.cristi.exemplu.ecoregraph.append_frame#getLocals()
	 * @see #getappend_frame()
	 * @generated
	 */
	EReference getappend_frame_Locals();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.full_frame <em>full frame</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>full frame</em>'.
	 * @see com.cristi.exemplu.ecoregraph.full_frame
	 * @generated
	 */
	EClass getfull_frame();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.full_frame#getOffset_delta <em>Offset delta</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset delta</em>'.
	 * @see com.cristi.exemplu.ecoregraph.full_frame#getOffset_delta()
	 * @see #getfull_frame()
	 * @generated
	 */
	EAttribute getfull_frame_Offset_delta();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.full_frame#getNumber_of_locals <em>Number of locals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number of locals</em>'.
	 * @see com.cristi.exemplu.ecoregraph.full_frame#getNumber_of_locals()
	 * @see #getfull_frame()
	 * @generated
	 */
	EAttribute getfull_frame_Number_of_locals();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.full_frame#getNumber_of_stack_items <em>Number of stack items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number of stack items</em>'.
	 * @see com.cristi.exemplu.ecoregraph.full_frame#getNumber_of_stack_items()
	 * @see #getfull_frame()
	 * @generated
	 */
	EAttribute getfull_frame_Number_of_stack_items();

	/**
	 * Returns the meta object for the containment reference list '{@link com.cristi.exemplu.ecoregraph.full_frame#getLocals <em>Locals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Locals</em>'.
	 * @see com.cristi.exemplu.ecoregraph.full_frame#getLocals()
	 * @see #getfull_frame()
	 * @generated
	 */
	EReference getfull_frame_Locals();

	/**
	 * Returns the meta object for the containment reference list '{@link com.cristi.exemplu.ecoregraph.full_frame#getStack <em>Stack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Stack</em>'.
	 * @see com.cristi.exemplu.ecoregraph.full_frame#getStack()
	 * @see #getfull_frame()
	 * @generated
	 */
	EReference getfull_frame_Stack();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.Exceptions_attribute <em>Exceptions attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exceptions attribute</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Exceptions_attribute
	 * @generated
	 */
	EClass getExceptions_attribute();

	/**
	 * Returns the meta object for the reference list '{@link com.cristi.exemplu.ecoregraph.Exceptions_attribute#getException_table <em>Exception table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Exception table</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Exceptions_attribute#getException_table()
	 * @see #getExceptions_attribute()
	 * @generated
	 */
	EReference getExceptions_attribute_Exception_table();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.Exceptions_attribute#getNumber_of_exceptions <em>Number of exceptions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number of exceptions</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Exceptions_attribute#getNumber_of_exceptions()
	 * @see #getExceptions_attribute()
	 * @generated
	 */
	EAttribute getExceptions_attribute_Number_of_exceptions();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.InterfaceCode_attribute <em>Interface Code attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface Code attribute</em>'.
	 * @see com.cristi.exemplu.ecoregraph.InterfaceCode_attribute
	 * @generated
	 */
	EClass getInterfaceCode_attribute();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.InnerClasses_attribute <em>Inner Classes attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inner Classes attribute</em>'.
	 * @see com.cristi.exemplu.ecoregraph.InnerClasses_attribute
	 * @generated
	 */
	EClass getInnerClasses_attribute();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.InnerClasses_attribute#getNumber_of_classes <em>Number of classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number of classes</em>'.
	 * @see com.cristi.exemplu.ecoregraph.InnerClasses_attribute#getNumber_of_classes()
	 * @see #getInnerClasses_attribute()
	 * @generated
	 */
	EAttribute getInnerClasses_attribute_Number_of_classes();

	/**
	 * Returns the meta object for the containment reference list '{@link com.cristi.exemplu.ecoregraph.InnerClasses_attribute#getClasses <em>Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Classes</em>'.
	 * @see com.cristi.exemplu.ecoregraph.InnerClasses_attribute#getClasses()
	 * @see #getInnerClasses_attribute()
	 * @generated
	 */
	EReference getInnerClasses_attribute_Classes();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.Class_member <em>Class member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class member</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Class_member
	 * @generated
	 */
	EClass getClass_member();

	/**
	 * Returns the meta object for the attribute '{@link com.cristi.exemplu.ecoregraph.Class_member#getInner_class_access_flags <em>Inner class access flags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Inner class access flags</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Class_member#getInner_class_access_flags()
	 * @see #getClass_member()
	 * @generated
	 */
	EAttribute getClass_member_Inner_class_access_flags();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.Class_member#getInner_class_info <em>Inner class info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Inner class info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Class_member#getInner_class_info()
	 * @see #getClass_member()
	 * @generated
	 */
	EReference getClass_member_Inner_class_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.Class_member#getOuter_class_info <em>Outer class info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Outer class info</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Class_member#getOuter_class_info()
	 * @see #getClass_member()
	 * @generated
	 */
	EReference getClass_member_Outer_class_info();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.Class_member#getInner_name <em>Inner name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Inner name</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Class_member#getInner_name()
	 * @see #getClass_member()
	 * @generated
	 */
	EReference getClass_member_Inner_name();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.Synthetic_attribute <em>Synthetic attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Synthetic attribute</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Synthetic_attribute
	 * @generated
	 */
	EClass getSynthetic_attribute();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.Signature_attribute <em>Signature attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signature attribute</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Signature_attribute
	 * @generated
	 */
	EClass getSignature_attribute();

	/**
	 * Returns the meta object for the reference '{@link com.cristi.exemplu.ecoregraph.Signature_attribute#getSignature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Signature</em>'.
	 * @see com.cristi.exemplu.ecoregraph.Signature_attribute#getSignature()
	 * @see #getSignature_attribute()
	 * @generated
	 */
	EReference getSignature_attribute_Signature();

	/**
	 * Returns the meta object for class '{@link com.cristi.exemplu.ecoregraph.NewEClass54 <em>New EClass54</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>New EClass54</em>'.
	 * @see com.cristi.exemplu.ecoregraph.NewEClass54
	 * @generated
	 */
	EClass getNewEClass54();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	EcoregraphFactory getEcoregraphFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_MethodHandle_InfoImpl <em>CONSTANT Method Handle Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_MethodHandle_InfoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_MethodHandle_Info()
		 * @generated
		 */
		EClass CONSTANT_METHOD_HANDLE_INFO = eINSTANCE.getCONSTANT_MethodHandle_Info();

		/**
		 * The meta object literal for the '<em><b>Reference kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT_METHOD_HANDLE_INFO__REFERENCE_KIND = eINSTANCE
				.getCONSTANT_MethodHandle_Info_Reference_kind();

		/**
		 * The meta object literal for the '<em><b>Constant ref info</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_METHOD_HANDLE_INFO__CONSTANT_REF_INFO = eINSTANCE
				.getCONSTANT_MethodHandle_Info_Constant_ref_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Fieldref_infoImpl <em>CONSTANT Fieldref info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Fieldref_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Fieldref_info()
		 * @generated
		 */
		EClass CONSTANT_FIELDREF_INFO = eINSTANCE.getCONSTANT_Fieldref_info();

		/**
		 * The meta object literal for the '<em><b>Constant class info</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_FIELDREF_INFO__CONSTANT_CLASS_INFO = eINSTANCE
				.getCONSTANT_Fieldref_info_Constant_class_info();

		/**
		 * The meta object literal for the '<em><b>Constant nameandtype info</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_FIELDREF_INFO__CONSTANT_NAMEANDTYPE_INFO = eINSTANCE
				.getCONSTANT_Fieldref_info_Constant_nameandtype_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Methodref_infoImpl <em>CONSTANT Methodref info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Methodref_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Methodref_info()
		 * @generated
		 */
		EClass CONSTANT_METHODREF_INFO = eINSTANCE.getCONSTANT_Methodref_info();

		/**
		 * The meta object literal for the '<em><b>Constant class info</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_METHODREF_INFO__CONSTANT_CLASS_INFO = eINSTANCE
				.getCONSTANT_Methodref_info_Constant_class_info();

		/**
		 * The meta object literal for the '<em><b>Constant nameandtype info</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_METHODREF_INFO__CONSTANT_NAMEANDTYPE_INFO = eINSTANCE
				.getCONSTANT_Methodref_info_Constant_nameandtype_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_InterfaceMethodref_infoImpl <em>CONSTANT Interface Methodref info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_InterfaceMethodref_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_InterfaceMethodref_info()
		 * @generated
		 */
		EClass CONSTANT_INTERFACE_METHODREF_INFO = eINSTANCE.getCONSTANT_InterfaceMethodref_info();

		/**
		 * The meta object literal for the '<em><b>Constant class info</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_INTERFACE_METHODREF_INFO__CONSTANT_CLASS_INFO = eINSTANCE
				.getCONSTANT_InterfaceMethodref_info_Constant_class_info();

		/**
		 * The meta object literal for the '<em><b>Constant nameandtype info</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_INTERFACE_METHODREF_INFO__CONSTANT_NAMEANDTYPE_INFO = eINSTANCE
				.getCONSTANT_InterfaceMethodref_info_Constant_nameandtype_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Class_infoImpl <em>CONSTANT Class info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Class_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Class_info()
		 * @generated
		 */
		EClass CONSTANT_CLASS_INFO = eINSTANCE.getCONSTANT_Class_info();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_CLASS_INFO__NAME = eINSTANCE.getCONSTANT_Class_info_Name();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_NameAndType_infoImpl <em>CONSTANT Name And Type info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_NameAndType_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_NameAndType_info()
		 * @generated
		 */
		EClass CONSTANT_NAME_AND_TYPE_INFO = eINSTANCE.getCONSTANT_NameAndType_info();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_NAME_AND_TYPE_INFO__NAME = eINSTANCE.getCONSTANT_NameAndType_info_Name();

		/**
		 * The meta object literal for the '<em><b>Field method descriptor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_NAME_AND_TYPE_INFO__FIELD_METHOD_DESCRIPTOR = eINSTANCE
				.getCONSTANT_NameAndType_info_Field_method_descriptor();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Utf8_infoImpl <em>CONSTANT Utf8 info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Utf8_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Utf8_info()
		 * @generated
		 */
		EClass CONSTANT_UTF8_INFO = eINSTANCE.getCONSTANT_Utf8_info();

		/**
		 * The meta object literal for the '<em><b>Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT_UTF8_INFO__LENGTH = eINSTANCE.getCONSTANT_Utf8_info_Length();

		/**
		 * The meta object literal for the '<em><b>Bytes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT_UTF8_INFO__BYTES = eINSTANCE.getCONSTANT_Utf8_info_Bytes();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_MethodType_infoImpl <em>CONSTANT Method Type info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_MethodType_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_MethodType_info()
		 * @generated
		 */
		EClass CONSTANT_METHOD_TYPE_INFO = eINSTANCE.getCONSTANT_MethodType_info();

		/**
		 * The meta object literal for the '<em><b>Method descriptor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_METHOD_TYPE_INFO__METHOD_DESCRIPTOR = eINSTANCE
				.getCONSTANT_MethodType_info_Method_descriptor();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_String_infoImpl <em>CONSTANT String info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_String_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_String_info()
		 * @generated
		 */
		EClass CONSTANT_STRING_INFO = eINSTANCE.getCONSTANT_String_info();

		/**
		 * The meta object literal for the '<em><b>Constant utf8 info</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_STRING_INFO__CONSTANT_UTF8_INFO = eINSTANCE.getCONSTANT_String_info_Constant_utf8_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_InvokeDynamic_infoImpl <em>CONSTANT Invoke Dynamic info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_InvokeDynamic_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_InvokeDynamic_info()
		 * @generated
		 */
		EClass CONSTANT_INVOKE_DYNAMIC_INFO = eINSTANCE.getCONSTANT_InvokeDynamic_info();

		/**
		 * The meta object literal for the '<em><b>Constant nameandtype info</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_INVOKE_DYNAMIC_INFO__CONSTANT_NAMEANDTYPE_INFO = eINSTANCE
				.getCONSTANT_InvokeDynamic_info_Constant_nameandtype_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.Field_infoImpl <em>Field info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.Field_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getField_info()
		 * @generated
		 */
		EClass FIELD_INFO = eINSTANCE.getField_info();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIELD_INFO__NAME = eINSTANCE.getField_info_Name();

		/**
		 * The meta object literal for the '<em><b>Field descriptor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIELD_INFO__FIELD_DESCRIPTOR = eINSTANCE.getField_info_Field_descriptor();

		/**
		 * The meta object literal for the '<em><b>Access flags</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_INFO__ACCESS_FLAGS = eINSTANCE.getField_info_Access_flags();

		/**
		 * The meta object literal for the '<em><b>Attributes count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD_INFO__ATTRIBUTES_COUNT = eINSTANCE.getField_info_Attributes_count();

		/**
		 * The meta object literal for the '<em><b>Interfacefield attribute info</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIELD_INFO__INTERFACEFIELD_ATTRIBUTE_INFO = eINSTANCE.getField_info_Interfacefield_attribute_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.Method_infoImpl <em>Method info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.Method_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getMethod_info()
		 * @generated
		 */
		EClass METHOD_INFO = eINSTANCE.getMethod_info();

		/**
		 * The meta object literal for the '<em><b>Access flags</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute METHOD_INFO__ACCESS_FLAGS = eINSTANCE.getMethod_info_Access_flags();

		/**
		 * The meta object literal for the '<em><b>Attributes count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute METHOD_INFO__ATTRIBUTES_COUNT = eINSTANCE.getMethod_info_Attributes_count();

		/**
		 * The meta object literal for the '<em><b>Method descriptor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METHOD_INFO__METHOD_DESCRIPTOR = eINSTANCE.getMethod_info_Method_descriptor();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METHOD_INFO__NAME = eINSTANCE.getMethod_info_Name();

		/**
		 * The meta object literal for the '<em><b>Interfacemethod attribute info</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METHOD_INFO__INTERFACEMETHOD_ATTRIBUTE_INFO = eINSTANCE
				.getMethod_info_Interfacemethod_attribute_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.AbstractCp_infoImpl <em>Abstract Cp info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.AbstractCp_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getAbstractCp_info()
		 * @generated
		 */
		EClass ABSTRACT_CP_INFO = eINSTANCE.getAbstractCp_info();

		/**
		 * The meta object literal for the '<em><b>Tag</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_CP_INFO__TAG = eINSTANCE.getAbstractCp_info_Tag();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Ref_info <em>Interface CONSTANT Ref info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Ref_info
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInterfaceCONSTANT_Ref_info()
		 * @generated
		 */
		EClass INTERFACE_CONSTANT_REF_INFO = eINSTANCE.getInterfaceCONSTANT_Ref_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Integer_infoImpl <em>CONSTANT Integer info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Integer_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Integer_info()
		 * @generated
		 */
		EClass CONSTANT_INTEGER_INFO = eINSTANCE.getCONSTANT_Integer_info();

		/**
		 * The meta object literal for the '<em><b>Bytes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT_INTEGER_INFO__BYTES = eINSTANCE.getCONSTANT_Integer_info_Bytes();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Float_infoImpl <em>CONSTANT Float info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Float_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Float_info()
		 * @generated
		 */
		EClass CONSTANT_FLOAT_INFO = eINSTANCE.getCONSTANT_Float_info();

		/**
		 * The meta object literal for the '<em><b>Bytes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT_FLOAT_INFO__BYTES = eINSTANCE.getCONSTANT_Float_info_Bytes();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Long_infoImpl <em>CONSTANT Long info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Long_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Long_info()
		 * @generated
		 */
		EClass CONSTANT_LONG_INFO = eINSTANCE.getCONSTANT_Long_info();

		/**
		 * The meta object literal for the '<em><b>High bytes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT_LONG_INFO__HIGH_BYTES = eINSTANCE.getCONSTANT_Long_info_High_bytes();

		/**
		 * The meta object literal for the '<em><b>Low bytes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT_LONG_INFO__LOW_BYTES = eINSTANCE.getCONSTANT_Long_info_Low_bytes();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.CONSTANT_Double_infoImpl <em>CONSTANT Double info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.CONSTANT_Double_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCONSTANT_Double_info()
		 * @generated
		 */
		EClass CONSTANT_DOUBLE_INFO = eINSTANCE.getCONSTANT_Double_info();

		/**
		 * The meta object literal for the '<em><b>High bytes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT_DOUBLE_INFO__HIGH_BYTES = eINSTANCE.getCONSTANT_Double_info_High_bytes();

		/**
		 * The meta object literal for the '<em><b>Low bytes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT_DOUBLE_INFO__LOW_BYTES = eINSTANCE.getCONSTANT_Double_info_Low_bytes();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.ClassFileImpl <em>Class File</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.ClassFileImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getClassFile()
		 * @generated
		 */
		EClass CLASS_FILE = eINSTANCE.getClassFile();

		/**
		 * The meta object literal for the '<em><b>Magic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_FILE__MAGIC = eINSTANCE.getClassFile_Magic();

		/**
		 * The meta object literal for the '<em><b>Minor version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_FILE__MINOR_VERSION = eINSTANCE.getClassFile_Minor_version();

		/**
		 * The meta object literal for the '<em><b>Major version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_FILE__MAJOR_VERSION = eINSTANCE.getClassFile_Major_version();

		/**
		 * The meta object literal for the '<em><b>Constant pool count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_FILE__CONSTANT_POOL_COUNT = eINSTANCE.getClassFile_Constant_pool_count();

		/**
		 * The meta object literal for the '<em><b>Access flags</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_FILE__ACCESS_FLAGS = eINSTANCE.getClassFile_Access_flags();

		/**
		 * The meta object literal for the '<em><b>Interfaces count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_FILE__INTERFACES_COUNT = eINSTANCE.getClassFile_Interfaces_count();

		/**
		 * The meta object literal for the '<em><b>Fields count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_FILE__FIELDS_COUNT = eINSTANCE.getClassFile_Fields_count();

		/**
		 * The meta object literal for the '<em><b>Methods count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_FILE__METHODS_COUNT = eINSTANCE.getClassFile_Methods_count();

		/**
		 * The meta object literal for the '<em><b>Attributes count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_FILE__ATTRIBUTES_COUNT = eINSTANCE.getClassFile_Attributes_count();

		/**
		 * The meta object literal for the '<em><b>Cp info</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_FILE__CP_INFO = eINSTANCE.getClassFile_Cp_info();

		/**
		 * The meta object literal for the '<em><b>Field info</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_FILE__FIELD_INFO = eINSTANCE.getClassFile_Field_info();

		/**
		 * The meta object literal for the '<em><b>Method info</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_FILE__METHOD_INFO = eINSTANCE.getClassFile_Method_info();

		/**
		 * The meta object literal for the '<em><b>Super class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_FILE__SUPER_CLASS = eINSTANCE.getClassFile_Super_class();

		/**
		 * The meta object literal for the '<em><b>Interfaces</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_FILE__INTERFACES = eINSTANCE.getClassFile_Interfaces();

		/**
		 * The meta object literal for the '<em><b>This class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_FILE__THIS_CLASS = eINSTANCE.getClassFile_This_class();

		/**
		 * The meta object literal for the '<em><b>Interfaceclass attribute info</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_FILE__INTERFACECLASS_ATTRIBUTE_INFO = eINSTANCE.getClassFile_Interfaceclass_attribute_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.AbstractAttribute_infoImpl <em>Abstract Attribute info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.AbstractAttribute_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getAbstractAttribute_info()
		 * @generated
		 */
		EClass ABSTRACT_ATTRIBUTE_INFO = eINSTANCE.getAbstractAttribute_info();

		/**
		 * The meta object literal for the '<em><b>Attribute length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_LENGTH = eINSTANCE.getAbstractAttribute_info_Attribute_length();

		/**
		 * The meta object literal for the '<em><b>Attribute name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_ATTRIBUTE_INFO__ATTRIBUTE_NAME = eINSTANCE.getAbstractAttribute_info_Attribute_name();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.InterfaceMethod_attribute_info <em>Interface Method attribute info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.InterfaceMethod_attribute_info
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInterfaceMethod_attribute_info()
		 * @generated
		 */
		EClass INTERFACE_METHOD_ATTRIBUTE_INFO = eINSTANCE.getInterfaceMethod_attribute_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.InterfaceField_attribute_info <em>Interface Field attribute info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.InterfaceField_attribute_info
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInterfaceField_attribute_info()
		 * @generated
		 */
		EClass INTERFACE_FIELD_ATTRIBUTE_INFO = eINSTANCE.getInterfaceField_attribute_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.InterfaceClass_attribute_info <em>Interface Class attribute info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.InterfaceClass_attribute_info
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInterfaceClass_attribute_info()
		 * @generated
		 */
		EClass INTERFACE_CLASS_ATTRIBUTE_INFO = eINSTANCE.getInterfaceClass_attribute_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.ConstantValue_attributeImpl <em>Constant Value attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.ConstantValue_attributeImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getConstantValue_attribute()
		 * @generated
		 */
		EClass CONSTANT_VALUE_ATTRIBUTE = eINSTANCE.getConstantValue_attribute();

		/**
		 * The meta object literal for the '<em><b>Interfaceconstant value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTANT_VALUE_ATTRIBUTE__INTERFACECONSTANT_VALUE = eINSTANCE
				.getConstantValue_attribute_Interfaceconstant_value();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Value <em>Interface CONSTANT Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.InterfaceCONSTANT_Value
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInterfaceCONSTANT_Value()
		 * @generated
		 */
		EClass INTERFACE_CONSTANT_VALUE = eINSTANCE.getInterfaceCONSTANT_Value();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.Code_attributeImpl <em>Code attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.Code_attributeImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getCode_attribute()
		 * @generated
		 */
		EClass CODE_ATTRIBUTE = eINSTANCE.getCode_attribute();

		/**
		 * The meta object literal for the '<em><b>Max stack</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_ATTRIBUTE__MAX_STACK = eINSTANCE.getCode_attribute_Max_stack();

		/**
		 * The meta object literal for the '<em><b>Max locals</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_ATTRIBUTE__MAX_LOCALS = eINSTANCE.getCode_attribute_Max_locals();

		/**
		 * The meta object literal for the '<em><b>Code length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_ATTRIBUTE__CODE_LENGTH = eINSTANCE.getCode_attribute_Code_length();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_ATTRIBUTE__CODE = eINSTANCE.getCode_attribute_Code();

		/**
		 * The meta object literal for the '<em><b>Exception table</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_ATTRIBUTE__EXCEPTION_TABLE = eINSTANCE.getCode_attribute_Exception_table();

		/**
		 * The meta object literal for the '<em><b>Exception table length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_ATTRIBUTE__EXCEPTION_TABLE_LENGTH = eINSTANCE.getCode_attribute_Exception_table_length();

		/**
		 * The meta object literal for the '<em><b>Attributes count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_ATTRIBUTE__ATTRIBUTES_COUNT = eINSTANCE.getCode_attribute_Attributes_count();

		/**
		 * The meta object literal for the '<em><b>Interfacecode attribute</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_ATTRIBUTE__INTERFACECODE_ATTRIBUTE = eINSTANCE.getCode_attribute_Interfacecode_attribute();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.exception_table_entryImpl <em>exception table entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.exception_table_entryImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getexception_table_entry()
		 * @generated
		 */
		EClass EXCEPTION_TABLE_ENTRY = eINSTANCE.getexception_table_entry();

		/**
		 * The meta object literal for the '<em><b>Start pc</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXCEPTION_TABLE_ENTRY__START_PC = eINSTANCE.getexception_table_entry_Start_pc();

		/**
		 * The meta object literal for the '<em><b>End pc</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXCEPTION_TABLE_ENTRY__END_PC = eINSTANCE.getexception_table_entry_End_pc();

		/**
		 * The meta object literal for the '<em><b>Handler pc</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXCEPTION_TABLE_ENTRY__HANDLER_PC = eINSTANCE.getexception_table_entry_Handler_pc();

		/**
		 * The meta object literal for the '<em><b>Catch type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXCEPTION_TABLE_ENTRY__CATCH_TYPE = eINSTANCE.getexception_table_entry_Catch_type();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.StackMapTable_attributeImpl <em>Stack Map Table attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.StackMapTable_attributeImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getStackMapTable_attribute()
		 * @generated
		 */
		EClass STACK_MAP_TABLE_ATTRIBUTE = eINSTANCE.getStackMapTable_attribute();

		/**
		 * The meta object literal for the '<em><b>Number of entries</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STACK_MAP_TABLE_ATTRIBUTE__NUMBER_OF_ENTRIES = eINSTANCE
				.getStackMapTable_attribute_Number_of_entries();

		/**
		 * The meta object literal for the '<em><b>Abstractstack map frame</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STACK_MAP_TABLE_ATTRIBUTE__ABSTRACTSTACK_MAP_FRAME = eINSTANCE
				.getStackMapTable_attribute_Abstractstack_map_frame();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.AbstractVerification_type_infoImpl <em>Abstract Verification type info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.AbstractVerification_type_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getAbstractVerification_type_info()
		 * @generated
		 */
		EClass ABSTRACT_VERIFICATION_TYPE_INFO = eINSTANCE.getAbstractVerification_type_info();

		/**
		 * The meta object literal for the '<em><b>Tag</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_VERIFICATION_TYPE_INFO__TAG = eINSTANCE.getAbstractVerification_type_info_Tag();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.Top_variable_infoImpl <em>Top variable info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.Top_variable_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getTop_variable_info()
		 * @generated
		 */
		EClass TOP_VARIABLE_INFO = eINSTANCE.getTop_variable_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.Integer_variable_infoImpl <em>Integer variable info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.Integer_variable_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInteger_variable_info()
		 * @generated
		 */
		EClass INTEGER_VARIABLE_INFO = eINSTANCE.getInteger_variable_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.Float_variable_infoImpl <em>Float variable info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.Float_variable_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getFloat_variable_info()
		 * @generated
		 */
		EClass FLOAT_VARIABLE_INFO = eINSTANCE.getFloat_variable_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.Null_variable_infoImpl <em>Null variable info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.Null_variable_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getNull_variable_info()
		 * @generated
		 */
		EClass NULL_VARIABLE_INFO = eINSTANCE.getNull_variable_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.UninitializedThis_variable_infoImpl <em>Uninitialized This variable info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.UninitializedThis_variable_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getUninitializedThis_variable_info()
		 * @generated
		 */
		EClass UNINITIALIZED_THIS_VARIABLE_INFO = eINSTANCE.getUninitializedThis_variable_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.Object_variable_infoImpl <em>Object variable info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.Object_variable_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getObject_variable_info()
		 * @generated
		 */
		EClass OBJECT_VARIABLE_INFO = eINSTANCE.getObject_variable_info();

		/**
		 * The meta object literal for the '<em><b>Constant class info</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_VARIABLE_INFO__CONSTANT_CLASS_INFO = eINSTANCE.getObject_variable_info_Constant_class_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.NewEClass36Impl <em>New EClass36</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.NewEClass36Impl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getNewEClass36()
		 * @generated
		 */
		EClass NEW_ECLASS36 = eINSTANCE.getNewEClass36();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.Uninitialized_variable_infoImpl <em>Uninitialized variable info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.Uninitialized_variable_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getUninitialized_variable_info()
		 * @generated
		 */
		EClass UNINITIALIZED_VARIABLE_INFO = eINSTANCE.getUninitialized_variable_info();

		/**
		 * The meta object literal for the '<em><b>Offset</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNINITIALIZED_VARIABLE_INFO__OFFSET = eINSTANCE.getUninitialized_variable_info_Offset();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.Long_variable_infoImpl <em>Long variable info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.Long_variable_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getLong_variable_info()
		 * @generated
		 */
		EClass LONG_VARIABLE_INFO = eINSTANCE.getLong_variable_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.Double_variable_infoImpl <em>Double variable info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.Double_variable_infoImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getDouble_variable_info()
		 * @generated
		 */
		EClass DOUBLE_VARIABLE_INFO = eINSTANCE.getDouble_variable_info();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.AbstractStack_map_frameImpl <em>Abstract Stack map frame</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.AbstractStack_map_frameImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getAbstractStack_map_frame()
		 * @generated
		 */
		EClass ABSTRACT_STACK_MAP_FRAME = eINSTANCE.getAbstractStack_map_frame();

		/**
		 * The meta object literal for the '<em><b>Frame type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_STACK_MAP_FRAME__FRAME_TYPE = eINSTANCE.getAbstractStack_map_frame_Frame_type();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.same_frameImpl <em>same frame</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.same_frameImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getsame_frame()
		 * @generated
		 */
		EClass SAME_FRAME = eINSTANCE.getsame_frame();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.same_locals_1_stack_item_frameImpl <em>same locals 1stack item frame</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.same_locals_1_stack_item_frameImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getsame_locals_1_stack_item_frame()
		 * @generated
		 */
		EClass SAME_LOCALS_1STACK_ITEM_FRAME = eINSTANCE.getsame_locals_1_stack_item_frame();

		/**
		 * The meta object literal for the '<em><b>Stack</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SAME_LOCALS_1STACK_ITEM_FRAME__STACK = eINSTANCE.getsame_locals_1_stack_item_frame_Stack();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.same_locals_1_stack_item_frame_extendedImpl <em>same locals 1stack item frame extended</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.same_locals_1_stack_item_frame_extendedImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getsame_locals_1_stack_item_frame_extended()
		 * @generated
		 */
		EClass SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED = eINSTANCE.getsame_locals_1_stack_item_frame_extended();

		/**
		 * The meta object literal for the '<em><b>Offset delta</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__OFFSET_DELTA = eINSTANCE
				.getsame_locals_1_stack_item_frame_extended_Offset_delta();

		/**
		 * The meta object literal for the '<em><b>Stack</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SAME_LOCALS_1STACK_ITEM_FRAME_EXTENDED__STACK = eINSTANCE
				.getsame_locals_1_stack_item_frame_extended_Stack();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.same_frame_extendedImpl <em>same frame extended</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.same_frame_extendedImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getsame_frame_extended()
		 * @generated
		 */
		EClass SAME_FRAME_EXTENDED = eINSTANCE.getsame_frame_extended();

		/**
		 * The meta object literal for the '<em><b>Offset delta</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SAME_FRAME_EXTENDED__OFFSET_DELTA = eINSTANCE.getsame_frame_extended_Offset_delta();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.chop_frameImpl <em>chop frame</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.chop_frameImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getchop_frame()
		 * @generated
		 */
		EClass CHOP_FRAME = eINSTANCE.getchop_frame();

		/**
		 * The meta object literal for the '<em><b>Offset delta</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHOP_FRAME__OFFSET_DELTA = eINSTANCE.getchop_frame_Offset_delta();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.append_frameImpl <em>append frame</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.append_frameImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getappend_frame()
		 * @generated
		 */
		EClass APPEND_FRAME = eINSTANCE.getappend_frame();

		/**
		 * The meta object literal for the '<em><b>Offset delta</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APPEND_FRAME__OFFSET_DELTA = eINSTANCE.getappend_frame_Offset_delta();

		/**
		 * The meta object literal for the '<em><b>Locals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPEND_FRAME__LOCALS = eINSTANCE.getappend_frame_Locals();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.full_frameImpl <em>full frame</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.full_frameImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getfull_frame()
		 * @generated
		 */
		EClass FULL_FRAME = eINSTANCE.getfull_frame();

		/**
		 * The meta object literal for the '<em><b>Offset delta</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FULL_FRAME__OFFSET_DELTA = eINSTANCE.getfull_frame_Offset_delta();

		/**
		 * The meta object literal for the '<em><b>Number of locals</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FULL_FRAME__NUMBER_OF_LOCALS = eINSTANCE.getfull_frame_Number_of_locals();

		/**
		 * The meta object literal for the '<em><b>Number of stack items</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FULL_FRAME__NUMBER_OF_STACK_ITEMS = eINSTANCE.getfull_frame_Number_of_stack_items();

		/**
		 * The meta object literal for the '<em><b>Locals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FULL_FRAME__LOCALS = eINSTANCE.getfull_frame_Locals();

		/**
		 * The meta object literal for the '<em><b>Stack</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FULL_FRAME__STACK = eINSTANCE.getfull_frame_Stack();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.Exceptions_attributeImpl <em>Exceptions attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.Exceptions_attributeImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getExceptions_attribute()
		 * @generated
		 */
		EClass EXCEPTIONS_ATTRIBUTE = eINSTANCE.getExceptions_attribute();

		/**
		 * The meta object literal for the '<em><b>Exception table</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXCEPTIONS_ATTRIBUTE__EXCEPTION_TABLE = eINSTANCE.getExceptions_attribute_Exception_table();

		/**
		 * The meta object literal for the '<em><b>Number of exceptions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXCEPTIONS_ATTRIBUTE__NUMBER_OF_EXCEPTIONS = eINSTANCE
				.getExceptions_attribute_Number_of_exceptions();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.InterfaceCode_attribute <em>Interface Code attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.InterfaceCode_attribute
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInterfaceCode_attribute()
		 * @generated
		 */
		EClass INTERFACE_CODE_ATTRIBUTE = eINSTANCE.getInterfaceCode_attribute();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.InnerClasses_attributeImpl <em>Inner Classes attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.InnerClasses_attributeImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getInnerClasses_attribute()
		 * @generated
		 */
		EClass INNER_CLASSES_ATTRIBUTE = eINSTANCE.getInnerClasses_attribute();

		/**
		 * The meta object literal for the '<em><b>Number of classes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INNER_CLASSES_ATTRIBUTE__NUMBER_OF_CLASSES = eINSTANCE.getInnerClasses_attribute_Number_of_classes();

		/**
		 * The meta object literal for the '<em><b>Classes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INNER_CLASSES_ATTRIBUTE__CLASSES = eINSTANCE.getInnerClasses_attribute_Classes();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.Class_memberImpl <em>Class member</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.Class_memberImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getClass_member()
		 * @generated
		 */
		EClass CLASS_MEMBER = eINSTANCE.getClass_member();

		/**
		 * The meta object literal for the '<em><b>Inner class access flags</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_MEMBER__INNER_CLASS_ACCESS_FLAGS = eINSTANCE.getClass_member_Inner_class_access_flags();

		/**
		 * The meta object literal for the '<em><b>Inner class info</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_MEMBER__INNER_CLASS_INFO = eINSTANCE.getClass_member_Inner_class_info();

		/**
		 * The meta object literal for the '<em><b>Outer class info</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_MEMBER__OUTER_CLASS_INFO = eINSTANCE.getClass_member_Outer_class_info();

		/**
		 * The meta object literal for the '<em><b>Inner name</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_MEMBER__INNER_NAME = eINSTANCE.getClass_member_Inner_name();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.Synthetic_attributeImpl <em>Synthetic attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.Synthetic_attributeImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getSynthetic_attribute()
		 * @generated
		 */
		EClass SYNTHETIC_ATTRIBUTE = eINSTANCE.getSynthetic_attribute();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.Signature_attributeImpl <em>Signature attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.Signature_attributeImpl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getSignature_attribute()
		 * @generated
		 */
		EClass SIGNATURE_ATTRIBUTE = eINSTANCE.getSignature_attribute();

		/**
		 * The meta object literal for the '<em><b>Signature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNATURE_ATTRIBUTE__SIGNATURE = eINSTANCE.getSignature_attribute_Signature();

		/**
		 * The meta object literal for the '{@link com.cristi.exemplu.ecoregraph.impl.NewEClass54Impl <em>New EClass54</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.cristi.exemplu.ecoregraph.impl.NewEClass54Impl
		 * @see com.cristi.exemplu.ecoregraph.impl.EcoregraphPackageImpl#getNewEClass54()
		 * @generated
		 */
		EClass NEW_ECLASS54 = eINSTANCE.getNewEClass54();

	}

} //EcoregraphPackage
