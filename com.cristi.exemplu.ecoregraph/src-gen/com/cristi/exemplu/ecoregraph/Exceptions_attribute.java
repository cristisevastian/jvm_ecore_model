/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exceptions attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Exceptions_attribute#getException_table <em>Exception table</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Exceptions_attribute#getNumber_of_exceptions <em>Number of exceptions</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getExceptions_attribute()
 * @model
 * @generated
 */
public interface Exceptions_attribute extends InterfaceMethod_attribute_info, AbstractAttribute_info {
	/**
	 * Returns the value of the '<em><b>Exception table</b></em>' reference list.
	 * The list contents are of type {@link com.cristi.exemplu.ecoregraph.CONSTANT_Class_info}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception table</em>' reference list.
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getExceptions_attribute_Exception_table()
	 * @model
	 * @generated
	 */
	EList<CONSTANT_Class_info> getException_table();

	/**
	 * Returns the value of the '<em><b>Number of exceptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number of exceptions</em>' attribute.
	 * @see #setNumber_of_exceptions(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getExceptions_attribute_Number_of_exceptions()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getNumber_of_exceptions();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Exceptions_attribute#getNumber_of_exceptions <em>Number of exceptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number of exceptions</em>' attribute.
	 * @see #getNumber_of_exceptions()
	 * @generated
	 */
	void setNumber_of_exceptions(int value);

} // Exceptions_attribute
