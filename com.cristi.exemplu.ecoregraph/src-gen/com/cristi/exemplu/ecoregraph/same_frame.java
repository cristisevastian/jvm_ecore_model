/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>same frame</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getsame_frame()
 * @model
 * @generated
 */
public interface same_frame extends AbstractStack_map_frame {
} // same_frame
