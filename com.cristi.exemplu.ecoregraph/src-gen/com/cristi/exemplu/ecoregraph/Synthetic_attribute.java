/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Synthetic attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getSynthetic_attribute()
 * @model
 * @generated
 */
public interface Synthetic_attribute extends AbstractAttribute_info, InterfaceClass_attribute_info,
		InterfaceField_attribute_info, InterfaceMethod_attribute_info {
} // Synthetic_attribute
