/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Top variable info</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getTop_variable_info()
 * @model
 * @generated
 */
public interface Top_variable_info extends AbstractVerification_type_info {
} // Top_variable_info
