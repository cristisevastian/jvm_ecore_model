/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer variable info</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getInteger_variable_info()
 * @model
 * @generated
 */
public interface Integer_variable_info extends AbstractVerification_type_info {
} // Integer_variable_info
