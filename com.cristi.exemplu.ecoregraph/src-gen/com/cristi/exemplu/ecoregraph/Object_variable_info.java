/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object variable info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.Object_variable_info#getConstant_class_info <em>Constant class info</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getObject_variable_info()
 * @model
 * @generated
 */
public interface Object_variable_info extends AbstractVerification_type_info {
	/**
	 * Returns the value of the '<em><b>Constant class info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant class info</em>' reference.
	 * @see #setConstant_class_info(CONSTANT_Class_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getObject_variable_info_Constant_class_info()
	 * @model
	 * @generated
	 */
	CONSTANT_Class_info getConstant_class_info();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.Object_variable_info#getConstant_class_info <em>Constant class info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant class info</em>' reference.
	 * @see #getConstant_class_info()
	 * @generated
	 */
	void setConstant_class_info(CONSTANT_Class_info value);

} // Object_variable_info
