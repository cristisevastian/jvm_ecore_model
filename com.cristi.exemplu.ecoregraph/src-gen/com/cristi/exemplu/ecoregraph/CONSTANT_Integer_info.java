/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CONSTANT Integer info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.CONSTANT_Integer_info#getBytes <em>Bytes</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_Integer_info()
 * @model
 * @generated
 */
public interface CONSTANT_Integer_info extends AbstractCp_info, InterfaceCONSTANT_Value {
	/**
	 * Returns the value of the '<em><b>Bytes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bytes</em>' attribute.
	 * @see #setBytes(long)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getCONSTANT_Integer_info_Bytes()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 * @generated
	 */
	long getBytes();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.CONSTANT_Integer_info#getBytes <em>Bytes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bytes</em>' attribute.
	 * @see #getBytes()
	 * @generated
	 */
	void setBytes(long value);

} // CONSTANT_Integer_info
