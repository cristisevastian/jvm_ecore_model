/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>full frame</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.full_frame#getOffset_delta <em>Offset delta</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.full_frame#getNumber_of_locals <em>Number of locals</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.full_frame#getNumber_of_stack_items <em>Number of stack items</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.full_frame#getLocals <em>Locals</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.full_frame#getStack <em>Stack</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getfull_frame()
 * @model
 * @generated
 */
public interface full_frame extends AbstractStack_map_frame {
	/**
	 * Returns the value of the '<em><b>Offset delta</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset delta</em>' attribute.
	 * @see #setOffset_delta(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getfull_frame_Offset_delta()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getOffset_delta();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.full_frame#getOffset_delta <em>Offset delta</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Offset delta</em>' attribute.
	 * @see #getOffset_delta()
	 * @generated
	 */
	void setOffset_delta(int value);

	/**
	 * Returns the value of the '<em><b>Number of locals</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number of locals</em>' attribute.
	 * @see #setNumber_of_locals(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getfull_frame_Number_of_locals()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getNumber_of_locals();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.full_frame#getNumber_of_locals <em>Number of locals</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number of locals</em>' attribute.
	 * @see #getNumber_of_locals()
	 * @generated
	 */
	void setNumber_of_locals(int value);

	/**
	 * Returns the value of the '<em><b>Number of stack items</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number of stack items</em>' attribute.
	 * @see #setNumber_of_stack_items(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getfull_frame_Number_of_stack_items()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getNumber_of_stack_items();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.full_frame#getNumber_of_stack_items <em>Number of stack items</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number of stack items</em>' attribute.
	 * @see #getNumber_of_stack_items()
	 * @generated
	 */
	void setNumber_of_stack_items(int value);

	/**
	 * Returns the value of the '<em><b>Locals</b></em>' containment reference list.
	 * The list contents are of type {@link com.cristi.exemplu.ecoregraph.AbstractVerification_type_info}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locals</em>' containment reference list.
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getfull_frame_Locals()
	 * @model containment="true"
	 * @generated
	 */
	EList<AbstractVerification_type_info> getLocals();

	/**
	 * Returns the value of the '<em><b>Stack</b></em>' containment reference list.
	 * The list contents are of type {@link com.cristi.exemplu.ecoregraph.AbstractVerification_type_info}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stack</em>' containment reference list.
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getfull_frame_Stack()
	 * @model containment="true"
	 * @generated
	 */
	EList<AbstractVerification_type_info> getStack();

} // full_frame
