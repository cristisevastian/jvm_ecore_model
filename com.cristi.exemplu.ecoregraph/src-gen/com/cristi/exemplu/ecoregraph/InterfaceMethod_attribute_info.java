/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface Method attribute info</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getInterfaceMethod_attribute_info()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface InterfaceMethod_attribute_info extends EObject {
} // InterfaceMethod_attribute_info
