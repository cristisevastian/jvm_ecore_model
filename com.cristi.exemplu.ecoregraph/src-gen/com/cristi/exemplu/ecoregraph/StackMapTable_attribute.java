/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stack Map Table attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.StackMapTable_attribute#getNumber_of_entries <em>Number of entries</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.StackMapTable_attribute#getAbstractstack_map_frame <em>Abstractstack map frame</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getStackMapTable_attribute()
 * @model
 * @generated
 */
public interface StackMapTable_attribute extends AbstractAttribute_info, InterfaceCode_attribute {
	/**
	 * Returns the value of the '<em><b>Number of entries</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number of entries</em>' attribute.
	 * @see #setNumber_of_entries(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getStackMapTable_attribute_Number_of_entries()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getNumber_of_entries();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.StackMapTable_attribute#getNumber_of_entries <em>Number of entries</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number of entries</em>' attribute.
	 * @see #getNumber_of_entries()
	 * @generated
	 */
	void setNumber_of_entries(int value);

	/**
	 * Returns the value of the '<em><b>Abstractstack map frame</b></em>' containment reference list.
	 * The list contents are of type {@link com.cristi.exemplu.ecoregraph.AbstractStack_map_frame}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstractstack map frame</em>' containment reference list.
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getStackMapTable_attribute_Abstractstack_map_frame()
	 * @model containment="true"
	 * @generated
	 */
	EList<AbstractStack_map_frame> getAbstractstack_map_frame();

} // StackMapTable_attribute
