/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Inner Classes attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.InnerClasses_attribute#getNumber_of_classes <em>Number of classes</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.InnerClasses_attribute#getClasses <em>Classes</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getInnerClasses_attribute()
 * @model
 * @generated
 */
public interface InnerClasses_attribute extends InterfaceClass_attribute_info {
	/**
	 * Returns the value of the '<em><b>Number of classes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number of classes</em>' attribute.
	 * @see #setNumber_of_classes(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getInnerClasses_attribute_Number_of_classes()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getNumber_of_classes();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.InnerClasses_attribute#getNumber_of_classes <em>Number of classes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number of classes</em>' attribute.
	 * @see #getNumber_of_classes()
	 * @generated
	 */
	void setNumber_of_classes(int value);

	/**
	 * Returns the value of the '<em><b>Classes</b></em>' containment reference list.
	 * The list contents are of type {@link com.cristi.exemplu.ecoregraph.Class_member}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classes</em>' containment reference list.
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getInnerClasses_attribute_Classes()
	 * @model containment="true"
	 * @generated
	 */
	EList<Class_member> getClasses();

} // InnerClasses_attribute
