/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Null variable info</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getNull_variable_info()
 * @model
 * @generated
 */
public interface Null_variable_info extends AbstractVerification_type_info {
} // Null_variable_info
