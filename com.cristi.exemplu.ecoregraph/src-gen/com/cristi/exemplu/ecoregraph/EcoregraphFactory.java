/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage
 * @generated
 */
public interface EcoregraphFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EcoregraphFactory eINSTANCE = com.cristi.exemplu.ecoregraph.impl.EcoregraphFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>CONSTANT Method Handle Info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CONSTANT Method Handle Info</em>'.
	 * @generated
	 */
	CONSTANT_MethodHandle_Info createCONSTANT_MethodHandle_Info();

	/**
	 * Returns a new object of class '<em>CONSTANT Fieldref info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CONSTANT Fieldref info</em>'.
	 * @generated
	 */
	CONSTANT_Fieldref_info createCONSTANT_Fieldref_info();

	/**
	 * Returns a new object of class '<em>CONSTANT Methodref info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CONSTANT Methodref info</em>'.
	 * @generated
	 */
	CONSTANT_Methodref_info createCONSTANT_Methodref_info();

	/**
	 * Returns a new object of class '<em>CONSTANT Interface Methodref info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CONSTANT Interface Methodref info</em>'.
	 * @generated
	 */
	CONSTANT_InterfaceMethodref_info createCONSTANT_InterfaceMethodref_info();

	/**
	 * Returns a new object of class '<em>CONSTANT Class info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CONSTANT Class info</em>'.
	 * @generated
	 */
	CONSTANT_Class_info createCONSTANT_Class_info();

	/**
	 * Returns a new object of class '<em>CONSTANT Name And Type info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CONSTANT Name And Type info</em>'.
	 * @generated
	 */
	CONSTANT_NameAndType_info createCONSTANT_NameAndType_info();

	/**
	 * Returns a new object of class '<em>CONSTANT Utf8 info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CONSTANT Utf8 info</em>'.
	 * @generated
	 */
	CONSTANT_Utf8_info createCONSTANT_Utf8_info();

	/**
	 * Returns a new object of class '<em>CONSTANT Method Type info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CONSTANT Method Type info</em>'.
	 * @generated
	 */
	CONSTANT_MethodType_info createCONSTANT_MethodType_info();

	/**
	 * Returns a new object of class '<em>CONSTANT String info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CONSTANT String info</em>'.
	 * @generated
	 */
	CONSTANT_String_info createCONSTANT_String_info();

	/**
	 * Returns a new object of class '<em>CONSTANT Invoke Dynamic info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CONSTANT Invoke Dynamic info</em>'.
	 * @generated
	 */
	CONSTANT_InvokeDynamic_info createCONSTANT_InvokeDynamic_info();

	/**
	 * Returns a new object of class '<em>Field info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Field info</em>'.
	 * @generated
	 */
	Field_info createField_info();

	/**
	 * Returns a new object of class '<em>Method info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Method info</em>'.
	 * @generated
	 */
	Method_info createMethod_info();

	/**
	 * Returns a new object of class '<em>CONSTANT Integer info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CONSTANT Integer info</em>'.
	 * @generated
	 */
	CONSTANT_Integer_info createCONSTANT_Integer_info();

	/**
	 * Returns a new object of class '<em>CONSTANT Float info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CONSTANT Float info</em>'.
	 * @generated
	 */
	CONSTANT_Float_info createCONSTANT_Float_info();

	/**
	 * Returns a new object of class '<em>CONSTANT Long info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CONSTANT Long info</em>'.
	 * @generated
	 */
	CONSTANT_Long_info createCONSTANT_Long_info();

	/**
	 * Returns a new object of class '<em>CONSTANT Double info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CONSTANT Double info</em>'.
	 * @generated
	 */
	CONSTANT_Double_info createCONSTANT_Double_info();

	/**
	 * Returns a new object of class '<em>Class File</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Class File</em>'.
	 * @generated
	 */
	ClassFile createClassFile();

	/**
	 * Returns a new object of class '<em>Constant Value attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constant Value attribute</em>'.
	 * @generated
	 */
	ConstantValue_attribute createConstantValue_attribute();

	/**
	 * Returns a new object of class '<em>Code attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Code attribute</em>'.
	 * @generated
	 */
	Code_attribute createCode_attribute();

	/**
	 * Returns a new object of class '<em>exception table entry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>exception table entry</em>'.
	 * @generated
	 */
	exception_table_entry createexception_table_entry();

	/**
	 * Returns a new object of class '<em>Stack Map Table attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Stack Map Table attribute</em>'.
	 * @generated
	 */
	StackMapTable_attribute createStackMapTable_attribute();

	/**
	 * Returns a new object of class '<em>Top variable info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Top variable info</em>'.
	 * @generated
	 */
	Top_variable_info createTop_variable_info();

	/**
	 * Returns a new object of class '<em>Integer variable info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Integer variable info</em>'.
	 * @generated
	 */
	Integer_variable_info createInteger_variable_info();

	/**
	 * Returns a new object of class '<em>Float variable info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Float variable info</em>'.
	 * @generated
	 */
	Float_variable_info createFloat_variable_info();

	/**
	 * Returns a new object of class '<em>Null variable info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Null variable info</em>'.
	 * @generated
	 */
	Null_variable_info createNull_variable_info();

	/**
	 * Returns a new object of class '<em>Uninitialized This variable info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uninitialized This variable info</em>'.
	 * @generated
	 */
	UninitializedThis_variable_info createUninitializedThis_variable_info();

	/**
	 * Returns a new object of class '<em>Object variable info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Object variable info</em>'.
	 * @generated
	 */
	Object_variable_info createObject_variable_info();

	/**
	 * Returns a new object of class '<em>New EClass36</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>New EClass36</em>'.
	 * @generated
	 */
	NewEClass36 createNewEClass36();

	/**
	 * Returns a new object of class '<em>Uninitialized variable info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uninitialized variable info</em>'.
	 * @generated
	 */
	Uninitialized_variable_info createUninitialized_variable_info();

	/**
	 * Returns a new object of class '<em>Long variable info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Long variable info</em>'.
	 * @generated
	 */
	Long_variable_info createLong_variable_info();

	/**
	 * Returns a new object of class '<em>Double variable info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Double variable info</em>'.
	 * @generated
	 */
	Double_variable_info createDouble_variable_info();

	/**
	 * Returns a new object of class '<em>same frame</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>same frame</em>'.
	 * @generated
	 */
	same_frame createsame_frame();

	/**
	 * Returns a new object of class '<em>same locals 1stack item frame</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>same locals 1stack item frame</em>'.
	 * @generated
	 */
	same_locals_1_stack_item_frame createsame_locals_1_stack_item_frame();

	/**
	 * Returns a new object of class '<em>same locals 1stack item frame extended</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>same locals 1stack item frame extended</em>'.
	 * @generated
	 */
	same_locals_1_stack_item_frame_extended createsame_locals_1_stack_item_frame_extended();

	/**
	 * Returns a new object of class '<em>same frame extended</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>same frame extended</em>'.
	 * @generated
	 */
	same_frame_extended createsame_frame_extended();

	/**
	 * Returns a new object of class '<em>chop frame</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>chop frame</em>'.
	 * @generated
	 */
	chop_frame createchop_frame();

	/**
	 * Returns a new object of class '<em>append frame</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>append frame</em>'.
	 * @generated
	 */
	append_frame createappend_frame();

	/**
	 * Returns a new object of class '<em>full frame</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>full frame</em>'.
	 * @generated
	 */
	full_frame createfull_frame();

	/**
	 * Returns a new object of class '<em>Exceptions attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exceptions attribute</em>'.
	 * @generated
	 */
	Exceptions_attribute createExceptions_attribute();

	/**
	 * Returns a new object of class '<em>Inner Classes attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inner Classes attribute</em>'.
	 * @generated
	 */
	InnerClasses_attribute createInnerClasses_attribute();

	/**
	 * Returns a new object of class '<em>Class member</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Class member</em>'.
	 * @generated
	 */
	Class_member createClass_member();

	/**
	 * Returns a new object of class '<em>Synthetic attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Synthetic attribute</em>'.
	 * @generated
	 */
	Synthetic_attribute createSynthetic_attribute();

	/**
	 * Returns a new object of class '<em>Signature attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signature attribute</em>'.
	 * @generated
	 */
	Signature_attribute createSignature_attribute();

	/**
	 * Returns a new object of class '<em>New EClass54</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>New EClass54</em>'.
	 * @generated
	 */
	NewEClass54 createNewEClass54();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	EcoregraphPackage getEcoregraphPackage();

} //EcoregraphFactory
