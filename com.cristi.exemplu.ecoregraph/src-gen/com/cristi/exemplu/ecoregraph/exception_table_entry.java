/**
 */
package com.cristi.exemplu.ecoregraph;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>exception table entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.cristi.exemplu.ecoregraph.exception_table_entry#getStart_pc <em>Start pc</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.exception_table_entry#getEnd_pc <em>End pc</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.exception_table_entry#getHandler_pc <em>Handler pc</em>}</li>
 *   <li>{@link com.cristi.exemplu.ecoregraph.exception_table_entry#getCatch_type <em>Catch type</em>}</li>
 * </ul>
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getexception_table_entry()
 * @model
 * @generated
 */
public interface exception_table_entry extends EObject {
	/**
	 * Returns the value of the '<em><b>Start pc</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start pc</em>' attribute.
	 * @see #setStart_pc(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getexception_table_entry_Start_pc()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getStart_pc();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.exception_table_entry#getStart_pc <em>Start pc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start pc</em>' attribute.
	 * @see #getStart_pc()
	 * @generated
	 */
	void setStart_pc(int value);

	/**
	 * Returns the value of the '<em><b>End pc</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End pc</em>' attribute.
	 * @see #setEnd_pc(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getexception_table_entry_End_pc()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getEnd_pc();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.exception_table_entry#getEnd_pc <em>End pc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End pc</em>' attribute.
	 * @see #getEnd_pc()
	 * @generated
	 */
	void setEnd_pc(int value);

	/**
	 * Returns the value of the '<em><b>Handler pc</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Handler pc</em>' attribute.
	 * @see #setHandler_pc(int)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getexception_table_entry_Handler_pc()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.UnsignedShort"
	 * @generated
	 */
	int getHandler_pc();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.exception_table_entry#getHandler_pc <em>Handler pc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Handler pc</em>' attribute.
	 * @see #getHandler_pc()
	 * @generated
	 */
	void setHandler_pc(int value);

	/**
	 * Returns the value of the '<em><b>Catch type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Catch type</em>' reference.
	 * @see #setCatch_type(CONSTANT_Class_info)
	 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getexception_table_entry_Catch_type()
	 * @model
	 * @generated
	 */
	CONSTANT_Class_info getCatch_type();

	/**
	 * Sets the value of the '{@link com.cristi.exemplu.ecoregraph.exception_table_entry#getCatch_type <em>Catch type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Catch type</em>' reference.
	 * @see #getCatch_type()
	 * @generated
	 */
	void setCatch_type(CONSTANT_Class_info value);

} // exception_table_entry
