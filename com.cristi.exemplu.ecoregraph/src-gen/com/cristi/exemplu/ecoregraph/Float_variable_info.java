/**
 */
package com.cristi.exemplu.ecoregraph;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Float variable info</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.cristi.exemplu.ecoregraph.EcoregraphPackage#getFloat_variable_info()
 * @model
 * @generated
 */
public interface Float_variable_info extends AbstractVerification_type_info {
} // Float_variable_info
